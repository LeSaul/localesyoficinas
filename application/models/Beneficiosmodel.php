<?php
class Beneficiosmodel extends CI_Model {

public function insertarBeneficio($data){
        $insertado = $this->db->insert("beneficiosanuncio", $data);

        if($insertado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function ultimoInsertado () {
        return $this->db->insert_id();
    }

    public function actualizarBeneficio($data, $id) {
        $this->db->where('idbeneficios', $id);
        $modificado =  $this->db->update('beneficiosanuncio',$data);
        if($modificado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function removerBeneficio($id) {
        $this->db->where('idbeneficios', $id);
        $modificado =  $this->db->delete('beneficiosanuncio');
        if($modificado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function obtenerBenficios($idAnuncio = 0) {
        $query = $this->db->query("select * from beneficiosanuncio where idAnuncio = $idAnuncio");

        return $query->result_array();;
    }


    public function numeroBeneficios ($id = 0) {
        $query = $this->db->query("select * from beneficiosanuncio where idAnuncio = $id;");
        if($query->num_rows() <= 6) {
            return true;
        } else {
            return false;
       }
    }

}

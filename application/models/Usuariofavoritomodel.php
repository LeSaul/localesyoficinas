<?php

class Usuariofavoritomodel extends CI_Model {

    public function obtener($idUsuario) {
        $informacion = null;
        $query = $this->db->query("select a.idAnuncio, a.fechaInicio, a.fechaFin, a.titulo, a.descripcion, a.precio, a.superficie, a.aprobado, a.renta, a.venta, a.idModo
        from anuncio a, usuario u, usuario_favorito uf
        where a.fechaFin > a.fechaInicio AND a.fechaFin > (select now()) AND a.aprobado = true and u.idUsuario = $idUsuario and u.idUsuario = uf.idUsuario and a.idAnuncio = uf.idAnuncio
order by a.idModo desc, a.fechaInicio desc;");

        if($query->num_rows()) {
            $informacion = $query->result();
        }
        return $informacion;
    }

    public function verificarFavorito ($idAnuncio, $idUsuario) {
        $query = $this->db->query("select* from usuario_favorito where idUsuario = $idUsuario and idAnuncio = $idAnuncio");
        if($query->num_rows() == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function insertar($idUsuario, $idAnuncio) {
        $result = false;

        $query = $this->db->query("select* from usuario_favorito where idUsuario = $idUsuario and idAnuncio = $idAnuncio");

        if($query->num_rows() == 0) {
            $data = array(
                'idUsuario' => $idUsuario,
                'idAnuncio' => $idAnuncio
            );
            $insertado = $this->db->insert("usuario_favorito", $data);
            if($insertado == 1){
                $result = true;
            }
        }

        return $result;
    }

    public function remover($idUsuario, $idAnuncio){
        $query = $this->db->query("delete from usuario_favorito
                                        where idUsuario = $idUsuario and idAnuncio = $idAnuncio");
        var_dump($query);
        return $query;
    }

}

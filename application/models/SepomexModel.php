<?php
class SepomexModel extends CI_Model {

	public function getMunicipios(){
		$data = $this->db->select('idMunicipio as id, municipio as nombre')
				->where('idEstado', 19)
				->group_by('idMunicipio')
				->order_by('municipio', 'ASC')
				->get('sepomex');

		return $data->custom_result_object('SepomexModel');
	}

	public function getColoniasCP($cp){
		$query = $this->db->where('cp', $cp)->order_by('asentamiento', 'asc')->get('sepomex');

		return $query->custom_result_object('SepomexModel');
	}

	public function getMunicipiosDisponibles($estado){

		$query = $this->db->select('s.idMunicipio as id_municipio, s.municipio as nombre_municipio')
						  ->from('anuncios as a')
						  ->join('sepomex as s', 'a.municipio = s.idMunicipio')
						  ->where('s.idEstado', $estado)
						  ->group_by('s.municipio, s.idMunicipio')
						  ->get();

		return $query->custom_result_object('SepomexModel');

	}

	public function getColonias($estado, $colonia){

		$query = $this->db->select('s.asentamiento')
						  ->from('anuncios as a')
						  ->join('sepomex as s', 'a.municipio = s.idMunicipio')
						  ->where('s.idEstado', $estado)
						  ->like('s.asentamiento', $colonia, 'after')
						  ->group_by('s.asentamiento')
						  ->get();

		$result = [];

		foreach ($query->result_array() as $s) {
			array_push($result, $s['asentamiento']);
		}

		$response = array(
			"query" 		=> "Unit",
			"suggestions"	=> $result
		);

		return $response;

	}

}
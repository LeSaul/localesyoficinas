<?php
class Caracteristicasmodel extends CI_Model {

public function insertarCaracteristica($data){
        $insertado = $this->db->insert("caracteristica", $data);
        if($insertado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function ultimoInsertado () {
        return $this->db->insert_id();
    }

    public function actualizarCaracteristica($data, $id) {
        $this->db->where('idCaracteristica', $id);
        $modificado =  $this->db->update('caracteristica',$data);
        if($modificado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function obtenerCaracteristicas() {
        $query = $this->db->query("select * from caracteristica");

        return $query->result_array();;
    }

    public function ligarInmobiliaria ($data) {
        $insertado = $this->db->insert("usuarioinmobiliaria", $data);
        if($insertado == 1){
            return true;
        }
        else {
            return false;
        }
    }
}

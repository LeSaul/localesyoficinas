<?php

class Mensajemodel extends CI_Model {

    public function insertarMensaje ($data){
        $insertado = $this->db->insert("mensaje", $data);
        if($insertado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function marcarLeido ($idReceptor, $idMensaje){
        $conditions = array('idReceptor' => $idReceptor, 'idMensaje' => $idMensaje);
        $this->db->where($conditions);
        $data = array('leido' => true);
        $modificado =  $this->db->update('mensaje',$data);
        if($modificado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function obtenerMensajes($id){
        $informacion = null;
        $query = $this->db->query("select u.idUsuario, m.idMensaje, concat(u.nombre , ' ', u.apellido) as nombre, u.correo, m.mensaje
                                        from usuario u left join mensaje m on m.idReceptor = u.idUsuario
                                        where u.idUsuario = $id AND m.mensaje != ''");

        $row = $query->row_array();

        if($query->num_rows()) {
            $informacion = $row;
        }
        return $informacion;
    }

}

<?php
class Rolesmodel extends CI_Model {

    public function obtenerRoles() {
        $informacion = null;
        $query = $this->db->query("select idRol as id, rolName
                                          from roles");

        $row = $query->result_array();

        return $row;
    }

    public function insertarRoles($data) {
        $insertado = $this->db->insert("roles", $data);
        if($insertado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function actualizarRoles($data, $id) {
        $this->db->where('idRol', $id);
        $modificado =  $this->db->update('roles',$data);
        if($modificado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function borrarRoles($id) {
        $this->db->where('idRol', $id);
        $modificado =  $this->db->delete('roles');
        if($modificado == 1){
            return true;
        }
        else{
            return false;
        }
    }
}

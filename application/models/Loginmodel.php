<?php
class Loginmodel extends CI_Model {
    function construct() {
        parent::__construct();
    }

    function obtenerSesion($correo, $contrasena) {
       $data = $this->db->Where('correo', $correo)->where('contrasena', $contrasena)->get('usuario');

       if ($data->num_rows() == 0) {
           $data = array(
                'error' => true,
                'msg'   => "Usuario y/o contraseña incorrecta" 
            );

           return $data;
       }


       return $data->row_array();
    }

    public function informacionSesion() {
        $result = array('session' => false, 'info' => null);

        $info = $this->session->all_userdata();
        if(isset($info['id'])){
            $result['session'] =  true;
            $result['info'] = $this->session->all_userdata();
        }

        return $result;
    }

    function terminarSesion() {
        $this->session->sess_destroy();
    }
}

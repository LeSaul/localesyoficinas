<?php

class Inmobiliariamodel extends CI_Model {

    public function insertarInmobiliaria($data){
        $insertado = $this->db->insert("inmobiliaria", $data);
        if($insertado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function ultimoInsertado () {
        return $this->db->insert_id();
    }

    public function actualizarInmobiliaria($data, $id) {
        $this->db->where('idInmobiliaria', $id);
        $modificado =  $this->db->update('inmobiliaria',$data);
        if($modificado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function obtenerInformacion($id) {
        $informacion = null;
        $query = $this->db->query("select * from inmobiliaria
                                          where idInmobiliaria = $id");

        $row = $query->row_array();

        if($query->num_rows()) {
            $informacion = $row;
        }
        return $informacion;
    }

    public function verificarPertenencia ($idUsuario, $idInmobiliaria) {
        $pertenece = false;

        $query = $this->db->query("SELECT * FROM usuarioinmobiliaria where idUSuario = $idUsuario and idInmobiliaria = $idInmobiliaria;");
        if($query->num_rows()) {
            $pertenece = true;
        }

        return $pertenece;
    }

    public function obtenerImagenActual($id = 0) {
        $reults = "";
        $query = $this->db->query("select logoPath from inmobiliaria
                                        where idInmobiliaria = $id;");
        foreach ($query->result() as $row) {
            $reults = $reults.$row->logoPath;
        }
        return $reults;
    }

    public function ligarInmobiliaria ($data) {
        $insertado = $this->db->insert("usuarioinmobiliaria", $data);
        if($insertado == 1){
            return true;
        }
        else {
            return false;
        }
    }


}

<?php

class Usuariomodel extends CI_Model {

    public function insertarUsuario($data) {
        $insertado = $this->db->insert("usuario", $data);
        if($insertado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function ultimaInsercion() {
        return $this->db->insert_id();
    }

    public function mailExistente ($mail)  {
        $query = $this->db->query("select * from usuario where correo = '$mail'");

        $row = $query->row_array();

        return $query->num_rows();

    }

    public function actualizarInformacion($data, $id) {
        $this->db->where('idUsuario', $id);
        $modificado =  $this->db->update('usuario',$data);
        if($modificado == 1){
            return true;
        }
        else{
            return false;
        }
    }

    public function obtenerInformacion($id) {
        $informacion = null;
        $query = $this->db->query("select concat(u.nombre, ' ', u.apellido) as usuario,
                                          u.idUsuario  as id
                                          from usuario u
                                          where idUsuario = $id");

        $row = $query->row_array();

        if($query->num_rows()) {
            $informacion = $row;
        }
        return $informacion;
    }

    public function obtenerTodaInformacion($id) {
        $informacion = null;
        $query = $this->db->query("select u.idUsuario  as id,
                                          nombre, apellido,
                                          correo, celular, telefono, direccion
                                          from usuario u
                                          where idUsuario = $id");

        $row = $query->row_array();

        if($query->num_rows()) {
            $informacion = $row;
        }
        return $informacion;
    }

}

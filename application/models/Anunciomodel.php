<?php

class Anunciomodel extends CI_Model {

    public function insertarAnuncio($data) {

        $insert = array(
            "fechaInicio"      =>   date('Y-m-d'),
            "fechaFin"         =>   date('Y-m-d', strtotime('+30 days', strtotime(date('Y-m-d')))),
            "titulo"           =>   $data['titulo'],
            "descripcion"      =>   $data['descripcion'],
            "precio"           =>   $data['precio'],
            "superficie"       =>   $data['superficie'],
            "tipo"             =>   $data['tipo'],
            "tipo_inmueble"    =>   $data['tipo_inmueble'],
            "colonia"          =>   $data['colonia'],
            "cp"               =>   $data['cp'],
            "estado"           =>   $data['estado'],
            "municipio"        =>   $data['municipio']
        );

        $this->db->insert('anuncios', $insert);

        $id = $this->db->insert_id();
        
        $response = array(
            'error' =>  false,
            'id'    =>  $id
        );

        return $response;

    }

    function asignarPropiedades($anuncio, $data){

        $insert = array(
            "id_anuncio"        =>  $anuncio['id'],
            "obra_gris"         =>  isset($data['obra_gris']) ? 1 : 0,
            "servicios"         =>  isset($data['servicios_basicos']) ? 1 : 0,
            "estacionamiento"   =>  isset($data['estacionamiento']) ? 1 : 0,
            "clima"             =>  isset($data['clima']) ? 1 : 0,
            "telefono"          =>  isset($data['telefono']) ? 1 : 0,
            "internet"          =>  isset($data['internet']) ? 1 : 0,
            "mobiliario"        =>  isset($data['mobiliario']) ? 1 : 0,
            "sanitarios"        =>  isset($data['sanitario']) ? 1 : 0,
            "otras"             =>  !empty($data['otras']) ? $data['otras'] : null
        );


        $this->db->insert('anuncios_caracteristicas', $insert);

        $insert_id = $this->db->insert_id();

        if ($insert_id != '') {

            $response = array(
                'error' =>  true,
                'msj'   =>  'Hubo un error al cargar las propiedades, favor de contactar a soporte.'
            );

            return $response;
        }

        $response = array(
            'error'     =>  false,
            'insert'    =>  $insert_id 
        );

        return $response;

    }

    function getPrecios(){

        $query = $this->db->select('min(precio) as precio_min, max(precio) as precio_max')->get('anuncios');

        return $query->custom_row_object(0, 'Anunciomodel');
    }

    function getSuperficie(){
        
        $query = $this->db->select('min(superficie) as superficie_min, max(superficie) as superficie_max')->get('anuncios');

        return $query->custom_row_object(0, 'Anunciomodel');
    }

}

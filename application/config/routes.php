<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'HomeController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['registro'] = 'LoginController/registrarUsuario';
$route['crear-usuario'] = 'LoginController/crearUsuario';
$route['login-user'] = 'LoginController/iniciarSesion';
$route['cerrar-sesion'] = 'LoginController/cerrarSesion';

$route['usuarios/insertar'] = 'Usuario/insertarUsuario';

$route['anuncios/nuevo-anuncio'] = 'AnuncioController/insertarAnuncio';
$route['anuncios/registrar'] = 'AnuncioController/nuevoAnuncio';

$route['inmuebles_encontrados/venta/(:num)'] = 'BusquedaController/indexVenta/$1';
$route['inmuebles_encontrados/renta/(:num)'] = 'BusquedaController/indexRenta/$1';

$route['sepomex/get-colonias/(:num)'] = 'SepomexController/getColoniasbyCP/$1';
$route['sepomex/colonias'] = 'SepomexController/getColonias';
$route['sepomex/get-cpdata/(:num)'] = 'SepomexController/getCpData/$1';

$route['anuncio/(:num)/(:num)'] = 'AnuncioController/mostrarAnuncio/$1/$2';

$route['contacto'] = 'ContactoController/index';

$route['inmuebles/venta/(:any)/(:num)'] = 'AnuncioController/anunciosVenta/$1/$2';
$route['inmuebles/renta/(:any)/(:num)'] = 'AnuncioController/anunciosVenta/$1/$2';

$route['security/bot-protection']   = 'SeguridadController/recaptchaVerification';


//Rutas de autenticación

$route['usuario/mis-anuncios/list'] = 'AnuncioController/misAnuncios';
$route['usuario/mis-anuncios/usuario/mis-anuncios/id/(:num)'] = 'AnuncioController/anuncio/$1';

$route['usuario/mi-perfil'] = 'ProfileController/index';
$route['usuario/transacciones'] = 'ProfileController/transactions';
$route['usuario/metodos-pago'] = 'ProfileController/metodos_pago';
$route['usuario/cambiar-password'] = 'ProfileController/cambiar_password';
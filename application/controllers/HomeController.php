<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('SepomexModel', 'sepomex');
		$this->load->model('Anunciomodel', 'anuncios');
    }

	public function index(){

		$estado = 19;

		$data = array(
			'title'			=>	'Inicio - Locales y Oficinas Monterrey',

			'precios'		=>	$this->anuncios->getPrecios(),

			'municipios'	=>	$this->sepomex->getMunicipiosDisponibles($estado),

			'superficies'	=>	$this->anuncios->getSuperficie(),

			'js_page'   	=>  array('home.js'),

			'css_page'		=> array('home.css'),

			'anuncios'		=>	$this->db->get('anuncios')->result_array(),

			'css_plugins'   =>  array('slick/slick.css', 'slick/slick-theme.css'),

            'js_plugins' 	=>  array('slick/slick.js', 'autocomplete/jquery.autocomplete.js', 'jquery-validation/jquery.validate.min.js'),

			'page'			=>	'home'

		);

		$this->load->view('layout/main', $data);
	}
}

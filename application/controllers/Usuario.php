<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class usuario extends CI_Controller {

    public function insertarUsuario() {
    

        $this->load->model('Usuariomodel');

        $resultadoInsercion = null;


        $nombre = $this->input->post('name');
        $apellido = $this->input->post('lastname');
        $correo = $this->input->post('register-email');
        $celular = !empty($this->input->post('celular')) ? $this->input->post('celular') : null;
        $telefono = !empty($this->input->post('telefono')) ? $this->input->post('telefono') : null;
        $direccion = !empty($this->input->post('direccion')) ? $this->input->post('direccion') : null;
        $contrasena = sha1($this->input->post('register-password'));
        $idRol = !empty($this->input->post('idRol')) ? $this->input->post('idRol') : 3;

        $data = array(
            'nombre' => $this->security->xss_clean($nombre),
            'apellido' => $this->security->xss_clean($apellido),
            'correo' => $this->security->xss_clean($correo),
            'celular' => $this->security->xss_clean($celular),
            'telefono' => $this->security->xss_clean($telefono),
            'direccion' => $this->security->xss_clean($direccion),
            'contrasena' => $this->security->xss_clean($contrasena),
            'idRol' => $this->security->xss_clean($idRol),
            'activo' => 1,
        );

        $existe = $this->Usuariomodel->mailExistente($correo);

        if ($existe > 0) {
            
            $result = array(
                'error' =>  true,
                'msj'   =>  'El correo electrónico ya ha sido registrado'
            );

        }else {

            $resultadoInsercion = $this->Usuariomodel->insertarUsuario($data);

            if ($resultadoInsercion == true) {
                $result = array(
                    'error' =>  false,
                    'msj'   =>  'Usuario agregado exitosamente. Redireccionando...'
                );

            } else {
                
                $result = array(
                    'error' =>  true,
                    'msj'   =>  'Hubo un problema al registrar la cuenta, intenta nuevamente'
                );
            }
        }

        echo json_encode($result);
    }

    public function actualizarContrasena() {
        $result = array('error' => true, 'success' => false, 'issues' => []);
        $this->load->library('form_validation');

        $this->load->model('Usuariomodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();

        $this->form_validation->set_rules('contrasena', 'contrasena', 'trim|required|exact_length[6]');

        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('exact_length', 'El campo {field} debe contener exactamente {param} caracteres.');

        if ($this->form_validation->run() == TRUE  && $info['session'] ==  true) {
            $id = $info['info']['id'];
            $contrasena = $this->input->post('contrasena');

            $data = array(
                'contrasena' => $this->security->xss_clean($contrasena)
            );

            $resultadoInsercion =  $this->Usuariomodel->actualizarInformacion($data, $id);

            if($resultadoInsercion == true) {
                $result['success'] = true;
                $result['error'] = false;
            } else{
                 array_push($result['issues'], 'Actualizacion no exitosa');
            }
        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }

        echo json_encode($result);
    }

    public function actualizarInformacion() {
        $result = array('error' => true, 'success' => false, 'issues' => []);
        $this->load->library('form_validation');

        $this->load->model('Usuariomodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('apellido','apellido', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('correo', 'correo', 'trim|required|max_length[50]|valid_email');
        $this->form_validation->set_rules('celular', 'celularL', 'trim|max_length[30]');
        $this->form_validation->set_rules('telefono', 'telefono', 'trim|max_length[30]');
        $this->form_validation->set_rules('direccion', 'direccion', 'trim|max_length[200]');
        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('max_length', 'El campo {field} debe tener como maximo {param} caracteres.');
        $this->form_validation->set_message('valid_email', 'El campo {field} no contiene una sintaxis correcta.');
        $this->form_validation->set_message('exact_length', 'El campo {field} debe contener exactamente {param} caracteres.');

        if ($this->form_validation->run() == true  && $info['session'] ==  true) {
            $nombre = $this->security->xss_clean($this->input->post('nombre'));
            $apellido = $this->security->xss_clean($this->input->post('apellido'));
            $correo = $this->security->xss_clean($this->input->post('correo'));
            $celular = $this->security->xss_clean($this->input->post('celular'));
            $telefono = $this->security->xss_clean($this->input->post('telefono'));
            $direccion = $this->security->xss_clean($this->input->post('direccion'));
            $id = $info['info']['id'];

            $data = array(
                'nombre' => $nombre,
                'apellido' => $apellido,
                'correo' => $correo,
                'celular' => $celular,
                'telefono' => $telefono,
                'direccion' => $direccion
            );

            $resultadoInsercion =  $this->Usuariomodel->actualizarInformacion($data,  $id);

            if($resultadoInsercion == true) {
                $result['success'] = true;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'Actualizacion no exitosa');
            }

        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }

        echo json_encode($result);
    }

    public function eliminar() {
        $result = array('error' => true, 'issues' => []);

        $this->load->model('Usuariomodel');
        $this->load->model('Loginmodel');
        $info = $this->Loginmodel->informacionSesion();

        $id =  $info['info']['id'];
        $rol = $info['info']['rol'];

        if($this->input->post('id') != null && ($this->input->post('id') == $id || $rol == 1) ) {
            $id = $this->input->post('id');
            $id = $this->security->xss_clean($id);

            $data = array(
                'activo' => 0
            );

            $resultado =  $this->Usuariomodel->actualizarInformacion($data,  $id);

            if($resultado !=  null) {
                $result['info'] = $resultado;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'No es posible actualizar informacion');
            }
        } else {
             array_push($result['issues'], 'El id es necesario para obtener informacion del usuario');
        }

        echo json_encode($result);
    }

    public function obtenerInformacion() {
        $result = array('error' => true, 'issues' => []);
        $this->load->model('Usuariomodel');

        if($this->input->post('id') != null) {
            $id = $this->input->post('id');
            $id = $this->security->xss_clean($id);

            $resultado = $this->Usuariomodel->obtenerInformacion($id);

            if($resultado !=  null) {
                $this->session->set_userdata($resultado);
                $result['info'] = $resultado;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'No es posible obtener informacion');
            }
        } else {
             array_push($result['issues'], 'El id es necesario para obtener informacion del usuario');
        }

        echo json_encode($result);
    }
    public function consultarInformacion() {
        $result = array('error' => true, 'issues' => []);
        $this->load->model('Usuariomodel');

        if($this->input->post('id') != null) {
            $id = $this->input->post('id');
            $id = $this->security->xss_clean($id);

            $resultado = $this->Usuariomodel->obtenerTodaInformacion($id);

            if($resultado !=  null) {
                $this->session->set_userdata($resultado);
                $result['info'] = $resultado;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'No es posible obtener informacion');
            }
        } else {
             array_push($result['issues'], 'El id es necesario para obtener informacion del usuario');
        }

        echo json_encode($result);
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileController extends CI_Controller {

	function __construct() {
        parent::__construct();
    }

    public function index(){

        $usuario = $this->db->where('idUsuario', $this->session->idUsuario)->get('usuario');

        $inmobiliaria = $this->db->from('usuarioinmobiliaria as u')
                             ->select('i.*')
                             ->where('u.idUsuario', $this->session->idUsuario)
                             ->join('inmobiliaria as i', 'u.idInmobiliaria = i.idInmobiliaria')
                             ->get();

        $anuncios = $this->db->where('estatus !=', 'P')->where('idUsuario', $this->session->idUsuario)->get('anuncios')->num_rows();
        
        $anuncios_activos = $this->db->where('estatus', 'A')->where('fechaFin >', date('Y-m-d'))->where('idUsuario', $this->session->idUsuario)->get('anuncios')->num_rows();

		$data = array(
            'title'			    =>	'Mis Formas de Pago - Locales y Oficinas Monterrey',

            'usuario'           =>  $usuario->row_array(),

            'usrInmobiliaria'   =>  $inmobiliaria->num_rows() > 0 ? true : false,
            
            'inmobiliaria'      =>  $inmobiliaria->row_array(),

            'anuncios_activos'  =>  $anuncios_activos,

            'anuncios'          =>  $anuncios,
            
			'js_page'   	    =>  array(),

			'css_plugins'       =>  array(),

            'js_plugins' 	    =>  array('jquery-validation/jquery.validate.min.js'),

			'page'			    =>	'perfil-info'

		);

		$this->load->view('layout/main', $data);
	}

	public function metodos_pago(){

        $usuario = $this->db->where('idUsuario', $this->session->idUsuario)->get('usuario');

        $inmobiliaria = $this->db->from('usuarioinmobiliaria as u')
                             ->select('i.*')
                             ->where('u.idUsuario', $this->session->idUsuario)
                             ->join('inmobiliaria as i', 'u.idInmobiliaria = i.idInmobiliaria')
                             ->get();

        $anuncios = $this->db->where('estatus !=', 'P')->where('idUsuario', $this->session->idUsuario)->get('anuncios')->num_rows();
        
        $anuncios_activos = $this->db->where('estatus', 'A')->where('fechaFin >', date('Y-m-d'))->where('idUsuario', $this->session->idUsuario)->get('anuncios')->num_rows();

		$data = array(
            'title'			    =>	'Mis Formas de Pago - Locales y Oficinas Monterrey',

            'usuario'           =>  $usuario->row_array(),

            'usrInmobiliaria'   =>  $inmobiliaria->num_rows() > 0 ? true : false,
            
            'inmobiliaria'      =>  $inmobiliaria->row_array(),

            'anuncios_activos'  =>  $anuncios_activos,

            'anuncios'          =>  $anuncios,
            
			'js_page'   	    =>  array(),

			'css_plugins'       =>  array(),

            'js_plugins' 	    =>  array('jquery-validation/jquery.validate.min.js'),

			'page'			    =>	'perfil-metodos-pago'

		);

		$this->load->view('layout/main', $data);
    }
    
    public function transactions(){

        $usuario = $this->db->where('idUsuario', $this->session->idUsuario)->get('usuario');

        $inmobiliaria = $this->db->from('usuarioinmobiliaria as u')
                             ->select('i.*')
                             ->where('u.idUsuario', $this->session->idUsuario)
                             ->join('inmobiliaria as i', 'u.idInmobiliaria = i.idInmobiliaria')
                             ->get();

        $anuncios = $this->db->where('estatus !=', 'P')->where('idUsuario', $this->session->idUsuario)->get('anuncios')->num_rows();
        
        $anuncios_activos = $this->db->where('estatus', 'A')->where('fechaFin >', date('Y-m-d'))->where('idUsuario', $this->session->idUsuario)->get('anuncios')->num_rows();

		$data = array(
            'title'			    =>	'Mis Formas de Pago - Locales y Oficinas Monterrey',

            'usuario'           =>  $usuario->row_array(),

            'usrInmobiliaria'   =>  $inmobiliaria->num_rows() > 0 ? true : false,
            
            'inmobiliaria'      =>  $inmobiliaria->row_array(),

            'anuncios_activos'  =>  $anuncios_activos,

            'anuncios'          =>  $anuncios,
            
			'js_page'   	    =>  array(),

			'css_plugins'       =>  array(),

            'js_plugins' 	    =>  array('jquery-validation/jquery.validate.min.js'),

			'page'			    =>	'perfil-transacciones'

		);

		$this->load->view('layout/main', $data);
    }
    
    public function cambiar_password(){

        $usuario = $this->db->where('idUsuario', $this->session->idUsuario)->get('usuario');

        $inmobiliaria = $this->db->from('usuarioinmobiliaria as u')
                             ->select('i.*')
                             ->where('u.idUsuario', $this->session->idUsuario)
                             ->join('inmobiliaria as i', 'u.idInmobiliaria = i.idInmobiliaria')
                             ->get();

        $anuncios = $this->db->where('estatus !=', 'P')->where('idUsuario', $this->session->idUsuario)->get('anuncios')->num_rows();
        
        $anuncios_activos = $this->db->where('estatus', 'A')->where('fechaFin >', date('Y-m-d'))->where('idUsuario', $this->session->idUsuario)->get('anuncios')->num_rows();

		$data = array(
            'title'			    =>	'Mis Formas de Pago - Locales y Oficinas Monterrey',

            'usuario'           =>  $usuario->row_array(),

            'usrInmobiliaria'   =>  $inmobiliaria->num_rows() > 0 ? true : false,
            
            'inmobiliaria'      =>  $inmobiliaria->row_array(),

            'anuncios_activos'  =>  $anuncios_activos,

            'anuncios'          =>  $anuncios,
            
			'js_page'   	    =>  array(),

			'css_plugins'       =>  array(),

            'js_plugins' 	    =>  array('jquery-validation/jquery.validate.min.js'),

			'page'			    =>	'perfil-password'

		);

		$this->load->view('layout/main', $data);
	}
}

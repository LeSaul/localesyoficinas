<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class LoginController extends CI_Controller {

     function __construct() {
        parent::__construct();
    }

    public function registrarUsuario(){

        $data = array(
            'title' =>  'Registro - Locales y Oficinas Monterrey',

            'page'  =>  'registro',

            'css_plugins'   =>  array(),

            'js_plugins' =>  array(),

            'js_page'   =>  array('register.js')
        );

        $this->load->view("layout/main", $data);
    }

    public function crearUsuario(){

        $data = $this->input->post();

        echo json_encode($data);

    }


    public function iniciarSesion() {

        $this->load->model('Loginmodel');

        $correo = trim($this->input->post('email'));
        $contrasena = sha1($this->input->post('password'));

        $user = $this->Loginmodel->obtenerSesion($correo, $contrasena);

        if(isset($user['error'])) {
            echo json_encode($user);
            return;
        }

        $newdata = array(
                'nombre'    =>  $user['nombre'],
                'fullname'  =>  $user['nombre']." ".$user['apellido'],
                'correo'    =>  $user['correo'],
                'idUsuario' =>  $user['idUsuario']
        );

        $this->session->set_userdata($newdata);

        echo json_encode($user);
    }

    public function cerrarSesion() {
        $this->session->sess_destroy();
        redirect(base_url()); 
    }
}


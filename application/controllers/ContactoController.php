<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactoController extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('SepomexModel', 'sepomex');
		$this->load->model('Anunciomodel', 'anuncios');
    }

	public function index(){

		$data = array(
			'title'			=>	'Inicio - Locales y Oficinas Monterrey',

			'js_page'   	=>  array(),

			'css_plugins'   =>  array(),

            'js_plugins' 	=>  array('jquery-validation/jquery.validate.min.js'),

			'page'			=>	'contacto'

		);

		$this->load->view('layout/main', $data);
	}
}

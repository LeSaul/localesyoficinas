<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AnuncioController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('SepomexModel', 'sepomex');
        $this->load->model('Anunciomodel', 'anuncios');
    }

    public function anuncio($id){

        if(!isset($this->session->idUsuario)){
            echo "not logged";
            return;
        }

        $anuncio = $this->db->from('anuncios as a')->select('s.asentamiento as colonia_nombre, a.*')
                                                   ->where('idAnuncio', $id)
                                                   ->join('sepomex as s', 'a.colonia = s.id')
                                                   ->get()
                                                   ->row_array();
        $caracteristicas = $this->db->where('id_anuncio', $id)->get('anuncios_caracteristicas')->row_array();

        $estados = $this->db->select('idEstado, estado')->group_by('idEstado')->get('sepomex')->result_array();

        $municipios = $this->db->select('idMunicipio, municipio')->where('idEstado', $anuncio['estado'])->group_by('idMunicipio')->get('sepomex')->result_array();

        $usuario = $this->db->where('idUsuario', $this->session->idUsuario)->get('usuario')->row_array();

        $inmobiliaria = $this->db->from('usuarioinmobiliaria as u')
                                 ->select('i.*')
                                 ->join('inmobiliaria as i', 'u.idInmobiliaria = i.idInmobiliaria')
                                 ->get();

        $data = array(
            'title'             =>  'Pubicar Anuncio - Locales y Oficinas',

            'page'              =>  'editar-anuncio',

            'anuncio'           =>  $anuncio,
            
            'estados'           =>  $estados,

            'municipios'        =>  $municipios,

            'caracteristicas'   =>  $caracteristicas,

            'usuario'           =>  $usuario,

            'usr_inmobiliaria'  =>  $inmobiliaria->num_rows() > 0 ? true : false,

            'inmobiliaria'      =>  $inmobiliaria->row_array(),

            'css_plugins'       =>  array(),

            'js_plugins'        =>  array(),

            'js_page'           =>  array('editar_anuncio.js'),

            'tipo_inmueble'     =>  $this->db->where('activo', 1)->get('tipo_inmueble')->result_array()
        );

        $this->load->view("layout/main", $data);
    }

    public function anunciosVenta($inmueble, $page){
    
        $this->load->library('pagination');

        $tipo = $this->uri->segment(2);

        if($page == 1){
            $offset = 0;
        }else{
            $offset = ($page-1)*25;
        }

        $items = 25;

        $anuncios = $this->db->where('fechaFin >', date('Y-m-d')  )
                             ->where('tipo_uso', strtolower($inmueble) == 'oficina' ? 'O' : 'L')
                             ->where('tipo', strtolower($tipo) == 'renta' ? 'R' : 'V')
                             ->order_by('premium', 'desc')
                             ->limit($items,$offset)
                             ->get('anuncios');

        $all_anuncios = $this->db->where('fechaFin >', date('Y-m-d')  )
                             ->where('tipo_uso', strtolower($inmueble) == 'oficina' ? 'O' : 'L')
                             ->where('tipo', strtolower($tipo) == 'renta' ? 'R' : 'V')
                             ->get('anuncios');
                             

        $config_pagination = array(
            'first_url'         =>  1,
            'base_url'          =>  base_url('/inmuebles/venta')."/".$inmueble,
            'total_rows'        =>  $all_anuncios->num_rows(),
            'uri_segment'       =>  4,
            'use_page_numbers'  =>  true,
            'per_page'          =>  25,
            'attributes'        =>  array('class'=>"page-link"),
            'full_tag_open'     =>  "<ul class='pagination justify-content-end'>",
            'full_tag_close'    =>  "</ul>",
            'num_tag_open'      =>  '<li class="page-item">',
            'num_tag_close'     =>  '</li>',
            'cur_tag_open'      =>  '<li class="page-item active"><a class="page-link" href="javascript:void(0)">',
            'cur_tag_close'     =>  "</a></li>",
            'next_link'         =>  "<i class='fa fa-chevron-right'></i>",
            'next_tag_open'     =>  '<li class="page-item">',
            'next_tagl_close'   =>  "</li>",
            'prev_link'         =>  "<i class='fa fa-chevron-left'></i>",
            'prev_tag_open'     =>  '<li class="page-item">',
            'prev_tagl_close'   =>  "</li>",
            'first_link'        =>  'Inicio',
            'first_tag_open'    =>  '<li class="page-item">',
            'first_tagl_close'  =>  "</li>",
            'last_link'         =>  'Final',
            'last_tag_open'     =>  '<li class="page-item">',
            'last_tagl_close'   =>  "</li>",
        );

        $this->pagination->initialize($config_pagination);

        $pagination = $this->pagination->create_links();

        $data = array(
            'title'			=>	'Locales y Oficinas - '.ucfirst($inmueble).' Venta',

            'anuncios'      =>  $anuncios->result_array(),

            'resultados'    =>  $anuncios->num_rows(),
            
            'css_plugins'   =>  array('slick/slick.css', 'slick/slick-theme.css'),

            'js_plugins' 	=>  array('slick/slick.js', 'autocomplete/jquery.autocomplete.js', 'jquery-validation/jquery.validate.min.js'),

            'page'			=>	'busqueda_anuncios',
            
            'pagination'    =>  $pagination

        );

        $this->load->view('layout/main', $data);

        
    }    

    public function misAnuncios(){

        if(!isset($this->session->idUsuario)){
            echo "not logged";
            return;
        }

        $anuncios = $this->db->from('anuncios as a')
                             ->select('a.idAnuncio, a.fechaFin, a.titulo, a.precio, s.estado as estado')
                             ->join('sepomex as s', 'a.estado = s.idEstado')
                             ->where('a.idUsuario', $this->session->idUsuario)
                             ->group_by('a.idAnuncio')
                             ->get();

        $anuncios_activos = $this->db->from('anuncios as a')
                                    ->select('a.idAnuncio, a.fechaFin, a.titulo, a.precio, s.estado as estado')
                                    ->join('sepomex as s', 'a.estado = s.idEstado')->where('idUsuario', $this->session->idUsuario)
                                    ->where('a.fechaFin >', date('Y-m-d'))
                                    ->where('a.estatus', 'A')
                                    ->group_by('a.idAnuncio')
                                    ->get();

        $anuncios_expirados = $this->db->from('anuncios as a')
                                    ->select('a.idAnuncio, a.fechaFin, a.titulo, a.precio, s.estado as estado')
                                    ->join('sepomex as s', 'a.estado = s.idEstado')->where('idUsuario', $this->session->idUsuario)
                                    ->where('idUsuario', $this->session->idUsuario)
                                    ->where('a.fechaFin <', date('Y-m-d'))
                                    ->where('a.estatus', 'A')
                                    ->group_by('a.idAnuncio')
                                    ->get();

        $anuncios_pendientes =  $this->db->from('anuncios as a')
                                        ->select('a.idAnuncio, a.fechaFin, a.titulo, a.precio, s.estado as estado')
                                        ->join('sepomex as s', 'a.estado = s.idEstado')->where('idUsuario', $this->session->idUsuario)
                                        ->where('a.idUsuario', $this->session->idUsuario)
                                        ->where('a.estatus', 'P')
                                        ->group_by('a.idAnuncio')
                                        ->get();
        
        
        $data = array(
            'title'                 =>  'Locales y Oficinas - Anuncio',

            'page'                  =>  'mis-anuncios',

            'anuncios'              =>  $anuncios->result_array(),

            'css_plugins'           =>  array(),

            'js_plugins'            =>  array(),

            'js_page'               =>  array(),

            'css_page'              =>  array(),

            'anuncios'              =>  $anuncios->result_array(),

            'anuncios_activos'      =>  $anuncios_activos->result_array(),

            'anuncios_expirados'    =>  $anuncios_expirados->result_array(),

            'anuncios_pendientes'   =>  $anuncios_pendientes->result_array(),

            'total_anuncios'        =>  $anuncios->num_rows(),
            
            'total_activos'         =>  $anuncios_activos->num_rows(),
            
            'total_expirados'       =>  $anuncios_expirados->num_rows(),
            
            'total_pendientes'      =>  $anuncios_pendientes->num_rows()
            
        );

        $this->load->view("layout/main", $data);
    }

    public function mostrarAnuncio($usuario, $id_anuncio){

        $data_anuncio = $this->db->where('idAnuncio', $id_anuncio)->get('anuncios')->row_array();
        $data_usuario = $this->db->from('usuario as u')
                                 ->select('u.*, r.rolName as rol_nombre')
                                 ->join('roles as r', 'u.idRol = r.idRol')
                                 ->where('u.idUsuario', $usuario)
                                 ->get()
                                 ->row_array();

        $data = array(
            'title'         =>  'Locales y Oficinas - Anuncio',

            'page'          =>  'anuncio',

            'anuncio'       =>  $data_anuncio,

            'usuario'       =>  $data_usuario,

            'css_plugins'   =>  array(),

            'js_plugins'    =>  array(),

            'js_page'       =>  array('anuncio.js'),

            'css_page'      =>  array('anuncio.css'),

            'municipios'    =>  $this->sepomex->getMunicipios(),

            'tipo_inmueble' =>  $this->db->where('activo', 1)->get('tipo_inmueble')->result_array()
        );

        $this->load->view("layout/main", $data);
    }

    public function insertarAnuncio() {

        $data = array(
            'title'         =>  'Pubicar Anuncio - Locales y Oficinas',

            'page'          =>  'crear-anuncio',

            'css_plugins'   =>  array('noty/lib/noty.css', 'summernote/summernote-bs4.css'),

            'js_plugins' =>  array('noty/lib/noty.min.js', 'summernote/summernote-bs4.min.js', 'summernote/lang/summernote-es-ES.js'),

            'js_page'       =>  array('nuevo-anuncio.js'),

            'municipios'    =>  $this->sepomex->getMunicipios(),

            'tipo_inmueble' =>  $this->db->where('activo', 1)->get('tipo_inmueble')->result_array()
        );

        $this->load->view("layout/main", $data);
    }

    function nuevoAnuncio(){

        $post = $this->input->post();

        $anuncio = $this->anuncios->insertarAnuncio($post);

        $this->propiedadesAnuncio($anuncio, $post);

        echo json_encode($anuncio, JSON_UNESCAPED_UNICODE);

    }

    function propiedadesAnuncio($anuncio, $post){

        $this->anuncios->asignarPropiedades($anuncio, $post);

    }

    function imagenesAnuncio(){
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( !$this->upload->do_upload('file')){
            $error = array('error' => $this->upload->display_errors());

            $this->load->view('upload_form', $error);
        }else{
            $data = array('upload_data' => $this->upload->data());

            $this->load->view('upload_success', $data);
        }
    }

    function getColoniaByName(){

        $get = $this->input->get();

        $colonias = $this->db->where('idEstado', 19)
                 ->where('idMunicipio', 39)
                 ->like('asentamiento', $get['colonia'], 'after')
                 ->get('sepomex')
                 ->result_array();

        echo json_encode($colonias, JSON_UNESCAPED_UNICODE);

    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SeguridadController extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('SepomexModel', 'sepomex');
		$this->load->model('Anunciomodel', 'anuncios');
    }

	public function recaptchaVerification(){
        $token = $this->input->post('token');
        
        $secret = '6LdaH6kUAAAAADcfeBbSVFlEuJNLnWWbupCkjZyV';
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$token);
        $responseData = json_decode($verifyResponse);
        echo json_encode($responseData);
         
	}
}

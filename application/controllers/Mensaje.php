<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mensaje extends CI_Controller {

    public function enviarMensaje() {
        $result = array('error' => true, 'issues' => []);

        $this->load->library('form_validation');

        $this->load->model('Mensajemodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();

        $this->form_validation->set_rules('receptor', 'receptor', 'trim|required|numeric');
        $this->form_validation->set_rules('mensaje', 'mensaje', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('fecha', 'fecha', 'trim|required');

        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('max_length', 'El campo {field} debe tener como maximo {param} caracteres.');

        if($info['session'] == false) {
            array_push($result['issues'], 'Es necesario iniciar sesion.');
        }

        if ($this->form_validation->run() == true && $info['session'] ==  true) {
            $idReceptor = $this->input->post('receptor');
            $mensaje = $this->input->post('mensaje');
            $fecha = $this->input->post('fecha');
            $leido = false;
            $idUser =  $info['info']['id'];

            $data = array(
                'idEmisor' => $idUser,
                'idReceptor' => $idReceptor,
                'mensaje' => $mensaje,
                'fecha' => $fecha,
                'leido' => $leido
            );

            $resultadoInsercion = $this->Mensajemodel->insertarMensaje ($data);

            if($resultadoInsercion == true) {
                $result['success'] = true;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'No es poenviarsible enviar mensaje');
            }

        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }

        echo json_encode($result);
    }

    public function marcarLeido() {
        $result = array('error' => true, 'issues' => []);

        $this->load->library('form_validation');

        $this->load->model('Mensajemodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();
        $idUser =  $info['info']['id'];

        $this->form_validation->set_rules('idMensaje', 'Id del mensaje', 'trim|required|numeric');
        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('rnumeric', 'El atributo {field} debe ser numerico.');

        if($info['session'] == false) {
            array_push($result['issues'], 'Es necesario iniciar sesion.');
        }

        if ($this->form_validation->run() == true && $info['session'] ==  true) {
            $idMensaje = $this->security->xss_clean($this->input->post('idMensaje'));
            $resultadoInsercion =  $this->Mensajemodel->marcarLeido($idReceptor, $idUser);
            if($resultadoInsercion == true) {
                $result['success'] = true;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'No es posible actualizar el estado del mensaje');
            }
        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }

        echo json_encode($result);
    }

    public function obtenerMensajes() {
               $result = array('error' => true, 'issues' =>[]);

        $this->load->model('Mensajemodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();

        if($info['session'] == false) {
            array_push($result['issues'], 'Es necesario iniciar sesion.');
        }

        if(isset($info['info']['id'])){
            $idUsuario =  $info['info']['id'];

            $resultado = $this->Mensajemodel->obtenerMensajes($idUsuario);
            if($resultado !=  null){
                $result['messages'] = $resultado;
                $result['error'] = false;
            } else {
                $result['error'] = false;
            }

        }

        echo json_encode($result);
    }

}


<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class favorito extends CI_Controller {

    public function obtenerFavoritos() {
        $result = array('error' => true, 'success' => false, 'issues' => []);

        $this->load->model('Usuariofavoritomodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();

        if ($info['session'] ==  true) {
            if(isset($info['info']['id'])){
                $idUsuario =  $info['info']['id'];

                $resultado = $this->Usuariofavoritomodel->obtener($idUsuario);
                if($resultado !=  null){
                    $result['messages'] = $resultado;
                    $result['error'] = false;
                } else {
                    $result['error'] = false;
                }

            } else {
                array_push($result['issues'], 'No tienes los permisos necesario para realizar la accion');
            }
        }

        echo json_encode($result);
    }

    public function insertarFavorito() {
        $result = array('error' => true, 'success' => false, 'issues' => []);
        $this->load->library('form_validation');

        $this->load->model('Usuariofavoritomodel');
        $this->load->model('Loginmodel');
        $info = $this->Loginmodel->informacionSesion();

        $this->form_validation->set_rules('idAnuncio', 'idAnuncio', 'trim|required|numeric');

        if($info['session'] ==  false) {
            array_push($result['issues'], 'Necesitas estar registrado para realizar esta opeacion');

            echo json_encode($result);
            return;
        }

        if ($this->form_validation->run() == TRUE) {
            if(isset($info['info']['id'])){
                $idUsuario =  $info['info']['id'];
                $idAnuncio =  $this->input->post('idAnuncio');

            $resultadoInsercion = $this->Usuariofavoritomodel->insertar($idUsuario, $idAnuncio);
            if ($resultadoInsercion == true) {
                $result['success'] = true;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'Insercion no exitosa');
            }

            } else {

                $issues = $this->form_validation->error_array();
                foreach ($issues as $value) {
                    array_push($result['issues'], $value);
                }

            }
        }

        echo json_encode($result);
    }

    public function removerFavorito() {
        $result = array('error' => true, 'success' => false, 'issues' => []);
        $this->load->library('form_validation');

        $this->load->model('Usuariofavoritomodel');
        $this->load->model('Loginmodel');
        $info = $this->Loginmodel->informacionSesion();

        $this->form_validation->set_rules('idAnuncio', 'idAnuncio', 'trim|required|numeric');

        if($info['session'] ==  false) {
            array_push($result['issues'], 'Necesitas estar registrado para realizar esta opeacion');

            echo json_encode($result);
            return;
        }

        if ($this->form_validation->run() == TRUE) {
            if(isset($info['info']['id'])){
                $idUsuario =  $info['info']['id'];
                $idAnuncio =  $this->input->post('idAnuncio');

            $resultadoInsercion = $this->Usuariofavoritomodel->remover($idUsuario, $idAnuncio);
            if ($resultadoInsercion == true) {
                $result['success'] = true;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'Insercion no exitosa');
            }

            } else {

                $issues = $this->form_validation->error_array();
                foreach ($issues as $value) {
                    array_push($result['issues'], $value);
                }

            }
        }

        echo json_encode($result);
    }

}

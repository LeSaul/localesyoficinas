<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class BusquedaController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('SepomexModel', 'sepomex');
    }

    public function indexVenta($page){

        $post = $this->input->post();

        $this->load->library('pagination');

        if($page == 1){
            $offset = 0;
        }else{
            $offset = ($page-1)*8;
        }

        $items = 8;

        $anuncios = $this->db->where('tipo', 'V')->where('fechaFin >', date('Y-m-d')  )->order_by('premium', 'desc')->limit($items,$offset)->get('anuncios');

        $all_anuncios = $this->db->where('tipo', 'V')->where('fechaFin >', date('Y-m-d')  )->get('anuncios');

        $config_pagination = array(
            'first_url'         =>  1,
            'base_url'          =>  base_url('/inmuebles_encontrados/venta'),
            'total_rows'        =>  $all_anuncios->num_rows(),
            'uri_segment'       =>  3,
            'use_page_numbers'  =>  true,
            'per_page'          =>  8,
            'attributes'        =>  array('class'=>"page-link"),
            'full_tag_open'     =>  "<ul class='pagination justify-content-end'>",
            'full_tag_close'    =>  "</ul>",
            'num_tag_open'      =>  '<li class="page-item">',
            'num_tag_close'     =>  '</li>',
            'cur_tag_open'      =>  '<li class="page-item active"><a class="page-link" href="javascript:void(0)">',
            'cur_tag_close'     =>  "</a></li>",
            'next_link'         =>  "<i class='fa fa-chevron-right'></i>",
            'next_tag_open'     =>  '<li class="page-item">',
            'next_tagl_close'   =>  "</li>",
            'prev_link'         =>  "<i class='fa fa-chevron-left'></i>",
            'prev_tag_open'     =>  '<li class="page-item">',
            'prev_tagl_close'   =>  "</li>",
            'first_link'        =>  'Inicio',
            'first_tag_open'    =>  '<li class="page-item">',
            'first_tagl_close'  =>  "</li>",
            'last_link'         =>  'Final',
            'last_tag_open'     =>  '<li class="page-item">',
            'last_tagl_close'   =>  "</li>",
        );

        $this->pagination->initialize($config_pagination);

        $pagination = $this->pagination->create_links();

        $data = array(
			'title'			=>	'Resulados de Búsqueda - Venta',

            'anuncios'      =>  $anuncios->result_array(),

            'resultados'    =>  $anuncios->num_rows(),
			
			'css_plugins'   =>  array('slick/slick.css', 'slick/slick-theme.css'),

            'js_plugins' 	=>  array('slick/slick.js', 'autocomplete/jquery.autocomplete.js', 'jquery-validation/jquery.validate.min.js'),

            'page'			=>	'busqueda_anuncios',
            
            'pagination'    =>  $pagination

		);

		$this->load->view('layout/main', $data);

    }

    public function indexRenta($page){
        $post = $this->input->post();

        $this->load->library('pagination');

        if($page == 1){
            $offset = 0;
        }else{
            $offset = ($page-1)*8;
        }

        $items = 8;

        $anuncios = $this->db->where('tipo', 'R')->where('fechaFin >', date('Y-m-d')  )->order_by('premium', 'desc')->limit($items,$offset)->get('anuncios');

        $all_anuncios = $this->db->where('tipo', 'R')->where('fechaFin >', date('Y-m-d')  )->get('anuncios');

        $config_pagination = array(
            'first_url'         =>  1,
            'base_url'          =>  base_url('/inmuebles_encontrados/renta'),
            'total_rows'        =>  $all_anuncios->num_rows(),
            'uri_segment'       =>  3,
            'use_page_numbers'  =>  true,
            'per_page'          =>  8,
            'attributes'        =>  array('class'=>"page-link"),
            'full_tag_open'     =>  "<ul class='pagination justify-content-end'>",
            'full_tag_close'    =>  "</ul>",
            'num_tag_open'      =>  '<li class="page-item">',
            'num_tag_close'     =>  '</li>',
            'cur_tag_open'      =>  '<li class="page-item active"><a class="page-link" href="javascript:void(0)">',
            'cur_tag_close'     =>  "</a></li>",
            'next_link'         =>  "<i class='fa fa-chevron-right'></i>",
            'next_tag_open'     =>  '<li class="page-item">',
            'next_tagl_close'   =>  "</li>",
            'prev_link'         =>  "<i class='fa fa-chevron-left'></i>",
            'prev_tag_open'     =>  '<li class="page-item">',
            'prev_tagl_close'   =>  "</li>",
            'first_link'        =>  'Inicio',
            'first_tag_open'    =>  '<li class="page-item">',
            'first_tagl_close'  =>  "</li>",
            'last_link'         =>  'Final',
            'last_tag_open'     =>  '<li class="page-item">',
            'last_tagl_close'   =>  "</li>",
        );

        $this->pagination->initialize($config_pagination);

        $pagination = $this->pagination->create_links();

        $data = array(
			'title'			=>	'Resulados de Búsqueda - Venta',

            'anuncios'      =>  $anuncios->result_array(),

            'resultados'    =>  $anuncios->num_rows(),
			
			'css_plugins'   =>  array('slick/slick.css', 'slick/slick-theme.css'),

            'js_plugins' 	=>  array('slick/slick.js', 'autocomplete/jquery.autocomplete.js', 'jquery-validation/jquery.validate.min.js'),

            'page'			=>	'busqueda_anuncios',
            
            'pagination'    =>  $pagination

		);

		$this->load->view('layout/main', $data);


    }
}

<?php
class Inmobiliaria extends CI_Controller {

    public function insertarInmobiliaria() {
        $result = array('error' => true, 'success' => false, 'issues' => []);

        $this->load->library('form_validation');

        $this->load->model('Inmobiliariamodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();
        $resultadoInsercion = null;

        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required|max_length[100]');
        $this->form_validation->set_rules('telefono', 'telefono', 'trim|required|max_length[30]');
        $this->form_validation->set_rules('URL', 'URL', 'trim|max_length[50]');
        $this->form_validation->set_rules('direccion', 'direccion', 'trim|required|max_length[150]');

        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('max_length', 'El campo {field} debe tener como maximo {param} caracteres.');

        if($info['session'] == false) {
            array_push($result['issues'], 'Necesitas estar logueado para realizar esta accion');
        }

        if ($this->form_validation->run() == true && $info['session'] == true) {
            $nombreInmobiliaria = $this->input->post('nombre');
            $telefonoInmobiliaria = $this->input->post('telefono');
            $URL = $this->input->post('URL');
            $direccionInmobiliaria = $this->input->post('direccion');

            $data = array(
                'nombreInmobiliaria' => $this->security->xss_clean($nombreInmobiliaria),
                'telefonoInmobiliaria' => $this->security->xss_clean($telefonoInmobiliaria),
                'URL' => $this->security->xss_clean($URL),
                'logoPath' => '',
                'direccionInmobiliaria' => $this->security->xss_clean($direccionInmobiliaria)
            );

             $resultadoInsercion = $this->Inmobiliariamodel->insertarInmobiliaria($data);

            if ($resultadoInsercion == true) {
                $idInmobiliaria = $this->Inmobiliariamodel->ultimoInsertado();

                $data = array(
                    'idUsuario' => $info['info']['id'],
                    'idInmobiliaria' => $idInmobiliaria
                );

                $this->Inmobiliariamodel->ligarInmobiliaria($data);

                $result['id'] = $idInmobiliaria;
                $result['success'] = true;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'Insercion no exitosa');
            }
        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }

        echo json_encode($result);
    }

    public function obtenerInformacion() {
        $result = array('error' => true, 'success' => false, 'issues' => []);

        $this->load->library('form_validation');
        $this->load->model('Inmobiliariamodel');

        $this->form_validation->set_rules('id', 'id', 'trim|required|numeric');

        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('numeric', 'El campo {field} debe contener unicamente valores numericos.');

        if($this->form_validation->run() == true) {
            $id = $this->input->post('id');
            $id = $this->security->xss_clean($id);

            $resultado = $this->Inmobiliariamodel->obtenerInformacion($id);
            if($resultado !=  null){
                $result['info'] = $resultado;
                $result['error'] = false;
            }

        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }

        echo json_encode($result);

    }

    public function actualizarInmobiliaria() {
        $result = array('error' => true, 'success' => false, 'issues' => []);

        $this->load->library('form_validation');

        $this->load->model('Inmobiliariamodel');
        $this->load->model('Loginmodel');

        $pertenece = false;
        $info = $this->Loginmodel->informacionSesion();
        $resultadoInsercion = null;

        $this->form_validation->set_rules('id', 'id', 'trim|required|numeric');
        $this->form_validation->set_rules('nombre', 'nombre', 'trim|required|max_length[100]');
        $this->form_validation->set_rules('telefono', 'telefono', 'trim|required|max_length[30]');
        $this->form_validation->set_rules('URL', 'URL', 'trim|max_length[50]');
        $this->form_validation->set_rules('direccion', 'direccion', 'trim|required|max_length[150]');

        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('max_length', 'El campo {field} debe tener como maximo {param} caracteres.');
        $this->form_validation->set_message('numeric', 'El campo {field} debe contener unicamente valores numericos.');

        $id = $this->input->post('id');

        if($info['session'] == true && isset($id) == true) {
            $pertenece = $this->Inmobiliariamodel->verificarPertenencia($info['info']['id'], $this->input->post('id'));

        }

        if ($this->form_validation->run() == true &&  $pertenece) {
            $id = $this->input->post('id');
            $nombreInmobiliaria = $this->input->post('nombre');
            $telefonoInmobiliaria = $this->input->post('telefono');
            $URL = $this->input->post('URL');
            $direccionInmobiliaria = $this->input->post('direccion');

            $data = array(
                'nombreInmobiliaria' => $this->security->xss_clean($nombreInmobiliaria),
                'telefonoInmobiliaria' => $this->security->xss_clean($telefonoInmobiliaria),
                'URL' => $URL,
                'direccionInmobiliaria' => $this->security->xss_clean($direccionInmobiliaria)
            );

             $resultadoInsercion = $this->Inmobiliariamodel->actualizarInmobiliaria($data, $id);

            if ($resultadoInsercion == true) {
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'Insercion no exitosa');
            }
        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }

        echo json_encode($result);
    }

    public function insertarImagenInmobiliaria() {
        $result = array('error' => true, 'success' => false, 'issues' => []);

        $this->load->library('form_validation');

        $this->load->helper("file");

        $this->load->model('Inmobiliariamodel');
        $this->load->model('Loginmodel');

        $pertenece = false;
        $info = $this->Loginmodel->informacionSesion();


        $this->form_validation->set_rules('id', 'id', 'trim|required|numeric');

        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('numeric', 'El atributo {field} necesita ser numerico.');

        $id = $this->input->post('id');

        if($info['session'] == true && isset($id) == true) {
            $pertenece = $this->Inmobiliariamodel->verificarPertenencia($info['info']['id'], $this->input->post('id'));

        } else {
            array_push($result['issues'], 'Es necesario iniciar sesion');
        }

        $imageUpload = isset($_FILES["file"]["tmp_name"]);

        if($imageUpload  == false) {
           array_push($result['issues'], 'El archivo es necesario');
        }

        if ($this->form_validation->run() == TRUE && $imageUpload == TRUE && $pertenece == true){

            $id = $this->input->post('id');
            $path = 'img/agencia/';

            $config['upload_path'] = $path;
            $config['allowed_types'] = 'jpg|jpeg|png|svg';
            $config['max_size'] = '2048';
            $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $config['file_name'] = $id;

            $this->load->library('upload', $config);

            $file = $this->Inmobiliariamodel->obtenerImagenActual($id);
            echo $file;
            if($file != '' && file_exists($file)) {
                unlink($file);
            }



            if(!is_dir($config['upload_path'])){
               mkdir($config['upload_path'], 0777);
            }

            if ($this->upload->do_upload('file')) {

                $data = array(
                    'logoPath' => $path .  $id . '.' . $extension
                );

                $resultadoInsercion =  $this->Inmobiliariamodel->actualizarInmobiliaria($data, $id);
                if($resultadoInsercion == true) {
                    $result['success'] = true;
                    $result['error'] = false;
                } else{
                     array_push($result['issues'], 'Actualizacion no exitosa');
                }

            } else {
                $error = array('error' => $this->upload->display_errors());
                foreach ($error as $value) {
                    array_push($result['issues'], $value);
                }
            }

        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }

        echo json_encode($result);
    }

}


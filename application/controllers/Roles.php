<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Roles extends CI_Controller {
    public function obtenerRoles() {
        $result = array('error' => true);

        $this->load->model('Rolesmodel');

        $resultado = $this->Rolesmodel->obtenerRoles();
        if($resultado !=  null){
            $result['info'] = $resultado;
            $result['error'] = false;
        }

        echo json_encode($result);
    }

    public function insertarRoles() {
        $result = array('error' => true, 'success' => false, 'issues' => []);

        $this->load->library('form_validation');

        $this->load->model('Rolesmodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();

        $id =  $info['info']['id'];
        $resultadoInsercion = null;

        $this->form_validation->set_rules('rol', 'rol', 'trim|required|max_length[50]');

        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('max_length', 'El campo {field} debe tener como maximo {param} caracteres.');

        if($id != 1) {
            array_push($result['issues'], 'No tienes permisos para realizar esta accion');
        }

        if ($this->form_validation->run() == true && $id == 1) {
            $roles = $this->input->post('rol');
            $data = array( 'rolName' => $this->security->xss_clean($roles) );

            $resultadoInsercion = $this->Rolesmodel->insertarRoles($data);
            if ($resultadoInsercion == true) {
                $result['success'] = true;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'Insercion no exitosa');
            }
        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }
        echo json_encode($result);
    }

    public function actualizarInformacion() {
        $result = array('error' => true, 'success' => false, 'issues' => []);

        $this->load->library('form_validation');

        $this->load->model('Rolesmodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();

        $idAdmin =  $info['info']['id'];

        $this->form_validation->set_rules('rol', 'rol', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('id', 'id', 'trim|required|numeric');

        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('max_length', 'El campo {field} debe tener como maximo {param} caracteres.');
        $this->form_validation->set_message('numeric', 'El campo {field} debe tener ser numerico.');

        if($idAdmin != 1) {
            array_push($result['issues'], 'No tienes permisos para realizar esta accion');
        }

        if ($this->form_validation->run() == true && $idAdmin == 1) {
            $roles= $this->input->post('rol');
            $id = $this->security->xss_clean($this->input->post('id'));

            $data = array('rolName' => $this->security->xss_clean($roles));

            $resultadoInsercion = $this->Rolesmodel->actualizarRoles($data, $id);
            if ($resultadoInsercion == true) {
                $result['success'] = true;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'Actualizacion no existosa');
            }
        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }

        echo json_encode($result);
    }

    public function removerRoles() {
        $result = array('error' => true, 'success' => false, 'issues' => []);

        $this->load->library('form_validation');

        $this->load->model('Rolesmodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();
        $idAdmin =  $info['info']['id'];
        $resultadoInsercion = null;

        $this->form_validation->set_rules('id', 'id', 'trim|required|numeric');

        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('numeric', 'El campo {field} debe tener ser numerico.');

        if($idAdmin != 1) {
            array_push($result['issues'], 'No tienes permisos para realizar esta accion');
        }

        if ($this->form_validation->run() == true && $idAdmin == 1) {
            $id = $this->input->post('id');

            $resultado  = $this->Rolesmodel->borrarRoles($id);
            if ($resultado == true) {
                $result['success'] = true;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'Eliminacion no exitosa');
            }
        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }
        echo json_encode($result);
    }
}

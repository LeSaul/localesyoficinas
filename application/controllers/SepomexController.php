<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class SepomexController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('SepomexModel', 'sepomex');
    }

    public function getColonias(){
        $estado = 19;
        $colonia = $this->input->get('query');

        $colonias = $this->sepomex->getColonias($estado, $colonia);

        echo json_encode($colonias, JSON_UNESCAPED_UNICODE);

    }

    public function getCpData($cp){
        $estado_municipio = $this->db->select('idEstado, estado, idMunicipio, municipio')->where('cp', $cp)->get('sepomex')->row_array();
        $colonias = $this->db->select('id, asentamiento as colonia')->where('cp', $cp)->get('sepomex')->result_array();

        $response = array(
            'idEstado'      => $estado_municipio['idEstado'],
            'estado'        => $estado_municipio['estado'],
            'idMunicipio'   => $estado_municipio['idMunicipio'],
            'municipio'     => $estado_municipio['municipio'],
            'colonias'      => $colonias
        );

        echo json_encode($response);

    }

    public function getColoniasbyCP($cp){
    	$colonias = $this->sepomex->getColoniasCP($cp);

        echo json_encode($colonias, JSON_UNESCAPED_UNICODE);
    }
}

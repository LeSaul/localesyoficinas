<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Caracteristicas extends CI_Controller {

    public function insertarCaracteristica() {
        $result = array('error' => true, 'success' => false, 'issues' => []);

        $this->load->library('form_validation');

        $this->load->model('Caracteristicasmodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();
        $resultadoInsercion = false;

        $this->form_validation->set_rules('caracteristica', 'nombre de la caracterisitca', 'trim|required|max_length[50]');

        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('max_length', 'El campo {field} debe tener como maximo {param} caracteres.');

        if($info['session'] == false) {
            array_push($result['issues'], 'Necesitas iniciar sesion para realizar esta accion.');
        }

        if ($this->form_validation->run() == true && $info['session'] == true) {
            $caracteristica = $this->input->post('caracteristica');
            $data = array(
                'caracteristica' => $caracteristica
            );

            if($info['info']['id'] == 1) {
                $resultadoInsercion = $this->Caracteristicasmodel->insertarCaracteristica($data);
            } else {
                array_push($result['issues'], 'No tienes los permisos necesarios para realizar esta operacion.');
            }


            if ($resultadoInsercion == true) {
                $result['success'] = true;
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'Insercion no exitosa');
            }
        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }

        echo json_encode($result);
    }

    public function obtenerCaracteristicas() {
        $result = array('error' => false, 'issues' => []);

        $this->load->library('form_validation');
        $this->load->model('Caracteristicasmodel');

        $result['info'] = $this->Caracteristicasmodel->obtenerCaracteristicas();

        echo json_encode($result);

    }

    public function actualizarCaracteristica() {
        $result = array('error' => true, 'issues' => []);

        $this->load->library('form_validation');

        $this->load->model('Caracteristicasmodel');
        $this->load->model('Loginmodel');

        $info = $this->Loginmodel->informacionSesion();
        $resultadoInsercion = null;

        $this->form_validation->set_rules('id', 'id', 'trim|required|numeric');
        $this->form_validation->set_rules('caracteristica', 'caracteristica', 'trim|required|max_length[50]');


        $this->form_validation->set_message('required', 'El atributo {field} es requerido.');
        $this->form_validation->set_message('max_length', 'El campo {field} debe tener como maximo {param} caracteres.');
        $this->form_validation->set_message('numeric', 'El campo {field} debe contener unicamente valores numericos.');

        $id = $this->input->post('id');

        if($info['session'] == false && isset($id) == false) {
             array_push($result['issues'], 'Necesitas iniciar sesion para realizar esta accion.');
        }

        if ($this->form_validation->run() == true) {
            $id = $this->input->post('id');
            $caracteristica = $this->input->post('caracteristica');

            $data = array(
                'caracteristica' => $this->security->xss_clean($caracteristica)
            );

             $resultadoInsercion = $this->Caracteristicasmodel->actualizarCaracteristica($data, $id);

            if ($resultadoInsercion == true) {
                $result['error'] = false;
            } else {
                array_push($result['issues'], 'Insercion no exitosa');
            }
        } else {
            $issues = $this->form_validation->error_array();
            foreach ($issues as $value) {
                array_push($result['issues'], $value);
            }
        }

        echo json_encode($result);
    }

}

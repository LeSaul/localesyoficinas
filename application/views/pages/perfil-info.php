<section class="slice-sm sct-color-1">
    <div class="profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-4">
                    <div class="sidebar sidebar--style-2 no-border stickyfill">
                            <div class="widget">
                            <!-- Profile picture -->
                            <div class="profile-picture profile-picture--style-2">
                                <img src="../../assets/images/elements/user.png" class="img-center">
                                <a href="#" class="btn-aux">
                                    <i class="ion ion-edit"></i>
                                </a>
                            </div>

                            <!-- Profile details -->
                            <div class="profile-details">
                                <h2 class="heading heading-4 strong-500 profile-name"><?= $this->session->fullname ?></h2>
                                <h3 class="heading heading-6 strong-400 profile-occupation mt-3">Miembro desde: <?= date('m/Y', strtotime($usuario['fecha_registro'])) ?></h3>
                                <?php if($usrInmobiliaria): ?>
                                <h3 class="heading heading-light heading-6 strong-400 profile-location"><?= $inmobiliaria['nombreInmobiliaria'] ?></h3>
                                <?php endif ?>
                            </div>

                            <!-- Profile connect -->
                            <div class="profile-connect mt-4">
                                <a href="#" class="btn btn-styled btn-block btn-rounded btn-base-1">Tel: <?= $usuario['telefono'] == null ? $usuario['celular'] == null ? 'N/A' : $usuario['celular']  : $usuario['telefono'] ?></a>
                                <a href="#" class="btn btn-styled btn-block btn-rounded btn-base-2">Enviar Mensaje</a>
                            </div>

                            <!-- Profile stats -->
                            <div class="profile-stats clearfix">
                                <div class="stats-entry">
                                    <span class="stats-count"><?= $anuncios ?></span>
                                    <span class="stats-label text-uppercase">Anuncios Publicados</span>
                                </div>
                                <div class="stats-entry">
                                    <span class="stats-count"><?= $anuncios_activos ?></span>
                                    <span class="stats-label text-uppercase">Anuncios Activos</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-12">
                                    <h1 class="heading heading-5 text-capitalize strong-500 mb-0">
                                        <a href="#" class="link text-underline--none">
                                            Información General
                                        </a>
                                    </h1>
                                </div>
                                <div class="col-lg-6 col-12">

                                </div>
                            </div>
                        </div>

                        <div class="link-menu link-menu--style-3 py-4 border-bottom">
                            <a href="javascript:void(0)" class="active">Información General</a>
                            <a href="/usuario/transacciones">Transacciones</a>
                            <a href="/usuario/formas-pago">Formas de Pago</a>
                            <a href="/usuario/cambiar-password">Cambiar Contraseña</a>
                        </div>

                        <!-- Account settings -->
                        <div class="row">
                            <div class="col-lg-12">
                                <form class="form-default" data-toggle="validator" role="form">
                                    <!-- General information -->
                                    <div class="card no-border bg-transparent">
                                        <div class="card-title px-0 pb-0 no-border">
                                            <h3 class="heading heading-6 strong-600">
                                                Datos Generales
                                            </h3>
                                            <p class="mt-1 mb-0">
                                                Ayúdanos llenando tus datos completos, esto nos permite ofrecer una mejor experiencia en nuestro sitio web.
                                            </p>
                                        </div>
                                        <div class="card-body px-0">
                                            <div class="row">
                                                <div class="col-md-6 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Nombre(s): </label>
                                                        <input type="text" class="form-control form-control-lg" value="<?= $usuario['nombre'] ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Apellido(s): </label>
                                                        <input type="text" class="form-control form-control-lg" value="<?= $usuario['apellido'] ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr class="mt-0 mb-0">

                                    <!-- Account info -->
                                    <div class="card no-border bg-transparent">
                                        <div class="card-title px-0 pb-0 no-border">
                                            <h3 class="heading heading-6 strong-600">
                                                Información de la cuenta
                                            </h3>
                                            <p class="mt-1 mb-0">
                                                Aquí puedes editar tu email así como tus suscripciones.
                                            </p>
                                        </div>
                                        <div class="card-body px-0">
                                            <div class="row align-items-center">
                                                <div class="col-md-6 col-lg-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Email</label>
                                                        <input type="email" class="form-control form-control-lg" value="<?= $usuario['correo'] ?>" disabled>
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-lg-4">
                                                    <a href="javascript_void(0)" class="link link-sm link--style-2 mt-2" onclick="cambiarEmail()">Cambiar Email</a>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6 col-lg-4">
                                                    <h3 class="heading heading-6 strong-600 mb-4">
                                                        Suscripciones
                                                    </h3>
                                                    <div class="checkbox checkbox-primary">
                                                        <input type="checkbox" name="noticias" id="noticias" class="suscribe" placement="left" >
                                                        <label for="">Boletin Noticias</label>
                                                    </div>
                                                    <div class="checkbox checkbox-primary">
                                                        <input type="checkbox" name="semanal" id="semanal" class="suscribe" placement="left" >
                                                        <label for="">Resumen Semanal</label>
                                                    </div>
                                                    <div class="checkbox checkbox-primary">
                                                        <input type="checkbox" name="notificaciones" id="notificaciones" class="suscribe" placement="left" >
                                                        <label for="">Notificaciones</label>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 pull-right d-flex justify-content-end">
                                                    <button class="btn btn-primary rounded">Guardar Cambios</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>                    
<div class="w-100 hero-mobile animated fadeIn delay-1s">
    <div class="row m-0">
    
        <div class="m-auto text-center pt-4">
            <h1 class="text-white text-center hero-mobile-heading">ENCUENTRA O PUBLICA ANUNCIOS DE LOCALES Y OFICINAS</h1>
            <h2 class="text-white text-center hero-mobile-subheading">FÁCIL Y RÁPIDO</h2>
        </div>
        
        <div class="col-md-12 col-lg-6 offset-lg-3 py-4">
            <div class="container text-center">
                <div class="btn-group" role="group">
                    <button type="button" class="btn-rs btn-rs-mobile btn btn-lg btn-outline-light" data-tipo="V">VENTA</button>
                    <button type="button" class="btn-rs btn-rs-mobile btn btn-lg btn-light activa" data-tipo="R">RENTA</button>
                </div>
            </div>

            <div class="form-container">
                <form action="/inmuebles_encontrados/renta/1" method="POST" id="searchForm">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-6 col-md-12">
                                <div class="form-group px-0 mt-2">
                                    <select name="inmueble" id="inmueble" class="form-control rounded">
                                        <option value="">Tipo de Inmueble *</option>
                                        <option value="oficina">OFICINA</option>
                                        <option value="local">LOCAL</option>
                                    </select>
                                </div>
                                
                                <div class="form-group px-0 mt-2">
                                    <select name="municipio" id="municipio" class="form-control rounded">
                                        <option value="">Municipio *</option>
                                        <?php foreach ($municipios as $municipio): ?>
                                            <option value="<?= $municipio->id_municipio ?>"><?= $municipio->nombre_municipio ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group px-0 mt-2">
                                    <input type="text" class="form-control rounded" name="colonia" id="colonia" placeholder="Colonia *">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-12">
                                <div class="input-slider-container">

                                    <div class="row mt-3">
                                        <label class="text-white px-3">Precio Máximo: $ <span id="precio-max-slider-value" class="range-slider-value" data-range-value-low="0"></span></label>
                                    </div>

                                    <div id="precio-max-slider" class="input-slider" data-range-value-min="0" data-range-value-max="<?= $precios->precio_max ?>"></div>

                                    <input type="text" class="d-none" name="precio_max" id="precio_max">
                                    
                                </div>

                                <div class="input-slider-container">

                                    <div class="row mt-3">
                                        <label class="text-white px-3">Superficie Máxima: <span id="superficie-max-slider-value" class="range-slider-value" data-range-value-low="0"></span> m<sup>2</sup></label>
                                    </div>

                                    <div id="superficie-max-slider" class="input-slider" data-range-value-min="0" data-range-value-max="<?= $superficies->superficie_max ?>"></div>

                                    <input type="text" class="d-none" name="superficie_max" id="superficie_max">
                                    
                                </div>
                            </div>
                        </div>

                        <div class="form-group px-0 mt-2">
                            <button id="search" class="btn btn-rs-submit btn-outline-light btn-lg d-block m-auto text-uppercase">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="w-100 py-4 bg-primary animated fadeIn delay-1s">
    <div class="container">
        <h2 class="text-white text-center heading">Te Recomendamos:</h2>
    </div>
</div>

<div class="container my-4 wow fadeIn">

    <div class="row cols-md-space cols-sm-space cols-xs-space carousel-responsive">
        <?php foreach ($anuncios as $anuncio): ?>
                <div class="slide-container col-md-3 overflow-hidden">
                    <div class="block block--style-3 premium">
                        <div class="block-image relative">
                            <div class="view view-first">
                                <a href="javascript:void(0) relative">
                                    <img src="http://via.placeholder.com/300x200" class="img-anuncio img-fluid m-0">
                                    <a href="javascript:void(0)" class="badge-corner badge-corner-gold">
                                        <span class="fa fa-star"></span>
                                    </a>
                                </a>
                            </div>
                        </div>

                        <div class="aux-info-wrapper border-bottom">
                            <ul class="aux-info">
                                <li class="heading strong-400 text-center">
                                    <i class="icon-real-estate-017"></i> <?= $anuncio['superficie'] ?> m<sup>2</sup></span>
                                </li>
                                <li class="heading strong-400 text-center">
                                    <i class="fa fa-car"></i> 2
                                </li>
                            </ul>
                        </div>

                        <div class="block-body">
                            <h3 class="heading heading-sm">
                                <a href="javascript:void(0)">
                                    <?= $anuncio['titulo'] ?>
                                </a>
                            </h3>
                            <p class="description truncate-text">
                                <?= $anuncio['descripcion'] ?>
                            </p>
                        </div>
                        <div class="block-footer border-top py-3">
                            <div class="row align-items-center">
                                <div class="col-12 text-center">
                                    <span class="block-price">$<?= number_format($anuncio['precio'],2 , '.', ',') ?> MXN</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php endforeach ?>
    </div>
</div>
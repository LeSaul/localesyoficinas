<div class="container-fluid">


<div class="tabs tabs--style-1" role="tabpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
            <a href="#tabOneCentered-1" aria-controls="home" role="tab"  data-toggle="tab" class="nav-link active text-center text-uppercase strong-500">Datos del Inmueble</a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="#tabOneCentered-2" aria-controls="profile" role="tab" data-toggle="tab" class="nav-link text-center text-uppercase strong-500">Agrega Fotos</a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="#tabOneCentered-3" aria-controls="messages" role="tab" data-toggle="tab" class="nav-link text-center text-uppercase strong-500">Datos de Contacto</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active animated fadeIn" id="tabOneCentered-1">
            <div class="tab-body">
                <form id="nuevoAnuncioFrm">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4 form-column">
                                <h5 class="text-center text-dark mb-3">Datos Generales</h5>
                                <div class="container form-group text-center">
                                    <div class="btn-group m0-auto d-block" data-toggle="buttons">
                                        <label class="btn btn-lg btn-primary <?= $anuncio['tipo'] == 'R' ? 'active': '' ?>">
                                            <input type="radio" id="tipo" name="tipo" value="R" > RENTA
                                        </label>
                                        <label class="btn btn-lg btn-primary <?= $anuncio['tipo'] == 'V' ? 'active': '' ?>">
                                            <input type="radio" id="tipo" name="tipo" value="V"> VENTA
                                        </label>
                                    </div>
                                </div>

                                <div class="container-fluid form-group">
                                    <div class="row">
                                        <div class="col-md-6 pl-0 mb-0">
                                            <label for="cp">Código Postal: <span>*</span></label>
                                            <div class="input-group mb-0">
                                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                                <input type="text" id="cp" name="cp" class="form-control form-control-lg" value="<?= $anuncio['cp'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <select name="estado" id="estado" class="form-control">
                                        <option value="">Estado *</option>
                                        <?php foreach($estados as $e): ?>
                                            <option value="<? $e['idEstado']?>" <?= $anuncio['estado'] == $e['idEstado'] ? 'selected' : '' ?> ><?= $e['estado'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>                               

                                <div class="form-group">
                                    <select name="municipio" id="municipio" class="form-control">
                                        <option value="">Municipio *</option>
                                        <?php foreach($municipios as $m): ?>
                                            <option value="<? $m['idMunicipio']?>" <?= $anuncio['municipio'] == $m['idMunicipio'] ? 'selected' : '' ?> ><?= $m['municipio'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <input type="text" id="colonia" name="colonia" class="form-control rounded" value="<?= $anuncio['colonia_nombre'] ?>" readonly>
                                </div>

                                <div class="form-group">
                                    <select name="tipo_inmueble" id="tipo_inmueble" class="form-control rounded">
                                        <option value="">Tipo de Inmueble <span>*</span></option>
                                       <?php foreach ($tipo_inmueble as $tipo): ?>
                                           <option value="<?= $tipo['id_inmueble'] ?>" <?= $anuncio['tipo_inmueble'] == $tipo['id_inmueble'] ? 'selected' : '' ?> ><?= $tipo['nombre_inmueble'] ?></option>
                                       <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h5 class="text-center text-dark mb-3">Características del Inmueble</h5>
    
                                <div class="container-fluid form-group">
                                    <div class="row">
                                        <div class="col-md-6 pl-0">
                                            <label for="precio">Precio: <span>*</span></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                                <input type="text" id="precio" name="precio" class="form-control form-control-lg" value="<?= $anuncio['precio'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6 pr-0">
                                            <label for="superficie">Superficie: <span>*</span></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-real-estate-017"></i></span>
                                                <input type="text" id="superficie" name="superficie" class="form-control form-control-lg" value="<?= $anuncio['superficie'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="obra_gris" id="obra_gris" class="amenities" placement="left" <?= $caracteristicas['obra_gris'] == 1 ? 'checked' : '' ?> >
                                                <label for="obra_gris">Obra Gris</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="servicios_basicos" id="servicios_basicos" class="amenities" placement="right" <?= $caracteristicas['servicios'] == 1 ? 'checked' : '' ?> >
                                                <label for="servicios_basicos">Servicios básicos</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="estacionamiento" id="estacionamiento" class="amenities" placement="left" <?= $caracteristicas['estacionamiento'] == 1 ? 'checked' : '' ?> >
                                                <label for="estacionamiento">Estacionamiento</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="clima" id="clima" class="amenities" placement="right" <?= $caracteristicas['clima'] == 1 ? 'checked' : '' ?> >
                                                <label for="clima">Clima</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="telefono" id="telefono" class="amenities" placement="left" <?= $caracteristicas['telefono'] == 1 ? 'checked' : '' ?> >
                                                <label for="telefono">Teléfono</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="internet" id="internet" class="amenities" placement="right" <?= $caracteristicas['internet'] == 1 ? 'checked' : '' ?> >
                                                <label for="internet">Internet</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="mobiliario" id="mobiliario" class="amenities" placement="left" <?= $caracteristicas['mobiliario'] == 1 ? 'checked' : '' ?> >
                                                <label for="mobiliario">Mobiliario</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="sanitario" id="sanitario" class="amenities" placement="right" <?= $caracteristicas['sanitarios'] == 1 ? 'checked' : '' ?> >
                                                <label for="sanitario">Sanitario</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="otras">Otras:</label>
                                        <input type="text" id="otras" name="otras" class="form-control form-control-lg" <?= $caracteristicas['otras'] ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h5>Título:</h5>
                                <div>
                                    <input type="text" name="titulo" id="titulo" class="form-control rounded" value="<?= $anuncio['titulo'] ?>">
                                </div>
                                <h5>Descripción:</h5>
                                <div >
                                    <textarea name="descripcion" id="descripcion" class="form-control rounded" rows="10" ><?= $anuncio['descripcion'] ?></textarea>
                                </div>
                                <p class="text-right"><small>Máximo 780 caracteres</small></p>
                                <div class="form-group">
                                    <button class="btn btn-block btn-primary btn-lg" id="btnDatosGenerales">CONTINUAR <i class="fa fa-arrow-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane animated fadeIn" id="tabOneCentered-2">
            <div class="tab-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="/anuncios/cargar-imagenes" class="dropzone" enctype="multipart/form-data">
                                <div class="dz-message needsclick">
                                    Suelta aquí los archivos para iniciar la carga
                                    <br>
                                    <span class="note needsclick">También puedes hacer click para buscarlos dentro de tu computadora</span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="container-fluid mt-4">
                        <div class="col-lg-4 d-inline-block">
                            <button type="button" class="btn btn-primary btn-block btn-lg"><i class="fa fa-arrow-left"></i> REGRESAR</button>
                        </div>
                        <div class="col-lg-4 d-inline-block float-right">
                            <button type="button" class="btn btn-primary btn-block btn-lg">CONTINUAR <i class="fa fa-arrow-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane animated fadeIn" id="tabOneCentered-3">
            <div class="tab-body">
                <form id="datosContacto">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4">
                                <h5 class="text-dark text-center">¿Cómo te encuentran?</h5>
                                <div class="form-group">
                                    <input type="text" id="nombre" name="nombre" class="form-control rounded" placeholder="Nombre *" value="<?= $usuario['nombre'] ?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="apellido" name="apellido" class="form-control rounded" placeholder="Apellido *" value="<?= $usuario['apellido'] ?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="email_user" name="email_user" class="form-control rounded" placeholder="Email *" value="<?= $usuario['correo'] ?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="cel" name="cel" class="form-control rounded" placeholder="Celular *" value="<?= $usuario['celular'] ?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="tel" name="tel" class="form-control rounded" placeholder="Teléfono *" value="<?= $usuario['telefono'] ?>">
                                </div>
                                <div class="col-md-6 p-0">
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" name="asesor" id="asesor" placement="right" <?= $usr_inmobiliaria ? 'checked' : '' ?> >
                                        <label for="asesor">Soy Asesor</label>
                                    </div>                                    
                                </div>
                                <button type="submit" class="btn btn-base-1 btn-block">
                                    Publicar
                                </button>
                            </div>
                            <div class="col-lg-4 d-none" id="informacionAsesor">
                                <h5 class="text-dark text-center">Información del Asesor</h5>
                                 <div class="form-group">
                                    <input type="text" id="inmobiliaria" name="inmobiliaria" class="form-control rounded" placeholder="Nombre de la Inmobiliaria *" value="<?= $inmobiliaria['nombreInmobiliaria'] ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <input type="text" id="tel_oficina" name="tel_oficina" class="form-control rounded" placeholder="Teléfono de Oficina *" value="<?= $inmobiliaria['telefonoInmobiliaria'] ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <input type="text" id="url" name="url" class="form-control rounded" placeholder="Página Web *" value="<?= $inmobiliaria['URL'] ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

	
</div>
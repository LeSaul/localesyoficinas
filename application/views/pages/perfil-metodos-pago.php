
<section class="slice-sm sct-color-1">
    <div class="profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-4">
                    <div class="sidebar sidebar--style-2 no-border stickyfill">
                            <div class="widget">
                            <!-- Profile picture -->
                            <div class="profile-picture profile-picture--style-2">
                                <img src="../../assets/images/elements/user.png" class="img-center">
                                <a href="#" class="btn-aux">
                                    <i class="ion ion-edit"></i>
                                </a>
                            </div>

                            <!-- Profile details -->
                            <div class="profile-details">
                                <h2 class="heading heading-4 strong-500 profile-name"><?= $this->session->fullname ?></h2>
                                <h3 class="heading heading-6 strong-400 profile-occupation mt-3">Miembro desde: <?= date('m/Y', strtotime($usuario['fecha_registro'])) ?></h3>
                                <?php if($usrInmobiliaria): ?>
                                <h3 class="heading heading-light heading-6 strong-400 profile-location"><?= $inmobiliaria['nombreInmobiliaria'] ?></h3>
                                <?php endif ?>
                            </div>

                            <!-- Profile connect -->
                            <div class="profile-connect mt-4">
                                <a href="#" class="btn btn-styled btn-block btn-rounded btn-base-1">Teléfono: <?= $usuario['telefono'] ?></a>
                                <a href="#" class="btn btn-styled btn-block btn-rounded btn-base-2">Enviar Mensaje</a>
                            </div>

                            <!-- Profile stats -->
                            <div class="profile-stats clearfix">
                                <div class="stats-entry">
                                    <span class="stats-count"><?= $anuncios ?></span>
                                    <span class="stats-label text-uppercase">Anuncios Publicados</span>
                                </div>
                                <div class="stats-entry">
                                    <span class="stats-count"><?= $anuncios_activos ?></span>
                                    <span class="stats-label text-uppercase">Anuncios Activos</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-12">
                                    <h1 class="heading heading-5 text-capitalize strong-500 mb-0">
                                        <a href="#" class="link text-underline--none">
                                            Formas de Pago
                                        </a>
                                    </h1>
                                </div>
                                <div class="col-lg-6 col-12">

                                </div>
                            </div>
                        </div>

                        <div class="link-menu link-menu--style-3 py-4 border-bottom">
                            <a href="/usuario/mi-perfil">Información General</a>
                            <a href="/usuario/transacciones">Transacciones</a>
                            <a href="javascript:void(0)" class=active>Formas de Pago</a>
                            <a href="/usuario/cambiar-password">Cambiar Contraseña</a>
                        </div>

                        <!-- Cards -->
                        <div class="card no-border bg-transparent">
                            <div class="card-title px-0 pb-0 no-border">
                                <h3 class="heading heading-6 strong-600">
                                    Mis tarjetas
                                </h3>
                            </div>

                            <div class="card-body px-0">
                                <ul class="list-group">
                                    <li class="list-group-item bg-transparent">
                                        <div class="row">
                                            <div class="col-4">
                                                Tarjetas Registradas
                                            </div>

                                            <div class="col-8">
                                                <!-- First card -->
                                                <div class="row mb-3">
                                                    <div class="col-8">
                                                        <img src="../../assets/images/icons/cards/visa.png" width="30" class="mr-1">
                                                        x-1023 (Expira en 11/2022)
                                                    </div>
                                                    <div class="col-4 text-right">
                                                        <a href="#" class="link link--style-3" data-toggle="tooltip" data-original-title="Eliminar tarjeta">
                                                            <i class="ion-trash-a"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                                <!-- Second card -->
                                                <div class="row">
                                                    <div class="col-8">
                                                        <img src="../../assets/images/icons/cards/mastercard.png" width="30" class="mr-1">
                                                        x-3165 (Expira en 09/2021)
                                                    </div>
                                                    <div class="col-4 text-right">
                                                        <a href="#" class="link link--style-3" data-toggle="tooltip" data-original-title="Eliminar tarjeta">
                                                            <i class="ion-trash-a"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item bg-transparent">
                                        <div class="row">
                                            <div class="col-4">
                                                Saldo a favor:
                                            </div>

                                            <div class="col-8">
                                                <strong>$28.00 <small>MXN Pesos</small></strong>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- Add new card -->
                        <div class="widget">
                            <div class="card-title px-0 pb-0 no-border">
                                <h3 class="heading heading-6 strong-600">
                                    Agregar una Forma de Pago
                                </h3>
                            </div>

                            <form class="form-default mt-4" data-toggle="validator" role="form">
                                <div class="card ">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h3 class="heading heading-6 strong-600">Tarjetas de crédito aceptadas:</h3>
                                            </div>

                                            <div class="col-lg-6">
                                                <img src="../../assets/images/icons/cards/mastercard.png" width="40" class="mr-2">
                                                <img src="../../assets/images/icons/cards/visa.png" width="40" class="mr-2">
                                                <img src="../../assets/images/icons/cards/american-express.png" width="40">
                                            </div>
                                        </div>

                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Número de Tarjeta:</label>
                                                    <div class="input-group input-group--style-1">
                                                        <input type="text" class="form-control form-control-lg input-mask" data-mask="0000 0000 0000 0000" placeholder="0000 0000 0000 0000">
                                                        <span class="input-group-addon">
                                                            <i class="ion-card"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Tarjetahabiente:</label>
                                                    <input type="text" class="form-control form-control-lg">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label">Fecha de Expiración</label>
                                                    <input type="text" class="form-control form-control-lg input-mask" data-mask="00/00" placeholder="MM/YY">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label">CVV</label>
                                                    <div class="input-group input-group--style-1">
                                                        <input type="text" class="form-control form-control-lg input-mask" data-mask="000">
                                                        <span class="input-group-addon" data-toggle="popover" title="¿Qué es el código CVV?" data-content="Es un código de tres dígitos el cual se encuentra al reverso de tu tarjeta.">
                                                            <i class="ion-help-circled"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="text-right">
                                            <a href="#" class="btn btn-base-1">Guardar Tarjeta</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>                    
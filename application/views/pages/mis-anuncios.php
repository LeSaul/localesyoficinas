<div class="tabs tabs--style-2" role="tabpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
            <a href="#todos" aria-controls="home" role="tab" data-toggle="tab" class="nav-link active text-center text-normal strong-600">Todos los Anuncios&nbsp;&nbsp;<span class="badge badge-md badge-pill badge-primary"><?= $total_anuncios ?></span> </a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="#activos" aria-controls="profile" role="tab" data-toggle="tab" class="nav-link text-center text-normal strong-600">Anuncios Activos&nbsp;&nbsp;<span class="badge badge-md badge-pill badge-primary"><?= $total_activos ?></span> </a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="#pendientes" aria-controls="messages" role="tab" data-toggle="tab" class="nav-link text-center text-normal strong-600">Anuncios Pendientes&nbsp;&nbsp;<span class="badge badge-md badge-pill badge-primary"><?= $total_pendientes ?></span></a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="#expirados" aria-controls="messages" role="tab" data-toggle="tab" class="nav-link text-center text-normal strong-600">Anuncios Expirados&nbsp;&nbsp;<span class="badge badge-md badge-pill badge-primary"><?= $total_expirados ?></span> </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="todos">
            <div class="tab-body">
                <?php foreach($anuncios as $a):?>
                <div class="card py-2">
                    <div class="card-body">
                        <div class="card-title py-4">
                            <a href="usuario/mis-anuncios/id/<?= $a['idAnuncio'] ?>"><span class="pull-left strong-600">Anuncio # <?= $a['idAnuncio'] ?></span></a>
                            <span class="pull-right">Activo hasta: <?= date('d-m-Y', strtotime($a['fechaFin'])) ?></span>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                        <a href="usuario/mis-anuncios/id/<?= $a['idAnuncio'] ?>"><img class="img-fluid" src="http://via.placeholder.com/200x200" alt="Test"></a>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row strong-600 mb-4">Estado: <?= $a['estado'] ?></div>
                                            <div class="row strong-600 mb-4">Título: <?= $a['titulo'] ?></div>
                                            <div class="row strong-600 mb-4">Precio: $ <?= number_format($a['precio'], 2, '.', ',') ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row strong-600 mb-4">Promocionar este anuncio&nbsp;<i class="fa fa-bullhorn text-danger"></i></div>
                                    <div class="row">
                                        <table>
                                            <tbody>
                                                <tr class="mb-4">
                                                    <td>Destacado&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">
                                                        <select name="periodo" id="periodo">
                                                            <option value="7">7 días</option>
                                                            <option value="15">15 días</option>
                                                            <option value="31">31 días</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="mb-4">
                                                    <td>Resaltar&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">7 días / $15.00</td>
                                                </tr>
                                                <tr class="mb-4">
                                                    <td>Urgente&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">7 días / $19.99</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="activos">
            <div class="tab-body">
            <?php foreach($anuncios_activos as $a):?>
                <div class="card py-2">
                    <div class="card-body">
                        <div class="card-title py-4">
                            <a href="usuario/mis-anuncios/id/<?= $a['idAnuncio'] ?>"><span class="pull-left strong-600">Anuncio # <?= $a['idAnuncio'] ?></span></a>
                            <span class="pull-right">Activo hasta: <?= date('d-m-Y', strtotime($a['fechaFin'])) ?></span>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                        <a href="usuario/mis-anuncios/id/<?= $a['idAnuncio'] ?>"><img class="img-fluid" src="http://via.placeholder.com/200x200" alt="Test"></a>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row strong-600 mb-4">Estado: <?= $a['estado'] ?></div>
                                            <div class="row strong-600 mb-4">Título: <?= $a['titulo'] ?></div>
                                            <div class="row strong-600 mb-4">Precio: $ <?= number_format($a['precio'], 2, '.', ',') ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row strong-600 mb-4">Promocionar este anuncio&nbsp;<i class="fa fa-bullhorn text-danger"></i></div>
                                    <div class="row">
                                        <table>
                                            <tbody>
                                                <tr class="mb-4">
                                                    <td>Destacado&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">
                                                        <select name="periodo" id="periodo">
                                                            <option value="7">7 días</option>
                                                            <option value="15">15 días</option>
                                                            <option value="31">31 días</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="mb-4">
                                                    <td>Resaltar&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">7 días / $15.00</td>
                                                </tr>
                                                <tr class="mb-4">
                                                    <td>Urgente&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">7 días / $19.99</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane " id="pendientes">
            <div class="tab-body">
            <?php foreach($anuncios_pendientes as $a):?>
                <div class="card py-2">
                    <div class="card-body">
                        <div class="card-title py-4">
                            <a href="usuario/mis-anuncios/id/<?= $a['idAnuncio'] ?>"><span class="pull-left strong-600">Anuncio # <?= $a['idAnuncio'] ?></span></a>
                            <span class="pull-right">Activo hasta: <?= date('d-m-Y', strtotime($a['fechaFin'])) ?></span>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                        <a href="usuario/mis-anuncios/id/<?= $a['idAnuncio'] ?>"><img class="img-fluid" src="http://via.placeholder.com/200x200" alt="Test"></a>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row strong-600 mb-4">Estado: <?= $a['estado'] ?></div>
                                            <div class="row strong-600 mb-4">Título: <?= $a['titulo'] ?></div>
                                            <div class="row strong-600 mb-4">Precio: $ <?= number_format($a['precio'], 2, '.', ',') ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row strong-600 mb-4">Promocionar este anuncio&nbsp;<i class="fa fa-bullhorn text-danger"></i></div>
                                    <div class="row">
                                        <table>
                                            <tbody>
                                                <tr class="mb-4">
                                                    <td>Destacado&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">
                                                        <select name="periodo" id="periodo">
                                                            <option value="7">7 días</option>
                                                            <option value="15">15 días</option>
                                                            <option value="31">31 días</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="mb-4">
                                                    <td>Resaltar&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">7 días / $15.00</td>
                                                </tr>
                                                <tr class="mb-4">
                                                    <td>Urgente&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">7 días / $19.99</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="expirados">
            <div class="tab-body">
            <?php foreach($anuncios_expirados as $a):?>
                <div class="card py-2">
                    <div class="card-body">
                        <div class="card-title py-4">
                            <a href="usuario/mis-anuncios/id/<?= $a['idAnuncio'] ?>"><span class="pull-left strong-600">Anuncio # <?= $a['idAnuncio'] ?></span></a>
                            <span class="pull-right">Activo hasta: <?= date('d-m-Y', strtotime($a['fechaFin'])) ?></span>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                        <a href="usuario/mis-anuncios/id/<?= $a['idAnuncio'] ?>"><img class="img-fluid" src="http://via.placeholder.com/200x200" alt="Test"></a>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row strong-600 mb-4">Estado: <?= $a['estado'] ?></div>
                                            <div class="row strong-600 mb-4">Título: <?= $a['titulo'] ?></div>
                                            <div class="row strong-600 mb-4">Precio: $ <?= number_format($a['precio'], 2, '.', ',') ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row strong-600 mb-4">Promocionar este anuncio&nbsp;<i class="fa fa-bullhorn text-danger"></i></div>
                                    <div class="row">
                                        <table>
                                            <tbody>
                                                <tr class="mb-4">
                                                    <td>Destacado&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">
                                                        <select name="periodo" id="periodo">
                                                            <option value="7">7 días</option>
                                                            <option value="15">15 días</option>
                                                            <option value="31">31 días</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="mb-4">
                                                    <td>Resaltar&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">7 días / $15.00</td>
                                                </tr>
                                                <tr class="mb-4">
                                                    <td>Urgente&nbsp;<i class="fa fa-info-circle text-warning"></i></td>
                                                    <td class="text-right">7 días / $19.99</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>

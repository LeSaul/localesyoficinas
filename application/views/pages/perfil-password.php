<section class="slice-sm sct-color-1">
    <div class="profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-4">
                    <div class="sidebar sidebar--style-2 no-border stickyfill">
                            <div class="widget">
                            <!-- Profile picture -->
                            <div class="profile-picture profile-picture--style-2">
                                <img src="../../assets/images/elements/user.png" class="img-center">
                                <a href="#" class="btn-aux">
                                    <i class="ion ion-edit"></i>
                                </a>
                            </div>

                            <!-- Profile details -->
                            <div class="profile-details">
                                <h2 class="heading heading-4 strong-500 profile-name"><?= $this->session->fullname ?></h2>
                                <h3 class="heading heading-6 strong-400 profile-occupation mt-3">Miembro desde: <?= date('m/Y', strtotime($usuario['fecha_registro'])) ?></h3>
                                <?php if($usrInmobiliaria): ?>
                                <h3 class="heading heading-light heading-6 strong-400 profile-location"><?= $inmobiliaria['nombreInmobiliaria'] ?></h3>
                                <?php endif ?>
                            </div>

                            <!-- Profile connect -->
                            <div class="profile-connect mt-4">
                                <a href="#" class="btn btn-styled btn-block btn-rounded btn-base-1">Tel: <?= $usuario['telefono'] == null ? $usuario['celular'] == null ? 'N/A' : $usuario['celular']  : $usuario['telefono'] ?></a>
                                <a href="#" class="btn btn-styled btn-block btn-rounded btn-base-2">Enviar Mensaje</a>
                            </div>

                            <!-- Profile stats -->
                            <div class="profile-stats clearfix">
                                <div class="stats-entry">
                                    <span class="stats-count"><?= $anuncios ?></span>
                                    <span class="stats-label text-uppercase">Anuncios Publicados</span>
                                </div>
                                <div class="stats-entry">
                                    <span class="stats-count"><?= $anuncios_activos ?></span>
                                    <span class="stats-label text-uppercase">Anuncios Activos</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-12">
                                    <h1 class="heading heading-5 text-capitalize strong-500 mb-0">
                                        <a href="#" class="link text-underline--none">
                                            Cambiar Contraseña
                                        </a>
                                    </h1>
                                </div>
                                <div class="col-lg-6 col-12">
                                </div>
                            </div>
                        </div>

                        <div class="link-menu link-menu--style-3 py-4 border-bottom">
                            <a href="/usuario/mi-perfil">Información General</a>
                            <a href="/usuario/transacciones">Transacciones</a>
                            <a href="/usuario/metodos-pago">Formas de Pago</a>
                            <a href="javascript:void(0)" class="active">Cambiar Contraseña</a>
                        </div>

                        <form action="">
                            <div class="card no-border bg-transparent">
                                <div class="card-body px-0">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label">Contraseña Actual</label>
                                                <input type="password" class="form-control form-control-lg">
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label">Nueva Contraseña</label>
                                                <input type="password" class="form-control form-control-lg">
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-lg-4">
                                            <div class="form-group">
                                                <label class="control-label">Confirmar Contraseña</label>
                                                <input type="password" class="form-control form-control-lg">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <a href="#" class="btn btn-base-1">Guardar Cambios</a>
                                    </div>
                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>                    
<div class="container my-4">
    <!-- <div class="container-fluid">
        <div class="row ad-navigation w-100 my-4">
            <div class="nav-filters">
                
            </div>
            <div class="nav-views">
                <ul class="views">                
                    <li class="list-view">
                        Cambiar Vista:
                        <ul class="list-view-sublist">
                            <li>
                                <a href="javascript:void(0)" title="Vista de Lista">
                                    <i class="fa fa-list"></i>
                                </a>
                            </li>
                            <li class="list-view">
                                <a class="active" href="javascript:void(0)" title="Vista de Cuadrícula">
                                    <i class="fa fa-th-large"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div> -->
    
    <div class="container-fluid">
        <div class="row cols-md-space">
            
            <?php if($resultados == 0): ?>
                <div class="container ">
                    <div class="row">
                        <div class="col-12 text-center">
                            <img class="img-fluid" src="/assets/images/elements/search.png" alt="No se encontraron resultados">
                            <div class="col py-4">
                                <h2 class="text-dark">No se encontraron resultados para esta consulta</h2>
                                <h3 class="text-dark">Vuelve a intentarlo</h3>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <?php foreach($anuncios as $a):?>
                <div class="col-lg-3 col-md-6 col-sm-12 col-12 mb-5">
                    <div class="block block--style-3 <?= $a['premium'] == 1 ? 'premium' : '' ?>">
                        <div class="block-image relative">
                            <?php if($a['premium'] == 1): ?>
                                <div class="view view-first">
                                    <a href="/anuncio/<?= $a['idUsuario'] ?>/<?= $a['idAnuncio'] ?>">
                                    <img src="/assets/images/prv/real-estate/img-1.jpg" class="img-fluid">
                                        <a href="/anuncio/<?= $a['idUsuario'] ?>/<?= $a['idAnuncio'] ?>" class="badge-corner badge-corner-gold">
                                            <span class="fa fa-star"></span>
                                        </a>
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="view view-first">
                                    <a href="/anuncio/<?= $a['idUsuario'] ?>/<?= $a['idAnuncio'] ?>">
                                        <img src="/assets/images/prv/real-estate/img-1.jpg" class="img-fluid">
                                    </a>
                                </div>
                            <?php endif ?>                        
                            
                            <span class="block-ribbon block-ribbon-fixed block-ribbon-left <?= $a['tipo_uso'] == 'L' ? 'bg-yellow' : 'bg-red' ?>"><?= $a['tipo_uso'] == 'L' ? 'LOCAL' : 'OFICINA' ?></span>
                        </div>

                        <div class="aux-info-wrapper border-bottom">
                            <ul class="aux-info">
                                <li class="heading strong-400 text-center">
                                    <i class="icon-real-estate-017"></i> <?= $a['superficie'] ?> m<sup>2</sup>
                                </li>
                                <li class="heading strong-400 text-center">
                                    <i class="icon-hotel-restaurant-085"></i>
                                </li>
                                <li class="heading strong-400 text-center">
                                    <i class="icon-furniture-032 inactive"></i>
                                </li>
                            </ul>
                        </div>

                        <div class="block-body">
                            <h3 class="heading heading-sm text-truncate">
                                <a href="/anuncio/<?= $a['idUsuario'] ?>/<?= $a['idAnuncio'] ?>">
                                    <?= $a['titulo'] ?>
                                </a>
                            </h3>
                            <p class="description truncate-2-lines">
                                <?= $a['descripcion'] ?>
                            </p>
                        </div>
                        <div class="block-footer border-top py-3">
                            <div class="row align-items-center">
                                <div class="col-12 text-center">
                                    <span class="block-price text-center">$<?= number_format($a['precio'], 2, '.', ',') ?></span>
                                    <span class="block-price-text">
                                        MXN
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach?>
        </div>
    </div>

    <div class="container-fluid">
        <nav aria-label="Right aligned pagination">
            <?= $pagination ?>
        </nav>
    </div>
</div>
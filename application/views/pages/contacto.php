<section class="slice">
    <div class="container">
        <div class="text-center">
            <h2 class="heading heading-2 strong-400">
                Ponte en Contacto
            </h2>
            <p>
                Llamanos o llena el formulario que se muestra abajo y nos pondremos en contacto contigo lo antes posible.
            </p>
            <a href="" class="btn btn-styled btn-xl btn-base-1 btn-icon-left mt-4">
                <i class="fa fa-phone"></i>+52 (81) 4884 2332
            </a>            
        </div>
    </div>
</section>

<section class="slice pt-0 sct-color-1">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <!-- Contact form -->
                <form id="form_contact" data-action="../../php/send-email.php" class="form-default" role="form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                <label for="" class="text-uppercase c-gray-light">Tu nombre</label>
                                <input type="text" name="name" class="form-control form-control-lg" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                <label for="" class="text-uppercase c-gray-light">Tu email</label>
                                <input type="email" name="email" class="form-control form-control-lg" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group has-feedback">
                                <label for="" class="text-uppercase c-gray-light">Inmobiliaria</label>
                                <input type="text" name="website" class="form-control form-control-lg">
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group has-feedback">
                                <label for="" class="text-uppercase c-gray-light">Teléfono</label>
                                <input type="text" name="phone" class="form-control form-control-lg" required>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group has-feedback">
                                <label for=""  class="text-uppercase c-gray-light">Mensaje</label>
                                <textarea name="message" class="form-control no-resize" rows="5" required></textarea>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 d-flex justify-content-center">
                            <button type="submit" class="btn btn-styled btn-base-1 mt-4">Enviar Mensaje</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
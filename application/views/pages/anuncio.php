
<!-- Properties listing -->
<section class="slice sct-color-2">
    <div class="container">
        <div class="anuncio-title">

            <h1>
                Anuncio # <?= $anuncio['idAnuncio'] ?>: <?= $anuncio['titulo'] ?>
            </h1>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="light-gallery">
                <!-- Gallery -->
                <div class="gallery-top">
                    <a href="../../assets/images/prv/real-estate/img-lg-1.jpg" class="item" data-fancybox>
                        <img src="../../assets/images/prv/real-estate/img-lg-1.jpg">
                    </a>
                </div>

                <div class="gallery-bottom">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="gallery-thumb">
                                <a class="item" href="../../assets/images/prv/real-estate/img-1.jpg" data-fancybox="group">
                                    <img src="../../assets/images/prv/real-estate/img-1.jpg">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="gallery-thumb">
                                <a class="item" href="../../assets/images/prv/real-estate/img-2.jpg" data-fancybox="group">
                                    <img src="../../assets/images/prv/real-estate/img-2.jpg">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="gallery-thumb">
                                <a class="item" href="../../assets/images/prv/real-estate/img-3.jpg" data-fancybox="group">
                                    <img src="../../assets/images/prv/real-estate/img-3.jpg">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="gallery-thumb">
                                <a class="item" href="../../assets/images/prv/real-estate/img-4.jpg" data-fancybox="group">
                                    <img src="../../assets/images/prv/real-estate/img-4.jpg">
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="gallery-thumb">
                                <a class="item" href="../../assets/images/prv/real-estate/img-5.jpg" data-fancybox="group">
                                    <img src="../../assets/images/prv/real-estate/img-5.jpg">
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="gallery-thumb">
                                <a class="item" href="../../assets/images/prv/real-estate/img-6.jpg" data-fancybox="group">
                                    <img src="../../assets/images/prv/real-estate/img-6.jpg">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <span class="space-md-md"></span>

            <!-- Description -->
            <div class="card">
                <div class="card-title b-xs-bottom">
                    <h3 class="heading heading-sm text-uppercase">Descripción</h3>
                </div>
                <div class="card-body">
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                    </p>

                    <p>
                        Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.
                    </p>

                </div>
            </div>

            <span class="space-md-md"></span>

            <!-- Address/Location -->
            <div class="card">
                <div class="card-title b-xs-bottom">
                    <h3 class="heading heading-sm text-uppercase">Ubicación</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <table class="table table-no-border table-striped table-responsive">
                                <tbody>
                                    <tr>
                                        <td><strong>Colonia</strong></td>
                                        <td>3015 Grand Avenue, CocoWalk</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Municipio</strong></td>
                                        <td>New York</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Estado</strong></td>
                                        <td>United States</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-6">
                            <!-- insertar mapa aqui -->
                        </div>
                    </div>
                </div>
            </div>

            <span class="space-md-md"></span>

            <!-- Property details -->
            <div class="card">
                <div class="card-title b-xs-bottom">
                    <h3 class="heading heading-sm text-uppercase">Detalles de la Propiedad</h3>
                    <span href="#" class="btn-aux">
                        Última actualización: <?= date('d-m-Y') ?>
                    </span>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-no-border table-striped table-responsive">
                                <tbody>
                                    <tr>
                                        <td><strong>Superficie:</strong> 230 m<sup>2</sup> </td>
                                        <td><strong>Precio Mensual</strong> $23,000.00</td>
                                        <td><strong>Obra Gris</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong>Servicios Incluídos:</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Teléfono</strong></td>
                                        <td><strong>Internet</strong></td>
                                        <td><strong>Agua</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong>Amenidades:</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Mobiliario</strong></td>
                                        <td><strong>Estacionamiento</strong></td>
                                        <td><strong>Clima</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><strong>Available from:</strong> <?= date('d-m-Y') ?></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

        <div class="sidebar-object col-md-3">
            <div class="card">
                <div class="card-title b-xs-bottom text-center">
                    <h3 class="heading heading-sm text-uppercase">Datos del Anunciante</h3>
                </div>
                <div class="anunciante-foto">
                    <img src="http://localesyoficinas.local/assets/images/prv/real-estate/img-2.jpg" alt="<?= $usuario['nombre'] ?>">
                </div>
                <div class="list-group-item">
                    <span class="strong-400 text-dark">Nombre</span>
                    <span class="pull-right text-dark"><?= $usuario['nombre'] ?> <?= $usuario['apellido'] ?></span>
                </div>
                <div class="list-group-item">
                    <span class="strong-400 text-dark">Tipo de Anunciante</span>
                    <span class="pull-right text-dark"><?= $usuario['rol_nombre'] ?></span>
                </div>

                <div class="card-body">
                    <a href="#" class="btn btn-block btn-base-1">
                        Enviar Mensaje
                    </a>
                    <a href="#" class="btn btn-block btn-base-1 btn-outline mt-1">
                        Agregar a Favoritos <i class="fa fa-star text-warning"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
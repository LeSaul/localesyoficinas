<div class="container-fluid">


<div class="tabs tabs--style-1" role="tabpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
            <a href="#tabOneCentered-1" aria-controls="home" role="tab"  data-toggle="tab" class="nav-link active text-center text-uppercase strong-500">Paso 1: Datos del Inmueble</a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="#tabOneCentered-2" aria-controls="profile" role="tab" data-toggle="tab" class="nav-link text-center text-uppercase strong-500">Paso 2: Agrega Fotos</a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="#tabOneCentered-3" aria-controls="messages" role="tab" data-toggle="tab" class="nav-link text-center text-uppercase strong-500">Paso 3: Datos de Contacto</a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="#tabOneCentered-4" aria-controls="messages" role="tab" data-toggle="tab" class="nav-link text-center text-uppercase strong-500">Paso 4: Verifica tu Anuncio</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active animated fadeIn" id="tabOneCentered-1">
            <div class="tab-body">
                <form id="nuevoAnuncioFrm">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4 form-column">
                                <h5 class="text-center text-dark mb-3">Datos Generales</h5>
                                <div class="container form-group text-center">
                                    <div class="btn-group m0-auto d-block" data-toggle="buttons">
                                        <label class="btn btn-lg btn-primary active">
                                            <input type="radio" id="tipo_renta" name="tipo" value="R" checked autocomplete="off"> RENTA
                                        </label>
                                        <label class="btn btn-lg btn-primary">
                                            <input type="radio" id="tipo_venta" name="tipo" value="V" autocomplete="off"> VENTA
                                        </label>
                                    </div>
                                </div>

                                <div class="container-fluid form-group">
                                    <div class="row">
                                        <div class="col-md-6 pl-0 mb-0">
                                            <label for="cp">Código Postal: <span>*</span></label>
                                            <div class="input-group mb-0">
                                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                                <input type="text" id="cp" name="cp" class="form-control form-control-lg">
                                            </div>
                                            <p class="d-none" id="error_cp">El código postal debe tener al 5 caracteres</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <select name="estado" id="estado" class="form-control" readonly>
                                        <option value="">Estado *</option>
                                    </select>
                                </div>                               

                                <div class="form-group">
                                    <select name="municipio" id="municipio" class="form-control" readonly>
                                        <option value="">Municipio *</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <select name="colonia" id="colonia" class="form-control">
                                        <option value="">Colonia</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <select name="tipo_inmueble" id="tipo_inmueble" class="form-control rounded">
                                        <option value="">Inmueble <span>*</span></option>
                                       <?php foreach ($tipo_inmueble as $tipo): ?>
                                           <option value="<?= $tipo['id_inmueble'] ?>"><?= $tipo['nombre_inmueble'] ?></option>
                                       <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <select name="tipo_uso" id="tipo_uso" class="form-control" readonly>
                                        <option value="tipo_uso">Uso *</option>
                                        <option value="L">Local</option>
                                        <option value="O">Oficina</option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <h5 class="text-center text-dark mb-3">Características del Inmueble</h5>
    
                                <div class="container-fluid form-group">
                                    <div class="row">
                                        <div class="col-md-6 pl-0">
                                            <label for="precio">Precio: <span>*</span></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                                <input type="text" id="precio" name="precio" class="form-control form-control-lg">
                                            </div>
                                        </div>
                                        <div class="col-md-6 pr-0">
                                            <label for="superficie">Superficie: <span>*</span></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-real-estate-017"></i></span>
                                                <input type="text" id="superficie" name="superficie" class="form-control form-control-lg">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="obra_gris" id="obra_gris" class="amenities" placement="left">
                                                <label for="obra_gris">Obra Gris</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="servicios_basicos" id="servicios_basicos" class="amenities" placement="right">
                                                <label for="servicios_basicos">Servicios básicos</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="estacionamiento" id="estacionamiento" class="amenities" placement="left">
                                                <label for="estacionamiento">Estacionamiento</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="clima" id="clima" class="amenities" placement="right">
                                                <label for="clima">Clima</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="telefono" id="telefono" class="amenities" placement="left">
                                                <label for="telefono">Teléfono</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="internet" id="internet" class="amenities" placement="right">
                                                <label for="internet">Internet</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="mobiliario" id="mobiliario" class="amenities" placement="left">
                                                <label for="mobiliario">Mobiliario</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-primary">
                                                <input type="checkbox" name="sanitario" id="sanitario" class="amenities" placement="right">
                                                <label for="sanitario">Sanitario</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="otras">Otras:</label>
                                        <input type="text" id="otras" name="otras" class="form-control form-control-lg">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h5>Título:</h5>
                                <div>
                                    <input type="text" name="titulo" id="titulo" class="form-control rounded">
                                </div>
                                <h5>Descripción:</h5>
                                <div >
                                    <textarea name="descripcion" id="descripcion" class="form-control rounded summernote" rows="10"></textarea>
                                </div>
                                <p class="text-right"><small>Máximo 780 caracteres</small></p>
                                <div class="form-group">
                                    <button class="btn btn-block btn-primary btn-lg" id="btnDatosGenerales">CONTINUAR <i class="fa fa-arrow-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane animated fadeIn" id="tabOneCentered-2">
            <div class="tab-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="/anuncios/cargar-imagenes" class="dropzone" enctype="multipart/form-data" id="imgFrm">
                                <div class="dz-message needsclick">
                                    Suelta aquí los archivos para iniciar la carga
                                    <br>
                                    <span class="note needsclick">También puedes hacer click para buscarlos dentro de tu computadora</span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="container-fluid mt-4">
                        <div class="col-lg-4 d-inline-block">
                            <button type="button" class="btn btn-primary btn-block btn-lg"><i class="fa fa-arrow-left"></i> REGRESAR</button>
                        </div>
                        <div class="col-lg-4 d-inline-block float-right">
                            <button type="button" class="btn btn-primary btn-block btn-lg">CONTINUAR <i class="fa fa-arrow-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane animated fadeIn" id="tabOneCentered-3">
            <div class="tab-body">
                <form id="datosContacto">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4">
                                <h5 class="text-dark text-center">¿Cómo te encuentran?</h5>
                                <div class="form-group">
                                    <input type="text" id="nombre" name="nombre" class="form-control rounded" placeholder="Nombre *">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="apellido" name="apellido" class="form-control rounded" placeholder="Apellido *">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="email_user" name="email_user" class="form-control rounded" placeholder="Email *">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="cel" name="cel" class="form-control rounded" placeholder="Celular *">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="tel" name="tel" class="form-control rounded" placeholder="Teléfono *">
                                </div>
                                <div class="col-md-6 p-0">
                                    <div class="checkbox checkbox-primary">
                                        <input type="checkbox" name="asesor" id="asesor" placement="right">
                                        <label for="asesor">Soy Asesor</label>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="col-lg-4 d-none" id="informacionAsesor">
                                <h5 class="text-dark text-center">Información del Asesor</h5>
                                 <div class="form-group">
                                    <input type="text" id="inmobiliaria" name="inmobiliaria" class="form-control rounded" placeholder="Nombre de la Inmobiliaria *">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="tel_oficina" name="tel_oficina" class="form-control rounded" placeholder="Teléfono de Oficina *">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="url" name="url" class="form-control rounded" placeholder="Página Web *">
                                </div>
                            </div>
                            <div class="col-lg-4 <?= isset($this->session->correo) ? 'd-none' : '' ?>">
                                <h5 class="text-dark text-center">Crea tu contraseña</h5>
                                <div class="form-group">
                                    <input type="text" id="new_user_password" name="new_user_password" class="form-control rounded" placeholder="Contraseña *">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="new_user_password_confirmation" name="new_user_password_confirmation" class="form-control rounded" placeholder="Confirmar Contraseña *">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane animated fadeIn" id="tabOneCentered-4">
            <div class="tab-body">                
                <div class="pricing-plans pricing-plans--style-3">
                    <div class="row justify-content-center">
                        <div class="col-lg-3">
                            <div class="block block-pricing">
                                <div class="plan-title-wrapper">
                                    <h2 class="plan-title heading heading-5 strong-600">Destacado</h2>
                                    <h3 class="price-tag"><sup>$</sup>50</h3>
                                    <span class="price-tag-subtitle text-normal">/ mes</span>
                                </div>

                                <ul class="text-center">
                                    <li>Publcación en el Home</li>
                                    <li>Primero en las listas de búsqueda</li>
                                    <li>Tu anuncio resalta entre los demás</li>
                                    <li>Recibe mas visitas</li>
                                </ul>
                                <div class="py-3 text-center">
                                    <button type="submit" class="btn btn-styled btn-base-1 btn-icon-right">
                                        Comprar y Publicar
                                        <i class="icon ion-arrow-right-c"></i>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="block block-pricing">
                                <div class="plan-title-wrapper">
                                    <h2 class="plan-title heading heading-5 strong-600">Resaltado</h2>
                                    <h3 class="price-tag"><sup>$</sup>30</h3>
                                    <span class="price-tag-subtitle text-normal">/ mes</span>
                                </div>

                                <ul class="text-center">
                                    <li><s>Publcación en el Home</s></li>
                                    <li><s>Primero en las listas de búsqueda</s></li>
                                    <li>Tu anuncio resalta entre los demás</li>
                                    <li>Recibe mas visitas</li>
                                </ul>

                                <div class="py-3 text-center">
                                    <button type="submit" class="btn btn-styled btn-base-1 btn-icon-right">
                                        Comprar y Publicar
                                        <i class="icon ion-arrow-right-c"></i>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="block block-pricing">
                                <div class="plan-title-wrapper">
                                    <h2 class="plan-title heading heading-5 strong-600">Normal</h2>
                                    <h3 class="price-tag"><sup>$</sup>0</h3>
                                    <span class="price-tag-subtitle text-normal">/ mes</span>
                                </div>

                                <ul class="text-center">
                                    <li><s>Publcación en el Home</s></li>
                                    <li><s>Primero en las listas de búsqueda</s></li>
                                    <li><s>Tu anuncio resalta entre los demás</s></li>
                                    <li><s>Recibe mas visitas</s></li>
                                </ul>

                                <div class="py-3 text-center">
                                    <button type="submit" class="btn btn-styled btn-base-1 btn-icon-right">
                                        Publicar
                                        <i class="icon ion-arrow-right-c"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

	
</div>
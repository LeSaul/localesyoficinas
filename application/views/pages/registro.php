<div class="container-fluid mb-4">

        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        <div class="col-xs-12 col-md-4 offset-md-4">
            <h3 class="text-center">Formulario de Registro</h3>

            <form id="registerFrm">
                <div class="form-group">
                    <label><strong>Nombre:</strong> <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="name"  name="name">
                </div>

                <div class="form-group">
                    <label><strong>Apellidos:</strong> <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="lastname"  name="lastname">
                </div>

                <div class="form-group">
                    <label><strong>Correo:</strong> <span class="text-danger">*</span></label>
                    <input type="email" class="form-control" id="register-email"  name="register-email">
                </div>

                <div class="form-group">
                    <label><strong>Contraseña:</strong> <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" id="register-password"  name="register-password">
                </div>

                <div class="form-group">
                    <label><strong>Confirmar Contraseña:</strong> <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" id="confirm-password"  name="confirm-password">
                </div>

                <div class="text-center form-group">
                    <div class="g-recaptcha" data-sitekey="6LeNMY8UAAAAAOVZk3hFxMyIRil0vs1CihbZUGeo" data-callback="imNotaRobot"></div>
                </div>
                
                <button type="submit" class="btn btn-aqua btn-block" value="Crear Cuenta" id="registerBtn" disabled>CREAR CUENTA</button>
            </form>
        </div>
    </div>

</div>




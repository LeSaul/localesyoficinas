<!-- FOOTER -->
    <footer id="footer" class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-5">
                    <div class="col">
                        <img src="/assets/images/logo/logo-1-c.png">
                        <span class="clearfix"></span>
                        <span class="heading heading-sm c-gray-light strong-400">One template. Infinite solutions.</span>
                    </div>
                </div>

                <div class="col-lg-2">
                    
                </div>

                <div class="col-lg-2">
                    
                </div>

                <div class="col-lg-3">
                    <div class="col">
                        <h4 class="heading heading-xs strong-600 text-uppercase mb-1">
                            Sígue nuestras Redes Sociales
                        </h4>

                        <ul class="social-media social-media--style-1-v4">
                            <li>
                                <a href="#" class="facebook" target="_blank" data-toggle="tooltip" data-original-title="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="instagram" target="_blank" data-toggle="tooltip" data-original-title="Instagram">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="dribbble" target="_blank" data-toggle="tooltip" data-original-title="Dribbble">
                                    <i class="fa fa-dribbble"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="dribbble" target="_blank" data-toggle="tooltip" data-original-title="Github">
                                    <i class="fa fa-github"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div><!-- END: body-wrap -->

<!-- SCRIPTS -->
<a href="#" class="back-to-top btn-back-to-top"></a>

<!-- Core -->
<script src="<?= base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/popper/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/js/vendor/jquery.easing.js') ?>"></script>
<script src="<?= base_url('assets/js/ie10-viewport-bug-workaround.js') ?>"></script>
<script src="<?= base_url('assets/js/slidebar/slidebar.js') ?>"></script>
<script src="<?= base_url('assets/js/classie.js') ?>"></script>

<!-- Bootstrap Extensions -->
<script src="<?= base_url('assets/vendor/bootstrap-notify/bootstrap-growl.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/scrollpos-styler/scrollpos-styler.js') ?>"></script>

<!-- Plugins: Sorted A-Z -->
<script src="<?= base_url('assets/vendor/adaptive-backgrounds/adaptive-backgrounds.js') ?>"></script>
<script src="<?= base_url('assets/vendor/countdown/js/jquery.countdown.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/dropzone/dropzone.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/easy-pie-chart/jquery.easypiechart.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/fancybox/js/jquery.fancybox.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/flatpickr/flatpickr.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/flip/flip.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/footer-reveal/footer-reveal.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/gradientify/jquery.gradientify.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/headroom/headroom.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/headroom/jquery.headroom.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/input-mask/input-mask.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/instafeed/instafeed.js') ?>"></script>
<script src="<?= base_url('assets/vendor/milestone-counter/jquery.countTo.js') ?>"></script>
<script src="<?= base_url('assets/vendor/nouislider/js/nouislider.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/paraxify/paraxify.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/select2/js/select2.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/sticky-kit/sticky-kit.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/swiper/js/swiper.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/textarea-autosize/autosize.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/typeahead/typeahead.bundle.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/typed/typed.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/vide/vide.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/viewport-checker/viewportchecker.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/wow/wow.min.js') ?>"></script>
<script src="<?= base_url('content/vendor/plugins/bower_components/jquery-validation/jquery.validate.min.js') ?>"></script>
<script src="<?= base_url('content/vendor/plugins/bower_components/jquery-validation/additional-methods.min.js') ?>"></script>
<script src="<?= base_url('content/vendor/plugins/bower_components/tooltip-validation/tooltip-validation.min.js') ?>"></script>
<script src="<?= base_url('content/vendor/plugins/bower_components/noty/lib/noty.js') ?>"></script>

<!-- Isotope -->
<script src="<?= base_url('assets/vendor/isotope/isotope.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/imagesloaded/imagesloaded.pkgd.min.js') ?>"></script>



<!-- App JS -->
<script src="<?= base_url('assets/js/boomerang.min.js') ?>"></script>

<script src="<?= base_url('assets/js/app.js') ?>"></script>
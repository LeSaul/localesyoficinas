<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="robots" content="index, follow">
<meta name="description" content="Locales y Oficinas en monterrey, buscar un lugar para tu negocio de manera sencilla y confiable!">
<meta name="keywords" content="locales en monterrey, oficinas en monterrey, oficinas o locales en monterrey, renta de locales monterrey, renta de oficinas en monterrey, oficinas en san pedro, locales en san pedro, rentar oficinas, oficinas bratas, renta de oficinas baratas, renta de locales baratos, locales baratos, locales en monterrey baratos, oficinas para negocio, locales para negocio">
<meta name="author" content="Locales y Oficinas">

<title>
  <?php if(isset($title)): ?>
    <?= $title ?>
  <?php else: ?>
    Locales y Oficinas - Busca un lugar para tu negocio
  <?php endif ?>
</title>

<!-- Page loader -->
<script src="<?= base_url('assets/vendor/pace/js/pace.min.js') ?>"></script>
<link rel="stylesheet" href="<?= base_url('assets/vendor/pace/css/pace-minimal.css') ?>" type="text/css">

<!-- Bootstrap -->
<link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>" type="text/css">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Montserrat|Open+Sans|Quicksand|Raleway|Roboto+Condensed|Rubik|Ubuntu|Vollkorn" rel="stylesheet">

<!-- Plugins -->
<link rel="stylesheet" href="<?= base_url('assets/vendor/swiper/css/swiper.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/vendor/hamburgers/hamburgers.min.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= base_url('assets/vendor/animate/animate.min.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= base_url('assets/vendor/fancybox/css/jquery.') ?>fancybox.min.css">

<!-- Icons -->
<link rel="stylesheet" href="<?= base_url('assets/fonts/font-awesome/css/font-awesome.min.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= base_url('assets/fonts/ionicons/css/ionicons.min.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= base_url('assets/fonts/line-icons/line-icons.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= base_url('assets/fonts/line-icons-pro/line-icons-pro.css') ?>" type="text/css">

<!-- Linea Icons -->
<link rel="stylesheet" href="<?= base_url('assets/fonts/linea/arrows/linea-icons.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= base_url('assets/fonts/linea/basic/linea-icons.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= base_url('assets/fonts/linea/ecommerce/linea-icons.css') ?>" type="text/css">
<link rel="stylesheet" href="<?= base_url('assets/fonts/linea/software/linea-icons.css') ?>" type="text/css">

<!-- Noty -->
<link rel="stylesheet" href="<?= base_url('content/vendor/plugins/bower_components/noty/lib/noty.css') ?>" type="text/css">

<!-- Global style (main) -->
<link id="stylesheet" type="text/css" href="<?= base_url('assets/css/boomerang.min.css') ?>" rel="stylesheet" media="screen">
<link id="stylesheet" type="text/css" href="<?= base_url('assets/css/animate.css') ?>" rel="stylesheet" media="screen">

<!-- Custom style - Remove if not necessary -->
<link type="text/css" href="<?= base_url('assets/css/custom-style.css') ?>" rel="stylesheet">

<!-- Favicon -->
<link href="<?= base_url('assets/images/favicon.png') ?>" rel="icon" type="image/png">

<!-- CSS de plugins -->
<?php if (isset($css_plugins)): ?>
  <?php foreach ($css_plugins as $css_plugin): ?>
  	<link rel="stylesheet" href="/content/vendor/plugins/bower_components/<?= $css_plugin ?>" />
  <?php endforeach ?>
<?php endif ?>

<!-- Estilos de páginas individuales -->
<?php if (isset($css_page)): ?>
  <?php foreach ($css_page as $css): ?>
  	<link rel="stylesheet" href="/content/auth/css/pages/<?= $css ?>" />
  <?php endforeach ?>
<?php endif ?>

<!-- Recaptcha v3 -->
<script src="https://www.google.com/recaptcha/api.js?render=6LdaH6kUAAAAAPrn7iI2dEmY96aGVA73NEH50EqU"></script>
<script>
  grecaptcha.ready(function() {
      grecaptcha.execute('6LdaH6kUAAAAAPrn7iI2dEmY96aGVA73NEH50EqU', {action: '<?= isset($action_page) ? $action_page : "homepage" ?>'}).then(function(t) {
        $.ajax({
          url: '/security/bot-protection',
          method: 'POST',
          data: {
            token: t
          },
          dataType: 'JSON',
          success: function(r){
            console.log(r);
          },
          error: function(xhr){
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log(xhr.response);
          }
        });
      });
  });
</script>


</head>
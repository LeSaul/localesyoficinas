<!DOCTYPE html>
<html>
    <?php $this->load->view('layout/head'); ?>
<body>


<!-- MAIN WRAPPER -->
<div class="body-wrap">

    <?php $this->load->view('layout/header') ?>

    <?php $this->load->view('pages/'.$page); ?>

    <?php $this->load->view('layout/footer') ?>

    <?php if (isset($js_plugins)): ?>
      <?php foreach ($js_plugins as $plugin): ?>
        <script src="/content/vendor/plugins/bower_components/<?= $plugin ?>"></script>
      <?php endforeach ?>
    <?php endif ?>

    <?php if (isset($js_page)): ?>
      <?php foreach ($js_page as $js): ?>
        <script src="/content/auth/js/pages/<?= $js ?>"></script>
      <?php endforeach ?>
    <?php endif ?>

</body>
</html>

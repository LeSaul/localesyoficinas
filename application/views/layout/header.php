<!-- Header -->
<div class="header">
<!-- Global Search -->
<section id="sctGlobalSearch" class="global-search global-search-overlay">
    <div class="container">
        <div class="global-search-backdrop mask-dark--style-2"></div>

        <!-- Search form -->
        <form class="form-horizontal form-global-search z-depth-2-top" role="form">
            <div class="px-4">
                <div class="row">
                    <div class="col-12">
                        <input type="text" class="search-input" placeholder="Type and hit enter ...">
                    </div>
                </div>
            </div>
            <a href="#" class="close-search" data-toggle="global-search" title="Close search bar"></a>
        </form>
    </div>
</section>

<!-- Navbar -->
<nav class="navbar navbar-expand-lg  navbar-light bg-default ">
    <div class="container-fluid navbar-container">
        <!-- Brand/Logo -->
        <a class="navbar-brand" href="<?= base_url() ?>">
            <img src="/assets/images/logo/logo-1-b.png" class="" alt="Locales y Oficinas Monterrey">
        </a>
        
        <div class="d-inline-block">
            <!-- Navbar toggler  -->
            <button class="navbar-toggler hamburger hamburger-js hamburger--spring" type="button" data-toggle="collapse" data-target="#navbar_main" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>

        <div class="collapse navbar-collapse align-items-center justify-content-end" id="navbar_main">
        <!-- Navbar search - For small resolutions -->
        <div class="navbar-search-widget b-xs-bottom py-3 d-lg-none d-none">
            <form class="" role="form">
                <div class="input-group input-group-lg">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                    <button class="btn btn-base-3" type="button">Go!</button>
                    </span>
                </div>
            </form>
        </div>                    
            <ul class="navbar-nav">

                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       Renta
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="/inmuebles/renta/local/1" class="dropdown-item">
                                Locales en Renta
                            </a>
                        </li>
                        <div class="dropdown-divider"></div>
                        <li>
                            <a href="/inmuebles/renta/oficina/1" class="dropdown-item">
                                Oficinas en Renta
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       Venta
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="/inmuebles/venta/local/1" class="dropdown-item">
                                Locales en Venta
                            </a>
                        </li>
                        <div class="dropdown-divider"></div>
                         <li>
                            <a href="/inmuebles/venta/oficina/1" class="dropdown-item">
                                Oficinas en Venta
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="/contacto" class="nav-link">
                       Contacto
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <?php if (isset($this->session->nombre)): ?>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Hola, <?= $this->session->nombre ?>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="javascript:void(0)" class="dropdown-item">
                                        <i class="fa fa-envelope"></i> Mensajes
                                        <span class="badge badge-inline badge-pill badge-danger">12</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/usuario/mis-anuncios/list" class="dropdown-item">
                                        <i class="fa fa-newspaper-o"></i> Mis Anuncios   
                                    </a>
                                </li>
                                <li>
                                    <a href="/usuario/mi-perfil" class="dropdown-item">
                                        <i class="fa fa-user-circle"></i> Mi Perfil   
                                    </a>
                                </li>
                                <div class="dropdown-divider"></div>
                                 <li>
                                    <a href="/cerrar-sesion" class="dropdown-item">
                                        <i class="fa fa-sign-out"></i> Cerrar Sesión
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php else: ?>
                        <a href="#" class="nav-link dropdown-toggle-login" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Iniciar Sesión
                        </a>                   

                        <ul class="dropdown-menu">
                            <li class="dropdown dropdown-submenu">
                                <form id="loginFrm" class="p-2">
                                    <div class="form-group mt-2">
                                        <label for="email">Email:</label>
                                        <input type="text" name="email" id="email" placeholder="Email" class="form-control rounded">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password:</label>
                                        <input type="password" name="password" id="password" placeholder="Password" class="form-control rounded">
                                    </div>
                                    <p class="mb-0"><a href="/registro">Crear Cuenta</a></p>
                                    <p><a href="/recuperar-password">Recuperar Contraseña</a></p>
                                    <div class="height: 100px">
                                        <div class="g-recaptcha" data-sitekey="6LeNMY8UAAAAAOVZk3hFxMyIRil0vs1CihbZUGeo" data-callback="recaptchaLogin"></div>
                                    </div>
                                    <div class="form-group pull-right">
                                    <div class="text-center form-group text-center">
                                        <button class="btn btn-primary rounded" id="loginBtn">Iniciar Sesión</button>
                                    </div>
                                </form>
                            </li>
                        </ul>
                     <?php endif ?>
                </li>

                <!-- <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       Iniciar Sesión
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="../../../html/default/homepages/homepage-portfolio-creative.html" class="dropdown-item">
                                Creative

                            </a>
                        </li>

                        <div class="dropdown-divider"></div>

                        <li class="dropdown dropdown-submenu">
                            <a href="#" class="dropdown-item" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Outer text</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="../../../html/default/portfolio/portfolio-outer-1-col.html">One column</a></li>
                                <li><a class="dropdown-item" href="../../../html/default/portfolio/portfolio-outer-2-cols.html">Two columns</a></li>
                                <li><a class="dropdown-item" href="../../../html/default/portfolio/portfolio-outer-3-cols.html">Three columns</a></li>
                            </ul>
                        </li>
                    </ul>
                </li> -->

            </ul>
        </div>
        <div class="pl-4 d-none d-lg-inline-block">
            <a href="/anuncios/nuevo-anuncio" class="btn btn-styled btn-sm btn-base-1 btn-circle animated heartBeat delay-5s">
                Publica Gratis
            </a>
            </div>
        </div>
    </nav>
</div>
'use strict';

(function() {
    var login = angular.module('localesServices', ['ngCookies']);
        
    login.factory('localesService', ['$rootScope', '$location','$http', '$q', '$window', '$cookies',
    function ($rootScope, $location, $http, $q, $window, $cookies) {
        var layoutServiceFactory = {},
        _logIn = '',
        _registerUser = '',
        _changePassword = '',
        _forgotPassword = '',
        _changeData = '',
        _getAllUserData = '',
        _getMisAnuncios = '',
        _getAnuncioDetalle = '',
        _getAllAnuncios = '',
        _getFavoritos = '',
        _insertarFavorito = '',
        _eliminarFavorito = '',
        _getUserData = '',
        _setCredentials = '',
        _clearCredentials = '';
        
        
        _logIn = function(mail, pswd) {
            var deferred = $q.defer();
            
            var dataObj = {
                correo : mail,
                contrasena : pswd
            };

            
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'login/iniciarSesion',
                data: dataObj
            
            }).success(function(data) {
                deferred.resolve(data);
            })
            .error(function(data) {
                deferred.reject(data);
            }); 


            return deferred.promise;
        };

        _registerUser = function(user) {
            var deferred = $q.defer();
            
            var dataObj = {
                nombre: user.registerName,
                apellido: user.registerLastName,
                correo: user.registerEmail,
                celular: '',
                telefono: '',
                direccion: '',
                contrasena: user.registerPassword,
                idRol: '1'
            };

            
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'usuario/insertarUsuario',
                data: dataObj
            
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 

        
            return deferred.promise;
        };

        _changePassword = function(password) {
            var deferred = $q.defer();
            
            var dataObj = {
                //contrasenaAnterior: password.oldPassword,
                contrasena: password.newPassword
            };

            console.log("en services");
            console.log(dataObj);

            
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'usuario/actualizarContrasena',
                data: dataObj
            
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 


        
            return deferred.promise;
        };


        _changeData = function(user) {
            var deferred = $q.defer();
            
            var dataObj = {
                nombre: user.registerName,
                apellido: user.registerLastName,
                correo: user.registerEmail,
                celular: user.registerCellphone,
                telefono: user.registerPhone,
                direccion: user.registerAddress

            };

            //console.log("en services");
            //console.log(dataObj);

            
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'usuario/actualizarInformacion',
                data: dataObj
            
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 

        
        
            return deferred.promise;
        };

        _getUserData = function() {
            var deferred = $q.defer();
            
            var dataObj = {
                id: $rootScope.globals.currentUser.idUser

            };
            
            
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'usuario/obtenerInformacion',
                data: dataObj
            
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 
        
            return deferred.promise;
        };

        _getAllUserData = function() {
            var deferred = $q.defer();
            
            var dataObj = {
                id: $rootScope.globals.currentUser.idUser
            };
            
            
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'usuario/consultarInformacion',
                data: dataObj
            
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 
        
            return deferred.promise;
        };


        _setCredentials = function (idUser, rol, username) {
            //var authdata = Base64.encode(idUser + ':' + username);
         
            $rootScope.globals = {
                currentUser: {
                    isLogged: true,
                    idUser: idUser,
                    rol: rol,
                    username: username
                }
            };

            //$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
            $cookies.username = username;
            $cookies.isLogged = true;
            $cookies.idUser = idUser;
            $cookies.rol = rol;
        };

        _clearCredentials = function () {
            var deferred = $q.defer();

            $rootScope.globals = {};
            $cookies.username = undefined;
            $cookies.isLogged = false;
            $cookies.idUser = undefined;
            $cookies.rol = undefined;

            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'login/cerrarSesion'
            
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 

            return deferred.promise;

        };

        _getMisAnuncios = function(user) {
            var deferred = $q.defer();
                
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'anuncio/obtenerMisAnuncios',
                
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 

        
        
            return deferred.promise;
        };

        _getAllAnuncios = function(user) {
            var deferred = $q.defer();
                
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'anuncio/obtenerTodos',
                
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 

        
        
            return deferred.promise;
        };

        _getAnuncioDetalle = function(idAnuncio) {
            var deferred = $q.defer();
            
            var dataObj = {
                idAnuncio: idAnuncio

            };
            
            
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'anuncio/obtenerInformacionAnuncio',
                data: dataObj
            
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 
        
            return deferred.promise;
        };


        _getFavoritos = function(user) {
            var deferred = $q.defer();
                
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'favorito/obtenerFavoritos',
                
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 

        
        
            return deferred.promise;
        };

        _insertarFavorito = function(anuncio) {
            var deferred = $q.defer();
            var dataObj = {
                idAnuncio: anuncio
            };

            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'favorito/insertarFavorito',
                data: dataObj
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 

        
        
            return deferred.promise;
        };

        _eliminarFavorito = function(anuncio) {
            var deferred = $q.defer();
            var dataObj = {
                idAnuncio: anuncio
            };
                
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'favorito/removerFavorito',
                data: dataObj
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 

        
        
            return deferred.promise;
        };

        _forgotPassword = function(user) {
            var deferred = $q.defer();
            
            var dataObj = {
                usuario: user
            };

            
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8";

            $http({
                method: 'POST',
                url: $rootScope.server_url + 'usuario/cambiarPassword',
                data: dataObj
            
            }).success(function(data) {
                deferred.resolve(data);
                
            })
            .error(function(data) {
                deferred.reject(data);
            }); 

        
            return deferred.promise;
        };

        layoutServiceFactory = {
            logIn : _logIn,
            registerUser : _registerUser,
            changePassword : _changePassword,
            forgotPassword : _forgotPassword,
            changeData : _changeData,
            getUserData : _getUserData,
            getMisAnuncios : _getMisAnuncios,
            getAllAnuncios : _getAllAnuncios,
            getAnuncioDetalle : _getAnuncioDetalle,
            getFavoritos : _getFavoritos,
            insertarFavorito : _insertarFavorito,
            eliminarFavorito : _eliminarFavorito,
            getAllUserData : _getAllUserData,
            setCredentials : _setCredentials,
            clearCredentials : _clearCredentials,
        }
    
    
        return layoutServiceFactory;
    }]);
})();
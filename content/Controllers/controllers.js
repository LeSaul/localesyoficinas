'use strict';

(function() {
	var app = angular.module('localesController', ['ui.bootstrap', 'localesServices']);

	app.controller("mainSearchController", ["$scope", "$rootScope", function($scope, $rootScope){
		$scope.returnData = {};

		$scope.submitForm = function(theForm){
			if(theForm.$invalid) return false;
		}

		$scope.toggleRentaVenta = function(){
			$rootScope.toggleRV = !$rootScope.toggleRV;
		}

	}]);
		
	app.controller('sliderController', ['$scope', '$rootScope', 'localesService', '$location','$http', '$cookies',
 		function sliderCtrl($scope, $rootScope, layoutService, $location, $http, $cookies) {
 			$scope.breakpoints = [
			  {
			    breakpoint: 990,
			    settings: {
			      slidesToShow: 3
			    }
			  }, {
			    breakpoint: 767,
			    settings: {
			      slidesToShow: 2
			    }
			  }
			];	

			$scope.breakpointsOther = [
			  {
			    breakpoint: 990,
			    settings: {
			      slidesToShow: 3
			    }
			  }, {
			    breakpoint: 767,
			    settings: {
			      slidesToShow: 2
			    }
			  }
			];		
	}]);

	app.controller('sliderDemoCtrl', function($scope, $log) {
		
			
			console.log('entre');
			
			$scope.demoVals = {
				sliderExample3:     14,
				sliderExample4:     14,
				sliderExample5:     50,
				sliderExample8:     0.34,
				sliderExample9:     [-0.52, 0.54],
				sliderExample10:     -0.37,
				sliderExample14a:	50,
				sliderExample14b:	50,
				sliderExample15:	[30, 60]
			};

			
		});

	app.controller('logInController', ['$scope', '$rootScope', 'localesService', '$location','$http', '$cookies','$uibModal',
 		function logInCtrl($scope, $rootScope, layoutService, $location, $http, $cookies, $uibModal) {

			$scope.user = '';
 			

			$scope.checkLog = function(){
				
				if($cookies.isLogged == "true"){
					return true;
				}else{
					return false;
				}
				
			}

 			$scope.logInCall = function() {
 				$scope.errorLogIn = false;
 				$scope.errorLogInStr = '';
 				layoutService.logIn($scope.logInEmail, $scope.logInPassword).then(function (data, status) {
 					console.log("------iniciar sesion ------");
 					console.log(data);
		 			if(data.issues.length > 0) {
		 				
		 				$scope.errorLogIn = true;
		 				$scope.errorLogInStr = data.issues;
		 			} else if(data.error) {
		 				
						$scope.errorLogIn = true;
		 				$scope.errorLogInStr = 'Usuario o contraseña incorrecta.';
		 			} else if(data.info) {
		 				
		 				layoutService.setCredentials(data.info.id, data.info.rol, data.info.usuario);
						window.location.href = 'perfilInterno';
		 			}
		 		});
 			}

 			$scope.logOut = function() {
 				console.log("cerrandoo la sesioooooooon");
 				layoutService.clearCredentials();
 				
 				window.location.href = 'inicio';	
 			}

 			$scope.registerCall = function(){
 				$scope.errorRegister = false;
 				$scope.errorRegisterStr = '';
 				var username = $scope.newUser.registerName + " " + $scope.newUser.registerLastName;
 				
 				layoutService.registerUser($scope.newUser).then(function (data, status) {
 					console.log(data);
 					if(!data.error && data.success){
 						layoutService.setCredentials(data.id, '1', username);
						window.location.href = 'perfilInterno';	
 					}else if(data.issues.length > 0) {
 						$scope.errorRegister = true;
		 				$scope.errorRegisterStr = data.issues[0];

 					}
		 			
		 		});

				
			 }
			 
			 $scope.forgotPassCall = function() {
				$scope.errorForgotPassword = false;
 				$scope.errorForgotPasswordStr= "";

				$scope.modalInstance = $uibModal.open({
					templateUrl : 'Content/Views/forgotPasswordModal.php',
					scope: $scope,
					controller: function($scope, $modalInstance, $http, passObj, $timeout) {

						$scope.passObj = passObj;

						$scope.reset = function(form){
							passObj = {
								user: ''
							}
							
						}

						$scope.submit = function(form) {
							console.log('Submiting user info.');
							console.log(form);
							layoutService.forgotPassword($scope.passObj).then(function (data, status) {
			 					console.log(data);
			 					if(!data.error && data.success){
			 						
									 $scope.user = '';
									form.$setPristine();
					 				$modalInstance.dismiss();
   									 
			 					}else if(data.issues.length > 0) {
			 						$scope.errorForgotPassword = true;
					 				$scope.errorForgotPasswordStr = data.issues[0];
					 				console.log($scope.errorForgotPasswordStr);
			 					}
					 			
					 		});

							
						};

						$scope.cancel = function() {
							$modalInstance.dismiss('cancel');
						};


					            
					},
					resolve : {
						passObj : function() {
							return $scope.passObj;
						}
					}
					
				});

			};	

 	}]);


	app.controller('AccountController', ['$scope', '$rootScope', 'localesService', '$location','$http','$uibModal',
 		function AccountCtrl($scope, $rootScope, layoutService, $location, $http, $uibModal) {
 		
 			
			$scope.passObj = {
			  oldPassword: "",
			  newPassword: ""
			};

			$scope.userLogged = {
			  	nombre: "",
	            apellido: "",
	            correo: "",
	            celular: "",
	            telefono: "",
	            direccion: "",
			};

			$scope.changePasswordCall = function() {
				//console.log($uibModal);
				//console.log($scope.passObj);
				$scope.errorchangePassword = false;
 				$scope.errorchangePasswordStr= "";

 				console.log("entre a modal");

				$scope.modalInstance = $uibModal.open({
					templateUrl : 'Content/Views/changePasswordModal.php',
					scope: $scope,
					controller: function($scope, $modalInstance, $http, passObj, $timeout) {

						$scope.passObj = passObj;

						$scope.reset = function(form){
							$scope.passObj = {
								newPassword: '',
								oldPassword: ''
							};
							
						}

						$scope.submit = function(form) {
							console.log('Submiting user info.');
							console.log(form);
							layoutService.changePassword($scope.passObj).then(function (data, status) {
			 					console.log(data);
			 					if(!data.error && data.success){
			 						
			 						//console.log("ingaaaa");
			 						//console.log($scope.passObj.newPassword);
									$scope.passObj = {
										newPassword: '',
										oldPassword: ''
									};
									form.$setPristine();
					 				$modalInstance.dismiss();
   									 
			 					}else if(data.issues.length > 0) {
			 						$scope.errorchangePassword = true;
					 				$scope.errorchangePasswordStr = data.issues[0];
					 				console.log($scope.errorchangePasswordStr);
			 					}
					 			
					 		});

							
						};

						$scope.cancel = function() {
							$modalInstance.dismiss('cancel');
						};


					            
					},
					resolve : {
						passObj : function() {
							return $scope.passObj;
						}
					}
					
				});

			};	

			$scope.changeDataCall = function() {
				//console.log($uibModal);
				//console.log($scope.userData);
				$scope.errorchangeData = false;
 				$scope.errorchangeDataStr= "";

 				console.log("entre a modal");

				$scope.modalInstance = $uibModal.open({
					templateUrl : 'Content/Views/changeDataModal.php',
					scope: $scope,
					controller: function($scope, $modalInstance, $http, userData) {

						$scope.userData = userData;

						$scope.initForm = function(){
							layoutService.getAllUserData().then(function (data, status) {
				 				console.log(data.info);
				 				
				 				$scope.userData = {
									registerName: data.info.nombre,
									registerLastName: data.info.apellido,
									registerEmail: data.info.correo,
									registerCellphone: data.info.celular,
									registerPhone: data.info.telefono,
									registerAddress: data.info.direccion
								};
		 						
 							});							
						}

						$scope.submit = function(form) {
							console.log('Submiting user info.');
							//console.log(form);
							layoutService.changeData($scope.userData).then(function (data, status) {
			 					console.log(data);
			 					if(!data.error && data.success){
			 						
			 						//console.log("ingaaaa");
			 						//console.log($scope.userData.newPassword);
									$scope.userData = {
										registerName: '',
										registerLastName: '',
										registerEmail: '',
										registerCellphone: '',
										registerPhone: '',
										registerAddress: ''
									};
									form.$setPristine();
						

									layoutService.getUserData().then(function (data, status) {
				 						//console.log(data.info);
				 						layoutService.setCredentials(data.info.id, '1', data.info.usuario);
		 							});

		 							$scope.displayUserInfo();

					 				$modalInstance.dismiss();
   									 
			 					}else if(data.issues.length > 0) {
			 						$scope.errorchangeData = true;
					 				$scope.errorchangeDataStr = data.issues[0];
					 				console.log($scope.errorchangeDataStr);
			 					}
					 			
					 		});
						};

						$scope.cancel = function() {
							$modalInstance.dismiss('cancel');
						};


					            
					},
					resolve : {
						userData : function() {
							return $scope.userData;
						}
					}
					
				});

			};	

			$scope.displayUserInfo = function() {
				
				layoutService.getAllUserData().then(function (data, status) {
 					$scope.userLogged = {
			            nombre: data.info.nombre,
						apellido: data.info.apellido,
						correo: data.info.correo,
						celular: data.info.celular,
						telefono: data.info.telefono,
						direccion: data.info.direccion
					};
		 		});
		 		
			};

	}]);


 	app.controller('resultListController', ['$scope', '$rootScope', 'localesService', '$location', '$timeout', '$uibModal', '$http', 
 		function resultListCtrl($scope, $rootScope, layoutService, $location, $timeout, $uibModal, $http) {


 			
 			$scope.getMisAnunciosCall = function(){
 				$scope.results = "";
 				

 				layoutService.getMisAnuncios().then(function (data, status) {
		 			
				  $scope.results = data.anuncios;
		 			
		 			console.log("los resultados -----");
		 			console.log($scope.results);
		 		});
 			}  	

 			$scope.verAnuncioCall = function(idAnuncio){
 				var dataObj = {
	                id: idAnuncio
	            };

				window.location.href = 'detalleAnuncio?id='+idAnuncio;
				 			
 			}

 			$scope.getAllAnunciosCall = function(){
 				$scope.results = "";
 				

 				layoutService.getAllAnuncios().then(function (data, status) {
		 			
				  $scope.results = data.anuncios;
		 			
		 			console.log("los resultados -----");
		 			console.log($scope.results);
		 		});
 			}  	

 			$scope.filtersCall = function(){
 				$scope.modalInstance = $uibModal.open({
					templateUrl : 'Content/Views/filtersModal.php',
					scope: $scope,
					controller: function($scope, $modalInstance, $http, passObj, $timeout) {
						$scope.selected = undefined;
						$scope.colonias = ['Florida', 'Contry', 'Tec'];
						$scope.selection = [];
									
						$scope.passObj = passObj;

						$scope.reset = function(form){
							$scope.passObj = {
								newPassword: '',
								oldPassword: ''
							};
							
						}

						$scope.submit = function(form) {
							console.log('Submiting filters info.');
							console.log(form);
							
						};

						$scope.cancel = function() {
							$modalInstance.dismiss('cancel');
						};

						$scope.addColonia = function (colonia){
							if(colonia && $scope.selection.length > 0){
								$scope.selection.filter(function(s){
									if(s !== colonia){
										$scope.selection.push(colonia)
									}
								})
							}else{
								$scope.selection.push(colonia)
							}
						}

						$scope.deleteColonia = function (colonia){
							var array = $scope.selection;
							var search_term = colonia;

							for (var i=array.length-1; i>=0; i--) {
								if (array[i] === search_term) {
									array.splice(i, 1);
								}
							}
						}
					},
					resolve : {
						passObj : function() {
							return $scope.passObj;
						}
					}
					
				})
			 }
			 
 
 	}]);


 	app.controller('detalleAnuncioController', ['$scope', '$rootScope', 'localesService', '$location', '$timeout', '$uibModal', '$http', 
 		function detalleAnuncioCtrl($scope, $rootScope, layoutService, $location, $timeout, $uibModal, $http) {
 			$scope.idAnuncioObj = "";
 			$scope.idAnuncio = "";
 			$scope.favoriteDisabled = false;
 			$scope.results = "";

 			$scope.verDetalle = function(){
				$scope.idAnuncioObj = $location.search();
				$scope.idAnuncio = $scope.idAnuncioObj.id;
				console.log($scope.idAnuncio);

				layoutService.getAnuncioDetalle($scope.idAnuncio).then(function (data, status) {
		 			
		 			console.log("get Detalle -----");
		 			console.log(data);
		 			if(data.success){
						$scope.results = data.anuncios[0];
		 			}

		 			console.log($scope.results);
		 		});

 			}

 			$scope.insertarFavoritoCall = function(){
 				
 				layoutService.insertarFavorito($scope.idAnuncio).then(function (data, status) {
		 			
		 			console.log("insertando favoritos -----");
		 			console.log(data);
		 			if(data.success){
		 				$scope.favoriteDisabled = true;
		 			}
		 		});
 			}
 
 	}]);


 	app.controller('misFavoritosController', ['$scope', '$rootScope', 'localesService', '$location', '$timeout', '$window',
 		function misFavoritosCtrl($scope, $rootScope, layoutService, $location, $timeout, $window) {
 			

 			$scope.getFavoritosCall =  function(){
 				
				$scope.results = "";

 				layoutService.getFavoritos().then(function (data, status) {
		 			$scope.results = data.messages;
		 			console.log("los favoritos -----");
		 			console.log(data.messages);
		 		});
 			}

 			$scope.eliminarFavoritoCall = function(idAnuncio){
 				console.log("el id es : ");
 				console.log(idAnuncio);
				layoutService.eliminarFavorito(idAnuncio).then(function (data, status) {
		 			
		 			console.log("eliminando favoritos -----");
		 			console.log(data);
		 			$window.location.reload();
		 			
		 		});
 			}

 			
 
 	}]);

	//Directives
	app.directive('sliderRange', ['$document', '$compile', function ($document, $compile) {

        // Move slider handle and range line
        var moveHandle = function (handle, elem, posX) {
            $(elem).find('.handle.' + handle).css("left", posX + '%');
        };
        var moveRange = function (elem, posMin, posMax) {
            $(elem).find('.range').css("left", posMin + '%');
            $(elem).find('.range').css("width", posMax - posMin + '%');
        };

        return {
            template: '<div class="slider horizontal">' +
                '<div class="range"></div>' +
                '<a class="handle min" ng-mousedown="mouseDownMin($event)"></a>' +
                '<a class="handle max" ng-mousedown="mouseDownMax($event)"></a>' +
                '</div>',
            replace: true,
            restrict: 'E',
            scope: {
                valueMin: "=",
                valueMax: "="
            },
            link: function postLink(scope, element, attrs) {
                // Initilization
                var dragging = false;
                var startPointXMin = 0;
                var startPointXMax = 0;
                var xPosMin = 0;
                var xPosMax = 0;
                var settings = {
                    "min": (typeof (attrs.min) !== "undefined" ? parseInt(attrs.min, 10) : 0),
                    "max": (typeof (attrs.max) !== "undefined" ? parseInt(attrs.max, 10) : 100),
                    "step": (typeof (attrs.step) !== "undefined" ? parseInt(attrs.step, 10) : 1)
                };
                if (typeof (scope.valueMin) == "undefined" || scope.valueMin === '')
                    scope.valueMin = settings.min;

                if (typeof (scope.valueMax) == "undefined" || scope.valueMax === '')
                    scope.valueMax = settings.max;

                // Track changes only from the outside of the directive
                scope.$watch('valueMin', function () {
                    if (dragging) return;
                    xPosMin = (scope.valueMin - settings.min) / (settings.max - settings.min) * 100;
                    if (xPosMin < 0) {
                        xPosMin = 0;
                    } else if (xPosMin > 100) {
                        xPosMin = 100;
                    }
                    moveHandle("min", element, xPosMin);
                    moveRange(element, xPosMin, xPosMax);
                });

                scope.$watch('valueMax', function () {
                    if (dragging) return;
                    xPosMax = (scope.valueMax - settings.min) / (settings.max - settings.min) * 100;
                    if (xPosMax < 0) {
                        xPosMax = 0;
                    } else if (xPosMax > 100) {
                        xPosMax = 100;
                    }
                    moveHandle("max", element, xPosMax);
                    moveRange(element, xPosMin, xPosMax);
                });

                // Real action control is here
                scope.mouseDownMin = function ($event) {
                    dragging = true;
                    startPointXMin = $event.pageX;

                    // Bind to full document, to make move easiery (not to lose focus on y axis)
                    $document.on('mousemove', function ($event) {
                        if (!dragging) return;

                        //Calculate handle position
                        var moveDelta = $event.pageX - startPointXMin;

                        xPosMin = xPosMin + ((moveDelta / element.outerWidth()) * 100);
                        if (xPosMin < 0) {
                            xPosMin = 0;
                        } else if (xPosMin > xPosMax) {
                            xPosMin = xPosMax;
                        } else {
                            // Prevent generating "lag" if moving outside window
                            startPointXMin = $event.pageX;
                        }
                        scope.valueMin = Math.round((((settings.max - settings.min) * (xPosMin / 100)) + settings.min) / settings.step) * settings.step;
                        scope.$apply();

                        // Move the Handle
                        moveHandle("min", element, xPosMin);
                        moveRange(element, xPosMin, xPosMax);
                    });
                    $document.mouseup(function () {
                        dragging = false;
                        $document.unbind('mousemove');
                        $document.unbind('mousemove');
                    });
                };

                scope.mouseDownMax = function ($event) {
                    dragging = true;
                    startPointXMax = $event.pageX;

                    // Bind to full document, to make move easiery (not to lose focus on y axis)
                    $document.on('mousemove', function ($event) {
                        if (!dragging) return;

                        //Calculate handle position
                        var moveDelta = $event.pageX - startPointXMax;

                        xPosMax = xPosMax + ((moveDelta / element.outerWidth()) * 100);
                        if (xPosMax > 100) {
                            xPosMax = 100;
                        } else if (xPosMax < xPosMin) {
                            xPosMax = xPosMin;
                        } else {
                            // Prevent generating "lag" if moving outside window
                            startPointXMax = $event.pageX;
                        }
                        scope.valueMax = Math.round((((settings.max - settings.min) * (xPosMax / 100)) + settings.min) / settings.step) * settings.step;
                        scope.$apply();

                        // Move the Handle
                        moveHandle("max", element, xPosMax);
                        moveRange(element, xPosMin, xPosMax);
                    });

                    $document.mouseup(function () {
                        dragging = false;
                        $document.unbind('mousemove');
                        $document.unbind('mousemove');
                    });
                };
            }
        };
    }]);
})();
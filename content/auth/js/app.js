$("#loginFrm").submit(function(e){
	var form = $(this);
	var btn = $("#loginBtn");

	btn.prop('disabled', true);
	btn.html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>');

	e.preventDefault();

	form.validate({
		rules:{
			email:{
				required: true,
				email: true
			},
			password:{
				required: true
			}
		},

		messages:{
			email:{
				required: "El campo email es obligatorio",
				email: "Ingresa un email válido"
			},
			password:{
				required: "El campo password es obligatorio"
			}
		}
	});

	if (form.valid() == true) {
		$.ajax({
			url: "/login-user",
			type: "POST",
			data: form.serialize(),
			dataType: "JSON",
			success: function(r){
				window.location.reload();
			},	
			error: function(xhr){
				console.log(xhr.stuatus);
				console.log(xhr.responseText);
			}
		});
	}

	btn.html('Iniciar Sesión');
	btn.prop('disabled', false);

});
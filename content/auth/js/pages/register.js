$("#registerFrm").on('submit', function(e){

	$("#registerBtn").prop('disabled', true);

	e.preventDefault();
	var form = $(this);
	
	form.validate({

		rules: {
			name:{
				required: true
			},

			lastname: {
				required: true
			},

			"register-email": {
				required: true,
				email: true
			},

			"register-password": {
				required: true,
				minlength: 6
			},

			"confirm-password": {
				required: true,
				equalTo: '#register-password'
			}
		},

		messages: {
			name:{
				required: "Campo Obligatorio, por favor completa la información",
			},

			lastname: {
				required: "Campo Obligatorio, por favor completa la información",
			},

			"register-email": {
				required: "Campo Obligatorio, por favor completa la información",
				email: "Por favor, ingresa un email válido"
			},

			"register-password": {
				required: "Campo Obligatorio, por favor completa la información",
				minlength: "La contraseña debe contener al menos {0} caracteres"
			},

			"confirm-password": {
				required: "Campo Obligatorio, por favor completa la información",
				equalTo: 'Las contraseñas no coinciden'
			}
		}

	});

	if (form.valid() == true && grecaptcha.getResponse() != '') {
		$.ajax({
			url: '/usuarios/insertar',
			type: 'POST',
			data: form.serialize(),
			dataType: 'JSON',
			success: function(r){
				if (r.error == true) {
					notificacion(r);
					return;
				}
				
				notificacion(r);
				setTimeout(function() { location.replace("/mi-perfil"); }, 1500);
			},
			error: function(xhr){
				console.log(xhr.status);
				console.log(xhr.responseText);
			}
		});
	}

	form[0].reset();
	grecaptcha.reset();
	$("#registerBtn").prop('disabled', true);
});

function imNotaRobot(){
	$("#registerBtn").prop('disabled', false);
}

var notificacion = function(data){

	var msj = data.msj;

	if (true) {}

	var type = data.error == true ? 'error' : 'success';

	new Noty({
		type: type,
		theme: 'mint',
		timeout: 1500,
		progressBar: true,
		layout: 'topRight',
		text: msj,
		animation: {
			open: 'animated bounceInRight',
			close: 'animated bounceOutRight'
		}
	}).show();
}
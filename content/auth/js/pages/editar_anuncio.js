$id_anuncio ='';

// window.onbeforeunload = function(){
// 	return "Si recargas la página, todos los cambios se perderán";
// }

$(document).ready(function(){
	if ($("#asesor").is(':checked')){
		$("#informacionAsesor").removeClass('d-none animated bounceOut');
		$("#informacionAsesor").addClass('animated bounceIn');
		return;
	}

	$("#informacionAsesor").removeClass('animated bounceIn');
	$("#informacionAsesor").addClass('animated bounceOut');
});

$("#asesor").click(function(){
	if ($("#asesor").is(':checked')){
		$("#informacionAsesor").removeClass('d-none animated bounceOut');
		$("#informacionAsesor").addClass('animated bounceIn');
		return;
	}

	$("#informacionAsesor").removeClass('animated bounceIn');
	$("#informacionAsesor").addClass('animated bounceOut');

});

$("#estado").change(function(){

});

$("#colonia").change(function(){
	$.ajax({
		url: '/AnuncioController/getColoniaByName',
		type: 'POST',
		data:{
			estado: $("#estado").val(),
			municipio: $("#municipio").val(),
			colonia: $("#colonia").val()
		},
		dataType: 'JSON',
		success: function(r){
			console.log(r);
		}
	});
});


$('a[role=tab]').on('click', function(e) {
   e.preventDefault();
});

// $("#cp").on('focusout', function(){	
// 	var cp = $(this).val();

// 	if (cp.length == 5) {

// 		$(this).removeClass('is-invalid');
// 		$(this).addClass('is-valid');

// 		$.ajax({
// 		 	url: '/sepomex/get-colonias/'+cp,
// 		 	type: 'GET',
// 		 	dataType: 'JSON',
// 		 	success: function(r){

// 		 		$.each(r, function (i, item) {
// 				    $('#estado').html($('<option>', { 
// 				        value: item.idEstado,
// 				        text : item.estado
// 				    }));
// 				});

// 		 		$.each(r, function (i, item) {
// 				    $('#municipio').html($('<option>', { 
// 				        value: item.idMunicipio,
// 				        text : item.municipio 
// 				    }));
// 				});

// 		 		$.each(r, function (i, item) {
// 				    $('#colonia').append($('<option>', { 
// 				        value: item.id,
// 				        text : item.asentamiento 
// 				    }));
// 				});

// 		 	},
// 		 	error: function(xhr){
// 		 		console.log('Error: '+xhr.error);
// 		 		console.log(xhr.responseText);
// 		 	}
// 		});

// 		return
// 	}

// 	$(this).removeClass('is-valid');
// 	$(this).addClass('is-invalid');

// 	$('#estado').html($('<option>', { 
//         value: '',
//         text : 'Estado *'
//     }));

// 	$('#municipio').html($('<option>', { 
//         value: '',
//         text : 'Municipio *'
//     }));

//     $('#colonia').html($('<option>', { 
//         value: '',
//         text : 'Colonia *'
//     }));
	

// });

$("#nuevoAnuncioFrm").submit(function(e){

	var form = $(this);
	var btn = $("#btnDatosGenerales");

	btn.prop('disabled', true);
	e.preventDefault();

	form.validate({
		errorClass: 'is-invalid',
		vaidClass: 'is-valid',
		rules:{
			tipo: {
				required: true
			},
			cp:{
				required: true,
				maxlength: 5,
				minlength: 5
			},
			estado:{
				required: true
			},
			municipio:{
				required: true
			},
			colonia: {
				required: true
			},
			tipo_inmueble: {
				required: true
			},
			obra_gris:{
				required: function(){
					if ($(".amenities:checked").length >= 2) {
						$(".amenities").tooltip('hide');
						return false;
					}
					$(".amenities").addClass('is-invalid');
					return true;
				}
			},
			servicios_basicos:{
				required: function(){
					if ($(".amenities:checked").length >= 2) {
						$(".amenities").tooltip('hide');
						return false;
					}
					$(".amenities").addClass('is-invalid');
					return true;
				}
			},
			estacionamiento:{
				required: function(){
					if ($(".amenities:checked").length >= 2) {
						$(".amenities").tooltip('hide');
						return false;
					}
					$(".amenities").addClass('is-invalid');
					return true;
				}
			},
			clima:{
				required: function(){
					if ($(".amenities:checked").length >= 2) {
						$(".amenities").tooltip('hide');
						return false;
					}
					$(".amenities").addClass('is-invalid');
					return true;
				}
			},
			telefono:{
				required: function(){
					if ($(".amenities:checked").length >= 2) {
						$(".amenities").tooltip('hide');
						return false;
					}
					$(".amenities").addClass('is-invalid');
					return true;
				}
			},
			internet:{
				required: function(){
					if ($(".amenities:checked").length >= 2) {
						$(".amenities").tooltip('hide');
						return false;
					}
					$(".amenities").addClass('is-invalid');
					return true;
				}
			},
			mobiliario:{
				required: function(){
					if ($(".amenities:checked").length >= 2) {
						$(".amenities").tooltip('hide');
						return false;
					}
					$(".amenities").addClass('is-invalid');
					return true;
				}
			},
			sanitario:{
				required: function(){
					if ($(".amenities:checked").length >= 2) {
						$(".amenities").tooltip('hide');
						return false;
					}
					$(".amenities").addClass('is-invalid');
					return true;
				}
			},
			precio: {
				required: true,
				digits: true 
			},
			superficie:{
				required: true,
				digits: true
			},
			titulo:{
				required: true
			},
			descripcion: {
				required: true
			}
		},

		messages:{
			cp:{
				required: "Campo obligatorio",
				maxlength: "Ingresa solamente 5 dígitos",
				minlength: "Ingresa 5 dígitos",
			},
			estado:{
				required: "Campo obligatorio"
			},
			tipo: {
				required: "Campo obligatorio"
			},
			tipo_inmueble: {
				required: "Campo obligatorio"
			},
			obra_gris:{
				required: "Selecciona mínimo 2"
			},
			servicios_basicos:{
				required: "Selecciona mínimo 2"
			},
			estacionamiento:{
				required: "Selecciona mínimo 2"
			},
			clima:{
				required: "Selecciona mínimo 2"
			},
			telefono:{
				required: "Selecciona mínimo 2"
			},
			internet:{
				required: "Selecciona mínimo 2"
			},
			mobiliario:{
				required: "Selecciona mínimo 2"
			},
			sanitario:{
				required: "Selecciona mínimo 2"
			},
			municipio:{
				required: "Campo obligatorio"
			},
			colonia: {
				required: "Campo obligatorio"
			},
			precio: {
				required: "Campo obligatorio",
				digits: "Ingresa solamente números" 
			},
			superficie:{
				required: "Campo obligatorio",
				digits: "Ingresa solamente números"
			},
			titulo:{
				required: "Campo obligatorio"
			},
			descripcion: {
				required: "Campo obligatorio"
			}
		},

		tooltip_options: {
			obra_gris: {
				placement: "bottom"
			},
			servicios_basicos: {
				placement: "bottom"
			},
			estacionamiento: {
				placement: "bottom"
			},
			clima: {
				placement: "bottom"
			},
			telefono: {
				placement: "bottom"
			},
			internet: {
				placement: "bottom"
			},
			mobiliario: {
				placement: "bottom"
			},
			sanitario: {
				placement: "bottom"
			},
		}
	});

	if (form.valid()) {
		$.ajax({
			url: '/anuncios/registrar',
			type: 'POST',
			data: form.serialize(),
			dataType: 'JSON',
			success: function(r){				
				if (r.error == false) {
					id_anuncio = r.id;
					$('[href="#tabOneCentered-2"]').tab('show');
				}
			},
			error: function(xhr){
				console.log('Error: '+xhr.status);
				console.log(xhr.responseText);
			}
		});
	}else{
		alert("Nah ah ah!, no dijiste las palabras mágicas");
	}

	btn.prop('disabled', false);

});
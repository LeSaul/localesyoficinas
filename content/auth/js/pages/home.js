// Configuración del Slider
$(function() {
    new WOW().init();

    $('.carousel-responsive').slick({
        dots: true,

        true: false,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 4500,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false
                }
            }
        ]
    });
});

$('.carousel-responsive').on('init', function(event, slick, direction) {
    $(".slide-container").removeClass('col-md-3');
    $(".carousel-responsive").show();
    setTimeout(function() {
        $("p.description").addClass('truncate-2-lines');
        $(".heading>a").addClass('truncate-1-lines');
    }, 2000);
});



$(".btn-rs").click(function() {
    var btn = $(this);

    $(".btn-rs").addClass("btn-outline-light");

    $(".btn-rs").removeClass("activa");

    btn.removeClass("btn-outline-light");

    btn.addClass("btn-light activa")

    var tipo = btn.attr('data-tipo') == 'V' ? 'venta' : 'renta';

    $("#searchForm").attr('action', '/inmuebles_encontrados/'+tipo+'/1');

});

$('#colonia').autocomplete({
    serviceUrl: '/sepomex/colonias',
    minChars: 1
});


$("#searchForm").submit(function(e) {

    var form = $(this);

    e.preventDefault();

    $("#precio_max").val($("#precio-max-slider-value").html());
    $("#superficie_max").val($("#superficie-max-slider-value").html());

    form.validate({
        errorClass: 'is-invalid',
        validClass: 'is-valid',
        rules: {
            inmueble: {
                required: true
            },
            municipio: {
                required: true
            }
        },

        messages: {
            inmueble: {
                required: "Debes completar este campo"
            },
            municipio: {
                required: "Debes completar este campo"
            }
        }
    });

    if (form.valid() == true) {
        form[0].submit();
        return;
    }

    console.log("Invalid");
});

function imNotaRobot() {
    $("#search").prop('disabled', false);
}   
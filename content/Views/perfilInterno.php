<!-- starts breadcrumb -->

<div>

  <div class="container">
    <div class="row">
      <div class="col-xs-12">
         <ol class="breadcrumb">
          <li><a href="inicio"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
          <li><a href="#">Mi Perfil</a></li>
        </ol>
      </div>
    </div>
  </div>
  <!-- ends breadcrumb -->

  <div class="container">
      <div class="row">
          <div class="col-xs-12">
            <p class="welcome-text" ng-bind="'Bienvenido, ' + globals.currentUser.username"></p>
          </div>
      </div>

      <!-- starts MIS ANUNCIOS -->
      <section ng-controller="resultListController" ng-init="getMisAnunciosCall()">
        <div class="row" vertilize-container>
          <div class="col-xs-12 col-md-10" vertilize>
            <div class="subtitle-txt clearfix">
              <h3 class="pull-left">Mis Anuncios</h3>
              <div class="pull-right">
                <a href="#" class="hidden-xs btn btn-aqua btn-aqua-inline">Ver todos los anuncios</a>
                <a href="#" class="hidden-xs btn btn-aqua btn-aqua-inline">Nuevo anuncio</a>
                <a href="#" class="visible-xs btn btn-aqua btn-aqua-inline">Ver todos</a>
              </div>
            </div>

             <!-- starts items -->
            <div class="row">
              <div class="main-items-container" ng-controller="sliderController">

                <slick ng-if="results.length" responsive="breakpoints"  dots="true" infinite="false" speed="300" slides-to-show="4" touch-move="true" slides-to-scroll="1"  init-onload="true" data="results">
                
                  <div ng-repeat="resultado in results" class="col-xs-6 col-sm-4 col-lg-3">
                    <div class="main-item" ng-class="{'highlight': resultado.idModo == 2, 'gray': resultado.idModo == 3}">
                        <div class="main-item-top">
                            <p ng-bind="resultado.titulo"></p>
                        </div>
                        <div class="main-item-content">
                            <div class="main-item-inner">
                                <div class="highlight-ribbon"><span>destacado</span></div>
                                <img src="Content/auth/img/dept.png" alt="">
                                <div class="item-location">
                                    <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                    <span>Monterrey</span>
                                </div>
                                <div class="item-info">
                                    <span ng-bind="resultado.precio | currency"></span>
                                    <span class="binding"><span ng-bind="resultado.superficie"></span>m<sup>2</sup></span>
                                </div>

                                <div class="inner-btns">
                                  <a href="#" class="btn btn-blue">Editar</a>
                                  <a href="#" class="btn btn-blue">Destacar</a>
                                  <a href="#" class="btn btn-blue">Resaltar</a>
                                  <a href="#" class="btn btn-blue">Renovar</a>
                                </div>

                            </div>
                        </div>
                        
                    </div>
                  </div>
                  
                  <!--
                  <div class="col-xs-6 col-sm-4 col-lg-3">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>

                                  <div class="inner-btns">
                                    <a href="#" class="btn btn-blue">Editar</a>
                                    <a href="#" class="btn btn-blue">Destacar</a>
                                    <a href="#" class="btn btn-blue">Resaltar</a>
                                    <a href="#" class="btn btn-blue">Renovar</a>
                                  </div>

                              </div>
                          </div>
                          
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-3">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>

                                  <div class="inner-btns">
                                    <a href="#" class="btn btn-blue">Editar</a>
                                    <a href="#" class="btn btn-blue">Destacar</a>
                                    <a href="#" class="btn btn-blue">Resaltar</a>
                                    <a href="#" class="btn btn-blue">Renovar</a>
                                  </div>

                              </div>
                          </div>
                          
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-3">
                      <div class="main-item gray">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>

                                  <div class="inner-btns">
                                    <a href="#" class="btn btn-blue">Editar</a>
                                    <a href="#" class="btn btn-blue">Destacar</a>
                                    <a href="#" class="btn btn-blue">Resaltar</a>
                                    <a href="#" class="btn btn-blue expired">Anuncio Vigente</a>
                                  </div>

                              </div>
                          </div>
                          
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-3">
                      <div class="main-item gray">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>

                                  <div class="inner-btns">
                                    <a href="#" class="btn btn-blue">Editar</a>
                                    <a href="#" class="btn btn-blue">Destacar</a>
                                    <a href="#" class="btn btn-blue">Resaltar</a>
                                    <a href="#" class="btn btn-blue expired">Anuncio Vigente</a>
                                  </div>

                              </div>
                          </div>
                          
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-3">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>

                                  <div class="inner-btns">
                                    <a href="#" class="btn btn-blue">Editar</a>
                                    <a href="#" class="btn btn-blue">Destacar</a>
                                    <a href="#" class="btn btn-blue">Resaltar</a>
                                    <a href="#" class="btn btn-blue">Renovar</a>
                                  </div>

                              </div>
                          </div>
                          
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-3">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>

                                  <div class="inner-btns">
                                    <a href="#" class="btn btn-blue">Editar</a>
                                    <a href="#" class="btn btn-blue">Destacar</a>
                                    <a href="#" class="btn btn-blue">Resaltar</a>
                                    <a href="#" class="btn btn-blue">Renovar</a>
                                  </div>

                              </div>
                          </div>
                          
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-3">
                      <div class="main-item gray">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>

                                  <div class="inner-btns">
                                    <a href="#" class="btn btn-blue">Editar</a>
                                    <a href="#" class="btn btn-blue">Destacar</a>
                                    <a href="#" class="btn btn-blue">Resaltar</a>
                                    <a href="#" class="btn btn-blue expired">Anuncio Vigente</a>
                                  </div>

                              </div>
                          </div>
                          
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-3">
                      <div class="main-item gray">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>

                                  <div class="inner-btns">
                                    <a href="#" class="btn btn-blue">Editar</a>
                                    <a href="#" class="btn btn-blue">Destacar</a>
                                    <a href="#" class="btn btn-blue">Resaltar</a>
                                    <a href="#" class="btn btn-blue expired">Anuncio Vigente</a>
                                  </div>

                              </div>
                          </div>
                          
                      </div>
                  </div>

                -->

                </slick> <!-- slick -->
                
              </div>
            </div>
            <!-- ends items -->

          </div>
          <div class="col-xs-12 col-md-2 hidden-xs hidden-sm">
            <div class="google-advertise">
                <div class="square-advertise" vertilize></div>
                <div class="advertise-caption">
                    <a href="#">Tu publicidad en este sitio</a>
                </div>
                
            </div>
          </div>
        </div>
      </section>
      <!-- end MIS ANUNCIOS -->

       <!-- starts MIS MENSAJES -->
      <section>
        <div class="row">
          <div class="col-xs-12 col-md-10">
            <div class="subtitle-txt clearfix mb-20">
              <h3 class="pull-left">Mis Mensajes</h3>
              <div class="pull-right">
                <a href="#" class="hidden-xs btn btn-aqua btn-aqua-inline">ver todos los mensajes</a>
                <a href="#" class="visible-xs btn btn-aqua btn-aqua-inline">ver todos</a>
              </div>
            </div>
            
            <!-- each message -->
            <div class="each-message">
              <div class="row">
                <div class="col-xs-12 col-md-8">
                  <p class="local-name">Local Plaza Lúa</p>
                  <p class="new-message visible-xs">mensaje nuevo</p>
                  <p class="local-number">Anuncio 1504012PLXT</p>
                  <p class="new-message hidden-xs">mensaje nuevo</p>
                </div>
                <div class="hidden-xs col-xs-12 col-md-4">
                  <a href="#" class="btn btn-aqua btn-aqua-inline pull-right">Marcar como contactado</a>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-5 col-md-12">
                  <div class="row message-from">
                    <div class="col-md-4">
                      <p>Alfonso Quiroga Salinas</p>
                    </div>
                    <div class="col-md-3">
                      <p>(818) 340 67 34</p>
                    </div>
                    <div class="col-md-5">
                      <p>poncho.salinas@gmail.com</p>
                    </div>
                  </div>
                </div>
                <div class="visible-xs col-xs-7 col-md-12">
                    <a href="#" class="btn btn-aqua btn-aqua-inline pull-right">Marcar como contactado</a>
                </div>
              </div>

             
              <div class="row">
                <div class="col-md-12">
                  <p class="message-date">01/Abril/2015</p>
                  <p class="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pellentesque sollicitudin facilisis. Duis dapibus justo a orci fermentum, rutrum sollicitudin quam tempor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pellentesque sollicitudin facilisis. Duis dapibus justo a orci fermentum, rutrum sollicitudin quam tempor.</p>
                </div>
              </div>
            </div>
            <!-- end each message -->
           
           <!-- more messages -->
           <div class="each-message">
              <div class="row">
                <div class="col-md-8">
                  <p class="local-name">Local Plaza Lúa</p>
                  <p class="local-number">Anuncio 1504012PLXT</p>
                  <p class="new-message"></p>
                </div>
                <div class="hidden-xs col-xs-12 col-md-4">
                  <a href="#" class="btn btn-aqua btn-aqua-inline pull-right">Marcar como contactado</a>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-5 col-md-12">
                  <div class="row message-from">
                    <div class="col-md-4">
                      <p>Alfonso Quiroga Salinas</p>
                    </div>
                    <div class="col-md-3">
                      <p>(818) 340 67 34</p>
                    </div>
                    <div class="col-md-5">
                      <p>poncho.salinas@gmail.com</p>
                    </div>
                  </div>
                </div>
                <div class="visible-xs col-xs-7 col-md-12">
                    <a href="#" class="btn btn-aqua btn-aqua-inline pull-right">Marcar como contactado</a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <p class="message-date">01/Abril/2015</p>
                  <p class="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pellentesque sollicitudin facilisis. Duis dapibus justo a orci fermentum, rutrum sollicitudin quam tempor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pellentesque sollicitudin facilisis. Duis dapibus justo a orci fermentum, rutrum sollicitudin quam tempor.</p>
                </div>
              </div>
            </div>

            <div class="each-message">
              <div class="row">
                <div class="col-md-8">
                  <p class="local-name">Local Plaza Lúa</p>
                  <p class="local-number">Anuncio 1504012PLXT</p>
                  <p class="new-message"></p>
                </div>
                
                <div class="hidden-xs col-xs-12 col-md-4">
                  <a href="#" class="btn btn-aqua btn-aqua-inline pull-right">Marcar como contactado</a>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-5 col-md-12">
                  <div class="row message-from">
                    <div class="col-md-4">
                      <p>Alfonso Quiroga Salinas</p>
                    </div>
                    <div class="col-md-3">
                      <p>(818) 340 67 34</p>
                    </div>
                    <div class="col-md-5">
                      <p>poncho.salinas@gmail.com</p>
                    </div>
                  </div>
                </div>
                <div class="visible-xs col-xs-7 col-md-12">
                    <span class="contacted-message">Contactado</span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <p class="message-date">01/Abril/2015</p>
                  <p class="message-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pellentesque sollicitudin facilisis. Duis dapibus justo a orci fermentum, rutrum sollicitudin quam tempor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pellentesque sollicitudin facilisis. Duis dapibus justo a orci fermentum, rutrum sollicitudin quam tempor.</p>
                </div>
              </div>
            </div>

           <!-- end more messages -->
           

          </div>
          <div class="col-xs-12 col-md-2"></div>
        </div>
        <div class="row">
          <div class="col-xs-12 visible-xs visible-sm">
            <div class="google-advertise">
                <div class="square-advertise"></div>
                <div class="advertise-caption">
                    <a href="#">Tu publicidad en este sitio</a>
                </div>
                
            </div>
          </div>
        </div>
      </section>
      <!-- ends MIS MENSAJES -->

      <!-- starts MIS FAVORITOS -->
      <section ng-controller="misFavoritosController" ng-init="getFavoritosCall()">
        <div class="row" ng-controller="resultListController" >
          <div class="col-xs-12 col-md-8">
            <div class="subtitle-txt clearfix">
              <h3 class="pull-left">Mis Favoritos</h3>
              <div class="pull-right">
                <a href="#" class="hidden-xs btn btn-aqua btn-aqua-inline">Ver todos mis favoritos</a>
                <a href="#" class="visible-xs btn btn-aqua btn-aqua-inline">Ver todos</a>

              </div>
            </div>

            <!-- starts items -->
            <div class="row">
              <div class="main-items-container">
                <slick ng-if="results.length" responsive="breakpoints"  dots="true" infinite="false" speed="300" slides-to-show="4" touch-move="true" slides-to-scroll="1"  init-onload="true" data="results">

                  <div ng-repeat="resultado in results " class="col-xs-6 col-sm-4 col-lg-3">
                    <div class="main-item" ng-class="{'highlight': resultado.idModo == 2, 'gray': resultado.idModo == 3}">
                        <div class="main-item-top">
                            <p ng-bind="resultado.titulo"></p>
                        </div>
                        <div class="main-item-content">
                            <div class="main-item-inner">
                                <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                <div class="item-location">
                                    <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                    <span>Monterrey</span>
                                </div>
                                <div class="item-info">
                                    <span ng-bind="resultado.precio | currency"></span>
                                    <span class="binding"><span ng-bind="resultado.superficie"></span>m<sup>2</sup></span>
                                </div>
                                <div class="inner-btns">
                                  <button class="btn btn-blue" ng-click="verAnuncioCall(resultado.idAnuncio);">Ver anuncio</button>
                                  <button class="btn btn-blue" ng-click="eliminarFavoritoCall(resultado.idAnuncio);">Eliminar de Favoritos</button>
                                  
                                </div>
                            </div>
                        </div>
                        
                    </div>
                  </div>
                 
                </slick> 
              </div>
            </div>
            <!-- ends items -->

          </div>
          <div class="col-xs-12 col-md-4 hidden-xs hidden-sm">
            <div class="google-advertise">
                <div class="square-advertise"></div>
                <div class="advertise-caption">
                    <a href="#">Tu publicidad en este sitio</a>
                </div>
                
            </div>
          </div>
        </div>
      </section>
      <!-- ends MIS FAVORITOS -->

      <!-- starts MIS DATOS -->
      <section ng-controller="AccountController" >
        <div class="row" ng-init="displayUserInfo()">
          <div class="col-xs-12 col-md-8">
            <div class="subtitle-txt clearfix">
              <h3 class="pull-left">Mis Datos de cuenta</h3>
              <div class="pull-right">
                <button class="btn btn-aqua btn-aqua-inline" ng-click="changeDataCall()">Editar</button>
              </div>
            </div>
            <div class="user-data">
              <p class="user-name">{{userLogged.nombre + " " + userLogged.apellido}}</p>
              <p class="user-email">{{userLogged.correo}}</p>
              <div class="user-phones">

                  <p ng-show="userLogged.telefono > 0">Oficina: <span>{{userLogged.telefono}}</span></p>
                  <p ng-show="userLogged.celular > 0">Celular: <span>{{userLogged.celular}}</span></p>
              </div>
              <a class="user-change-password" ng-click="changePasswordCall()">Cambiar contraseña</a>
            
            </div>

          </div>
          <div class="col-xs-12 col-md-4 hidden-xs hidden-sm">
            <div class="google-advertise">
                <div class="square-advertise"></div>
                <div class="advertise-caption">
                    <a href="#">Tu publicidad en este sitio</a>
                </div>
                
            </div>
          </div>
        </div>

      </section>
      <!-- ends MIS DATOS -->
  </div>

</div>
<!-- starts breadcrumb -->
<div class="container">
  <div class="row">
    <div class="col-xs-12">
       <ol class="breadcrumb">
        <li><a href="inicio"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
        <li><a href="#">Renta - Locales - Monterrey - La Ladrillera</a></li>
      </ol>
    </div>
  </div>
</div>
<!-- ends breadcrumb -->

<div class="container">
  
    <!-- starts SEARCH RESULTS -->
    <section ng-controller="resultListController">
      <div class="row">
        <div class="col-xs-12">
          <div class="list-search-container search-form" ng-controller="mainSearchController" ng-init="getAllAnunciosCall()">
                <h3 class="form-title hidden-xs">Búsqueda</h3>
                <h3 class="navbar-toggle collapsed" data-toggle="collapse" data-target="#resultSearch" aria-expanded="false" aria-controls="navbar">Modificar Búsqueda<span class="glyphicon glyphicon-play" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                </h3>
                <form id="resultSearch" name="resultSearch" class="navbar-collapse collapse" method="post" novalidate action="listaResultados">
                  <div class="row">
                    <div class="col-xs-12 col-md-10">
                      <div class="row">
                        <div class="col-xs-12 col-md-3">
                           <div class="form-group">
                                <label></label>
                                <div class="input-group clearfix">
                                    <input type="button" class="form-control input-group-addon" value="Renta" ng-class="{true: 'active', false: 'disabled'} [toggleRV]" ng-click="toggleRentaVenta()">
                                    <input type="button" class="form-control" value="Venta" ng-class="{false: 'active', true: 'disabled'}[toggleRV]" ng-click="toggleRentaVenta()">
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                          <div class="form-group" ng-class="{'has-error': mainSearch.mainInmueble.$invalid && !mainSearch.mainInmueble.$pristine}">
                              <label for="mainInmueble">Tipo de inmueble</label>
                              <select id="mainInmueble" name="mainInmueble" class="form-control" ng-model="returnData.mainInmueble" required>
                                <option value="">Tipo de inmueble</option>
                                <option>Departamento</option>
                                <option>Casa</option>
                                <option>Oficinas</option>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" ng-class="{'has-error': mainSearch.mainMunicipio.$invalid && !mainSearch.mainMunicipio.$pristine}">
                                <label for="mainMunicipio">Municipio</label>
                                <select id="mainMunicipio" name="mainMunicipio"class="form-control" ng-model="returnData.mainMunicipio"required>
                                  <option value="">Municipio</option>
                                  <option>Monterrey</option>
                                  <option>San Nicolas</option>
                                  <option>Santa Catarina</option>
                                  <option>San Pedro</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group" ng-class="{'has-error': mainSearch.mainColonia.$invalid && !mainSearch.mainColonia.$pristine}">
                              <label for="mainColonia">Colonia</label>
                              <select id="mainColonia" name="mainColonia"class="form-control" ng-model="returnData.mainColonia"required>
                                <option value="">Colonia</option>
                                <option>Contry</option>
                                <option>Paseo Residencial</option>
                                <option>Pedregal La Silla</option>
                                <option>Satelite</option>
                              </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-2">
                   
                      <div class="form-group">
                          <label></label>
                          <input type="submit" class="btn btn-aqua" value="buscar" ng-click="submitForm(mainSearch)"/>
                      </div>
                   
                    </div>
                  </div>
                    
                </form>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-md-8">
          <div class="row">
              <div class="col-sm-7">
                
                <div class="form-group">
                  <div class="clearfix order-container">
                    <label for="ordenarBusqueda">Ordenar por:</label>    
                    <select id="ordenarBusqueda" name="mainInmueble" class="form-control" ng-model="returnData.mainInmueble" required="">
                      <option value="">Precio mayor a menor</option>
                      <option value="">Precio menor a mayor</option>
                      <option>Tipo de Inmueble</option>
                      <option>Precio</option>
                      <option>Colonia</option>
                    </select>
                  </div>
                 
                  <p class="light-txt hidden-xs hidden-sm">Su búsqueda obtuvo <span class="result-quantity">150</span> resultados</p>
                </div>
              </div>
              <div class="col-sm-5 clearfix list-top-pagination">
                <nav class="text-center pull-right">
                  <ul class="no-margin pagination original-style">
                    <li>
                      <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                      <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>
                  </ul>
                </nav>
                <p class="light-txt visible-xs visible-sm text-center">Su búsqueda obtuvo <span class="result-quantity">150</span> resultados</p>
              </div>
          </div>
          
          <!-- starts items -->
          <div class="row">
            <div class="main-items-container">
              <div class="filters-container hidden-xs hidden-sm">
                <div class="filter-blue filter-cont">
                  <!--<div class="filter-blue filter-cont" ng-click="filtersCall()">-->
                  <div class="filter-icon big-icon cursor-pointer" ng-click="filtersCall()">
                    <span class="glyphicon glyphicon-filter" aria-hidden="true"></span>
                    <span>Filtros</span>
                  </div>  
                </div>
                
                <div class="hide filter-green filter-cont">
                  <div class="filter-icon">
                    <span>$</span>
                  </div>
                  <span>$5,000 - $15,000</span>
                </div>
                
                <div class="hide filter-yellow filter-cont">
                  <div class="filter-icon">
                    <span>M<sup>2</sup></span>
                  </div>
                  <span>28 - 80</span>
                </div>

                <div class="hide filter-red filter-cont">
                  <div class="filter-icon">
                    <span class="glyphicon glyphicon-bed" aria-hidden="true"></span>
                  </div>
                </div>

                <div class="hide filter-aqua filter-cont">
                  <div class="filter-icon">
                    <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                  </div>
                </div>
              </div>
              
              <div class="col-xs-12" ng-repeat="resultado in results">
                  <div class="main-item full-item ng-cloak" ng-class="{'highlight': resultado.idModo == 2, 'gray': resultado.idModo == 3}">
                      <div class="main-item-top">
                          <p ng-bind="resultado.titulo"></p>
                          <div class="highlight-ribbon"><span>destacado</span></div>
                      </div>
                      <div class="main-item-content">
                          <div class="main-item-inner">
                              <div class="row">
                                <div class="col-xs-5 col-sm-3">
                                  <slick dots="false">
                                    <div><img src="Content/auth/img/listaResultados/1_thumb.png" alt=""></div>
                                    <div><img src="Content/auth/img/listaResultados/2_thumb.png" alt=""></div>
                                    <div><img src="Content/auth/img/listaResultados/3_thumb.png" alt=""></div>
                                    <div><img src="Content/auth/img/listaResultados/4_thumb.png" alt=""></div>
                                  </slick>  
                                </div>
                                <div class="col-xs-7 col-sm-9">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-5">
                                      <div class="item-location">
                                          <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                          <span>Monterrey</span>
                                      </div>
                                      <div class="item-location-st">
                                        <p>Col. Ladrillera</p>
                                      </div>
                                      <div class="item-info">
                                          <span ng-bind="resultado.precio | currency"></span>
                                      </div> 
                                      <div class="item-published clearfix">
                                          <span class="pull-left item-info visible-xs item-length binding"><span ng-bind="resultado.superficie"></span>m<sup>2</sup></span>
                                          <p class="pull-right binding">Publicado el: <span ng-bind="resultado.fechaInicio | date:'dd/MMM/yyyy'"></span> </p>

                                      </div> 
                                    </div>
                                    <div class="col-sm-7 hidden-xs">
                                      <p ng-bind="resultado.descripcion"></p>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <div class="inner-btns">
                                        <button class="pull-right btn btn-blue" ng-click="verAnuncioCall(resultado.idAnuncio);">Ver inmueble</button>
                                      </div>    
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                      
                  </div>
              </div>

              <div class="col-xs-12">
                  <div class="main-item full-item ">
                      <div class="main-item-top">
                          <p>DEPARTAMENTO EN RENTA EN ...</p>
                          <div class="highlight-ribbon"><span>destacado</span></div>
                      </div>
                      <div class="main-item-content">
                          <div class="main-item-inner">
                              <div class="row">
                                <div class="col-xs-5 col-sm-3">
                                  <slick dots="false">
                                    <div><img src="Content/auth/img/listaResultados/1_thumb.png" alt=""></div>
                                    <div><img src="Content/auth/img/listaResultados/2_thumb.png" alt=""></div>
                                    <div><img src="Content/auth/img/listaResultados/3_thumb.png" alt=""></div>
                                    <div><img src="Content/auth/img/listaResultados/4_thumb.png" alt=""></div>
                                  </slick>  
                                </div>
                                <div class="col-xs-7 col-sm-9">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-5">
                                      <div class="item-location">
                                          <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                          <span>Monterrey</span>
                                      </div>
                                      <div class="item-location-st">
                                        <p>Col. Ladrillera</p>
                                      </div>
                                      <div class="item-info">
                                          <span>$13,000</span>
                                      </div> 
                                      <div class="item-published clearfix">
                                          <span class="pull-left item-info visible-xs item-length">12m<sup>2</sup></span>  
                                          <p class="pull-right">Publicado el: <span>03/Marzo/2015</span> </p>
                                      </div> 
                                    </div>
                                    <div class="col-sm-7 hidden-xs">
                                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lorem est, aliquet nec congue sit amet, ultricies sed sapien.</p>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-xs-12">
                                      <div class="inner-btns">
                                        <a href="detalleAnuncio" class="pull-right btn btn-blue">Ver inmueble</a>
                                      </div>    
                                    </div>
                                  </div>
                                </div>
                              </div>

                          </div>
                      </div>
                      
                  </div>
              </div>

              <div class="col-xs-12 clearfix">
                  <a href="#" class="btn btn-white-black highligh-btn pull-right">Descúbre cómo destacar tu publicación aquí</a>
              </div>

              
            </div>    
          </div>

          <!-- ends items -->

          <!-- starts pagination -->
          <div class="row">
              <div class=" col-sm-12">
                 <nav class="text-center">
                  <ul class="pagination original-style">
                    <li>
                      <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                      <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
          </div>
          <!--ends pagination -->

        </div>

        <div class="col-xs-12 col-md-4 ">
          <div class="google-advertise">
              <div class="square-advertise"></div>
              <div class="advertise-caption">
                  <a href="#">Tu publicidad en este sitio</a>
              </div>
          </div>
          <div class="google-advertise">
              <div class="square-advertise"></div>
              <div class="advertise-caption">
                  <a href="#">Tu publicidad en este sitio</a>
              </div>
          </div>
          <div class="google-advertise">
              <div class="square-advertise"></div>
              <div class="advertise-caption">
                  <a href="#">Tu publicidad en este sitio</a>
              </div>
          </div>
        </div>
      </div>
    </section>
    <!-- ends SEARCH RESULTS -->

</div>

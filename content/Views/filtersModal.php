

 <div>
        <div class="filters-section-header">
          <h3><span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Filtros</h3>
          <button type="button" ng-click="cancel()" class="close"><span aria-hidden="true">&times;</span></button>

        </div>
        <form id="filtersForm" name="filtersForm" class="editForm" method="post" ng-init="reset(this)" ng-submit="submit(filtersForm)" novalidate>
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="filters-section">
                <h4>Precio MXN</h4>
                <div class="row mb-20">
                  <div class="col-md-6"><span class="tag">{{passObj.price_min | currency}}</span></div>
                  <div class="col-md-6 text-right"><span class="tag">{{passObj.price_max | currency}}</span></div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <slider-range min="0" max="18000" value-min="passObj.price_min" value-max="passObj.price_max"></slider-range>
                  </div>
                </div>
                <div class="row mt-20">
                  <div class="col-md-6">
                    <span class="clear-tag">$0</span>
                  </div>
                  <div class="col-md-6 text-right">
                  <span class="clear-tag">$18,000</span>
                  </div>
                </div>
              </div>
              <div class="filters-section">
                <h4>Superficie M<sup>2</sup></h4>
                <div class="row mb-20">
                  <div class="col-md-6"><span class="tag">{{passObj.surface_min}}</span></div>
                  <div class="col-md-6 text-right"><span class="tag">{{passObj.surface_max}}</span></div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <slider-range min="0" max="120" value-min="passObj.surface_min" value-max="passObj.surface_max"></slider-range>
                  </div>
                </div>
                <div class="row mt-20">
                  <div class="col-md-6">
                    <span class="clear-tag">0</span>
                  </div>
                  <div class="col-md-6 text-right">
                  <span class="clear-tag">120</span>
                  </div>
                </div>
              </div>
              <div class="filters-section">
                <h4>Equipamento</h4>
                <div class="equipment-filters">
                  <input type="checkbox" name="equipamentoFilter" value="obraGris">Obra Gris<br>
                  <input type="checkbox" name="equipamentoFilter" value="serviciosBasicos">Servicios Básicos(luz, agua, gas)<br>
                  <input type="checkbox" name="equipamentoFilter" value="estacionamiento">Estacionamiento<br>
                  <input type="checkbox" name="equipamentoFilter" value="clima">Clima<br>
                  <input type="checkbox" name="equipamentoFilter" value="telefono">Teléfono<br>
                  <input type="checkbox" name="equipamentoFilter" value="internet">Internet<br>
                  <input type="checkbox" name="equipamentoFilter" value="mobiliario">Mobiliario<br>
                  <input type="checkbox" name="equipamentoFilter" value="sanitario">Sanitario<br>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="filters-section">
                <h4>Colonias</h4>
                <div class="input-group">
                  <input type="text" ng-model="selected" uib-typeahead="colonia for colonia in colonias | filter:$viewValue | limitTo:8" class="form-control">
                  <span class="input-group-addon rounded-btn glyphicon glyphicon-plus" ng-click="addColonia(selected)"></span>
                </div>
                <div class="mt-20">
                  <div class="tag list" ng-repeat="item in selection">{{item}} <span class="glyphicon glyphicon-remove right cursor-pointer" ng-click="deleteColonia(item)"></span></div>
                </div>
              </div>
            </div>
          </div>
        </form>
    <div class="modal-footer mt-20">
      <button type="submit" class="btn btn-blue btn-lg right" ng-disabled="filtersForm.$invalid">Ok Filtrar </button>
    </div>
  </div>
      
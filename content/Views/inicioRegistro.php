     <div class="container">
        <!-- starts breadcrumb -->
        <div class="row">
          <div class="col-xs-12">
             <ol class="breadcrumb">
              <li><a href="inicio"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
              <li><a href="#">Iniciar Sesión</a></li>
            </ol>
          </div>
        </div>
        <!-- ends breadcrumb -->
        <section ng-controller="logInController">
          
          <div class="row">
            <div class="col-xs-12 col-md-6 start-item">
              <div class="top-side">
                <p class="h2">Entra a tu perfil</p>
              </div>
              <div class="bottom-side">
                  <form id="logInForm" name="logInForm" method="post" ng-submit="logInCall()" novalidate>
                    <div class="form-error" ng-show="errorLogIn">
                      <span class="text-danger text-center">{{errorLogInStr}}</span>
                    </div>
                    
                    <div class="form-group" ng-class="{'has-error': (logInForm.logInEmail.$invalid && !logInForm.logInEmail.$pristine) || (errorLogIn)}">
                        <label>Correo</label>
                        <input type="email" class="form-control" id="logInEmail"  name="logInEmail" ng-model="logInEmail" placeholder="micorreo@cuenta.com" ng-pattern="regexp_email" required>

                        <div class="form-error" ng-show="logInForm.logInEmail.$invalid && !logInForm.logInEmail.$pristine">
                          <span class="text-danger" ng-show="logInForm.logInEmail.$error.required">*Obligatorio</span>
                          <span class="text-danger" ng-show="logInForm.logInEmail.$error.pattern">*No es un formato de email válido</span>
                        </div>
                    </div>
                    <div class="form-group" ng-class="{'has-error': (logInForm.logInPassword.$invalid && !logInForm.logInPassword.$pristine) || (errorLogIn)}">
                        <label>Contraseña</label>
                        <input type="password" class="form-control" id="logInPassword"  name="logInPassword" ng-model="logInPassword" placeholder="Contraseña" ng-pattern="regexp_6digits" required>
                        
                        <div class="form-error" ng-show="logInForm.logInPassword.$invalid && !logInForm.logInPassword.$pristine" >
                          <span class="text-danger" ng-show="logInForm.logInPassword.$error.required">*Obligatorio</span>
                          <span class="text-danger" ng-show="logInForm.logInPassword.$error.pattern">*La contraseña debe contener exactamente 6 caractéres.</span>
                        </div>
                    </div>

                    <div class="form-group" >
                      <a class="link" ng-click="forgotPassCall()">¿Olvidaste tu contraseña?</a>
                    </div>


                    <input type="submit" class="btn btn-aqua" ng-disabled="logInForm.$invalid"  value="Iniciar Sesión" >

                  </form>
              </div>
            </div>

            <div class="col-xs-12 col-md-6 start-item">
              <div class="top-side">
                <p class="h2"><span class="text-normal">Si no tienes una cuenta,</span> Regístrate</p>
              </div>
              <div class="bottom-side">
                  <form id="registerForm" name="registerForm" method="post" ng-submit="registerCall()" novalidate>
                    <div class="form-error" ng-show="errorRegister">
                      <span class="text-danger text-center">{{errorRegisterStr}}</span>
                    </div>
                    <div class="form-group" ng-class="{'has-error': registerForm.registerName.$invalid && !registerForm.registerName.$pristine}">
                        <label>Nombre</label>
                        <input type="text" class="form-control" id="registerName"  name="registerName" ng-model="newUser.registerName" placeholder="Nombre" required>

                        <div class="form-error" ng-show="registerForm.registerName.$invalid && !registerForm.registerName.$pristine">
                          <span class="text-danger" ng-show="registerForm.registerName.$error.required">*Obligatorio</span>
                        </div>
                    </div>

                    <div class="form-group" ng-class="{'has-error': registerForm.registerLastName.$invalid && !registerForm.registerLastName.$pristine}">
                        <label>Apellidos</label>
                        <input type="text" class="form-control" id="registerLastName"  name="registerLastName" ng-model="newUser.registerLastName" placeholder="Apellidos" required>

                        <div class="form-error" ng-show="registerForm.registerLastName.$invalid && !registerForm.registerLastName.$pristine">
                          <span class="text-danger" ng-show="registerForm.registerLastName.$error.required">*Obligatorio</span>
                        </div>
                    </div>

                    <div class="form-group" ng-class="{'has-error': registerForm.registerEmail.$invalid && !registerForm.registerEmail.$pristine}">
                        <label>Correo</label>
                        <input type="email" class="form-control" id="registerEmail"  name="registerEmail" ng-model="newUser.registerEmail" placeholder="micorreo@cuenta.com" ng-pattern="regexp_email" required>

                        <div class="form-error" ng-show="registerForm.registerEmail.$invalid && !registerForm.registerEmail.$pristine">
                          <span class="text-danger" ng-show="registerForm.registerEmail.$error.required">*Obligatorio</span>
                          <span class="text-danger" ng-show="registerForm.registerEmail.$error.pattern">*No es un formato de email válido</span>
                        </div>
                    </div>

                    <div class="form-group" ng-class="{'has-error': registerForm.registerPassword.$invalid && !registerForm.registerPassword.$pristine}">
                        <label>Contraseña</label>
                        <input type="password" class="form-control" id="registerPassword"  name="registerPassword" ng-model="newUser.registerPassword" ng-pattern="regexp_6digits" placeholder="Contraseña" required>
                        
                        <div class="form-error" ng-show="registerForm.registerPassword.$invalid && !registerForm.registerPassword.$pristine" >
                          <span class="text-danger" ng-show="registerForm.registerPassword.$error.required">*Obligatorio</span>
                          <span class="text-danger" ng-show="registerForm.registerPassword.$error.pattern">*La contraseña debe contener exactamente 6 caractéres.</span>
                        </div>
                    </div>

                    <input type="submit" class="btn btn-aqua" ng-disabled="registerForm.$invalid"  value="Crear Cuenta" >

                  </form>
              </div>
            </div>
          </div>
        </section>

      </div>
        
      
    

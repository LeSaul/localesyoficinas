

 
      <h3>Cambiar Datos de Cuenta</h3>
      
      
      <form id="changeDataForm" name="changeDataForm" class="editForm" method="post" ng-init="initForm()" ng-submit="submit(changeDataForm)" novalidate>
        <div class="form-error" ng-show="errorRegister">
          <span class="text-danger text-center">{{errorRegisterStr}}</span>
        </div>
        <div class="form-group" ng-class="{'has-error': changeDataForm.registerName.$invalid && !changeDataForm.registerName.$pristine}">
            <label>Nombre</label>
            <input type="text" class="form-control" id="registerName"  name="registerName" ng-model="userData.registerName" placeholder="Nombre" required>

            <div class="form-error" ng-show="changeDataForm.registerName.$invalid && !changeDataForm.registerName.$pristine">
              <span class="text-danger" ng-show="changeDataForm.registerName.$error.required">*Obligatorio</span>
            </div>
        </div>

        <div class="form-group" ng-class="{'has-error': changeDataForm.registerLastName.$invalid && !changeDataForm.registerLastName.$pristine}">
            <label>Apellidos</label>
            <input type="text" class="form-control" id="registerLastName"  name="registerLastName" ng-model="userData.registerLastName" placeholder="Apellidos" required>

            <div class="form-error" ng-show="changeDataForm.registerLastName.$invalid && !changeDataForm.registerLastName.$pristine">
              <span class="text-danger" ng-show="changeDataForm.registerLastName.$error.required">*Obligatorio</span>
            </div>
        </div>

        <div class="form-group" ng-class="{'has-error': changeDataForm.registerEmail.$invalid && !changeDataForm.registerEmail.$pristine}">
            <label>Correo</label>
            <input type="email" class="form-control" id="registerEmail"  name="registerEmail" ng-model="userData.registerEmail" placeholder="micorreo@cuenta.com" ng-pattern="regexp_email" required>

            <div class="form-error" ng-show="changeDataForm.registerEmail.$invalid && !changeDataForm.registerEmail.$pristine">
              
              <span class="text-danger" ng-show="changeDataForm.registerEmail.$error.required">*Obligatorio</span>
              <span class="text-danger" ng-show="changeDataForm.registerEmail.$error.pattern">*No es un formato de email válido</span>
            </div>
        </div>

        <div class="form-group" ng-class="{'has-error': changeDataForm.registerCellphone.$invalid && !changeDataForm.registerCellphone.$pristine}">
            <label>Celular</label>
            <input type="text" class="form-control" id="registerCellphone"  name="registerCellphone" ng-model="userData.registerCellphone" placeholder="Celular" >

            <div class="form-error" ng-show="changeDataForm.registerCellphone.$invalid && !changeDataForm.registerCellphone.$pristine">
              <span class="text-danger" ng-show="changeDataForm.registerCellphone.$error.required">*Obligatorio</span>
            </div>
        </div>

        <div class="form-group" ng-class="{'has-error': changeDataForm.registerPhone.$invalid && !changeDataForm.registerPhone.$pristine}">
            <label>Teléfono</label>
            <input type="text" class="form-control" id="registerPhone"  name="registerPhone" ng-model="userData.registerPhone" placeholder="Teléfono" >

            <div class="form-error" ng-show="changeDataForm.registerPhone.$invalid && !changeDataForm.registerPhone.$pristine">
              <span class="text-danger" ng-show="changeDataForm.registerPhone.$error.required">*Obligatorio</span>
            </div>
        </div>

        <div class="form-group" ng-class="{'has-error': changeDataForm.registerAddress.$invalid && !changeDataForm.registerAddress.$pristine}">
            <label>Dirección</label>
            <input type="text" class="form-control" id="registerAddress"  name="registerAddress" ng-model="userData.registerAddress" placeholder="Dirección" >

            <div class="form-error" ng-show="changeDataForm.registerAddress.$invalid && !changeDataForm.registerAddress.$pristine">
              <span class="text-danger" ng-show="changeDataForm.registerAddress.$error.required">*Obligatorio</span>
            </div>
        </div>

        
        <input type="submit" class="btn btn-aqua" ng-disabled="changeDataForm.$invalid" value="Cambiar Datos" >
        <button type="button" class="btn btn-aqua" ng-click="cancel()">Cancelar </button>

        </form>
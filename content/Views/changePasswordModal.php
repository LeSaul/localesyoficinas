

 
        <h3>Cambiar Contraseña</h3>
        <form id="passwordForm" name="passwordForm" class="editForm" method="post" ng-init="reset(this)" ng-submit="submit(passwordForm)" novalidate>
            <div class="form-error" ng-show="errorchangePassword">
              <span class="text-danger text-center">{{errorchangePasswordStr}}</span>
            </div>
            <!--
            <div class="form-group" ng-class="{'has-error': passwordForm.oldPassword.$invalid && !passwordForm.oldPassword.$pristine}">
                <label>Contraseña Anterior</label>
                <input type="password" class="form-control" id="oldPassword"  name="oldPassword" ng-model="passObj.oldPassword" ng-pattern="regexp_6digits" required>

                <div class="form-error" ng-show="passwordForm.oldPassword.$invalid && !passwordForm.oldPassword.$pristine">
                  <span class="text-danger" ng-show="passwordForm.oldPassword.$error.required">*Obligatorio</span>
                   <span class="text-danger" ng-show="passwordForm.oldPassword.$error.pattern">*La contraseña debe contener exactamente 6 caractéres.</span>
                </div>
            </div>

          -->

            <div class="form-group" ng-class="{'has-error': passwordForm.newPassword.$invalid && !passwordForm.newPassword.$pristine}">
                <label>Contraseña Nueva</label>
                <input type="password" class="form-control" id="newPassword"  name="newPassword" ng-model="passObj.newPassword" ng-pattern="regexp_6digits" autofocus required>

                <div class="form-error" ng-show="passwordForm.newPassword.$invalid && !passwordForm.newPassword.$pristine">
                  <span class="text-danger" ng-show="passwordForm.newPassword.$error.required">*Obligatorio</span>
                  <span class="text-danger" ng-show="passwordForm.newPassword.$error.pattern">*La contraseña debe contener exactamente 6 caractéres.</span>
                </div>
            </div>


            <div class="clearfix">
              <button type="submit" class="btn btn-aqua" ng-disabled="passwordForm.$invalid">Cambiar </button>
              <button type="button" class="btn btn-aqua" ng-click="cancel()">Cancelar </button>
            </div>

          </form>
      
<!-- starts breadcrumb -->
<div class="container" >
  <div class="row">
    <div class="col-xs-12">
       <ol class="breadcrumb">
        <li><a href="inicio"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
        <li><a href="#">Renta - Locales - Monterrey - La Ladrillera - Local en renta en nuevo sur</a></li>
      </ol>
    </div>
  </div>
</div>
<!-- ends breadcrumb -->

<div class="detail-pagination container">
  <div class="row">
    <div class="col-xs-4">
      <a href="detalleAnuncio" aria-label="Anterior" class="indicators-cont pull-left">
        <div class="indicators">
          <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
        </div>
        <span>Anterior</span>
      </a>
    </div>
    <div class="col-xs-4">
      <a href="listaResultados" class="detail-link">Regresar a la búsqueda</a>
    </div>
    <div class="col-xs-4">
       <a href="detalleAnuncio" aria-label="Siguiente" class="indicators-cont pull-right">
        <span>Siguiente</span>
        <div class="indicators">
          <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
        </div>
      </a>
    </div>
  </div>
</div>

<div class="container" ng-controller="detalleAnuncioController" ng-init="verDetalle()" >
  <div class="row">
    <div class="col-md-8 col-xs-12">
      <div class="row">
        <div class="col-md-8 col-xs-12">
          <slick class="slider-for detaileSlider" infinite="false" arrows="false" dots="false" slides-to-show="1" slides-to-scroll="1" >
            <div><img src="Content/auth/img/detalleAnuncio/1.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/2.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/3.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/4.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/1.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/2.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/3.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/4.png" alt=""></div>
          </slick>

          <slick class="detailSlider-pager" class="slider-nav" dots="true" infinite="false" speed="300" slides-to-show="5" touch-move="true" slides-to-scroll="5" as-nav-for=".slider-for" focus-on-select="true">
            <div><img src="Content/auth/img/detalleAnuncio/1_thumb.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/2_thumb.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/3_thumb.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/4_thumb.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/1_thumb.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/2_thumb.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/3_thumb.png" alt=""></div>
            <div><img src="Content/auth/img/detalleAnuncio/4_thumb.png" alt=""></div>
          </slick>

        </div>
        <div class="col-md-4 col-xs-12 detail-information" ng-controller="misFavoritosController as favCtrl">
          <div class="row">
            <div class="col-md-12 col-xs-6">
              <span class="semibold-txt">RENTA EN:</span>
              <span class="detail-price" ng-bind="results.precio | currency"></span>
              <span class="detail-length binding">Superficie: <span ng-bind="results.superficie"></span> m<sup>2</sup></span>
              <span class="detail-city">
                <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Monterrey
              </span>
              <span class="detail-residence">Col. La Ladrillera</span>
              <span class="detail-publish">Publicado el: <span ng-bind="results.fechaInicio | date:'dd/MMM/yyyy'"></span></span>
            </div>
            <div class="col-md-12 col-xs-6">
              <span class="equipment-header">Equipamiento:</span>
              <ul class="equipment-list">
                <li>Obra gris</li>
                <li>Estacionamiento</li>
                <li>Clima</li>
                <li>Teléfono</li>
                <li>Servicios</li>
                <li>Internet</li>
                <li>Mobiliario</li>
                <li>Sanitarios</li>
              </ul>
            </div>
          </div>
         
          <button class="btn btn-aqua" ng-disabled="results.favorito || favoriteDisabled" ng-click="insertarFavoritoCall();">
            <span class=" glyphicon glyphicon-star" aria-hidden="true"></span> 
            Agregar a Favoritos
          </button>
          <button class="btn btn-aqua">
            <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
            Compartir por correo
          </button>
        </div>
      </div>

      <div class="subtitle-txt mb-20 clearfix">
          <h3 class="pull-left">DESCRIPCIÓN</h3>
      </div>
      <p ng-bind="results.descripcion"></p>
      
    </div>
    <div class="col-md-4 col-xs-12">
        <div class="detail-contact well">
          <a href="perfilAgente"><span class="header">Servicios García</span></a>
          <a href="perfilAgente" class="go-profile">Ir al perfil</a>
          <div class="company-logo">
            <div class="row">
              <div class="col-xs-4">
                <img src="Content/auth/img/company.png" alt="..." class="img-circle">
              </div>
              <div class="col-xs-8">
                <span class="company-name">COMPANY NAME HERE</span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-xs-6">
              <span class="company-phone">Teléfono: <span class="company-number">(818) 870 61 45</span></span>
            </div>
            <div class="col-md-12 col-xs-6">
              <span class="company-celphone">Celular: <span class="company-celnumber">(818) 870 61 45</span></span>
            </div>
          </div>
          
          <form id="detailForm" name="detailForm" class="clearfix detail-form" method="post" novalidate>
            <label class="navbar-toggle collapsed" data-toggle="collapse" data-target="#detailFormContent" aria-expanded="false" aria-controls="navbar">Hacer una pregunta:</label>
            <label class="hidden-xs hidden-sm">Hacer una pregunta:</label>
            
            <div id="detailFormContent" class="navbar-collapse collapse">
              <div class="form-group" ng-class="{'has-error': detailForm.contactName.$invalid && !detailForm.contactName.$pristine}">
                <label>Nombre Completo</label>
                <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" id="contactName" name="contactName" ng-model="returnData.contactName"required>

                <div class="form-error ng-hide" ng-show="detailForm.contactName.$invalid && !detailForm.contactName.$pristine">
                <span class="text-danger" ng-show="detailForm.contactName.$error.required">*Obligatorio</span>
                  </div>
              </div>
              <div class="form-group" ng-class="{'has-error': detailForm.contactEmail.$invalid && !detailForm.contactEmail.$pristine}">
                <label>Correo Electrónico:</label>
                <input type="email" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" id="contactEmail" name="contactEmail" ng-model="returnData.contactEmail" ng-pattern="regexp_email" required>

                <div class="form-error ng-hide" ng-show="detailForm.contactEmail.$invalid && !detailForm.contactEmail.$pristine">
                <span class="text-danger" ng-show="detailForm.contactEmail.$error.required">*Obligatorio</span>
                <span class="text-danger" ng-show="detailForm.contactEmail.$error.pattern">*No es un formato de email válido</span>
                  </div>
              </div>
              <div class="form-group" ng-class="{'has-error': detailForm.contactPhone.$invalid && !detailForm.contactPhone.$pristine}">
                <label>Teléfono:</label>
                <input type="text" class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" id="contactPhone" name="contactPhone" ng-model="returnData.contactPhone"required>

                <div class="form-error ng-hide" ng-show="detailForm.contactPhone.$invalid && !detailForm.contactPhone.$pristine">
                <span class="text-danger" ng-show="detailForm.contactPhone.$error.required">*Obligatorio</span>
                  </div>
              </div>
              <div class="form-group" ng-class="{'has-error': detailForm.contactMessage.$invalid && !detailForm.contactMessage.$pristine}">
                <label>¿Qué dudas tienes?</label>
                <textarea class="form-control ng-pristine ng-invalid ng-invalid-required ng-touched" rows="3" id="contactMessage" name="contactMessage" ng-minlength="10" ng-model="returnData.contactMessage" required></textarea>

                <div class="form-error ng-hide" ng-show="detailForm.contactMessage.$invalid && !detailForm.contactMessage.$pristine">
                  <span class="text-danger" ng-show="detailForm.contactMessage.$error.required">*Obligatorio</span>
                </div>
              </div>

              <button type="submit" class="full-width btn btn-blue">Solicitar Información</button>

            </div>
          </form>
        </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-4 col-xs-12">
      <div class="google-advertise">
          <div class="square-advertise"></div>
          <div class="advertise-caption">
              <a href="#">Tu publicidad en este sitio</a>
          </div>
      </div>
    </div>
   <div class="col-md-4 col-xs-12 hidden-xs">
      <div class="google-advertise">
          <div class="square-advertise"></div>
          <div class="advertise-caption">
              <a href="#">Tu publicidad en este sitio</a>
          </div>
      </div>
    </div>
   <div class="col-md-4 col-xs-12 hidden-xs">
      <div class="google-advertise">
          <div class="square-advertise"></div>
          <div class="advertise-caption">
              <a href="#">Tu publicidad en este sitio</a>
          </div>
      </div>
    </div>
  </div>
</div>

<section class="remember-section" ng-controller="resultListController">
    <div class="container">
        <div class="row">
            <div class=" col-sm-12">
                <div class="subtitle-txt clearfix">
                    <h3 class="pull-left">También te puede interesar</h3>
                </div>
            </div>
        </div>
        <!-- starts home items -->
        <div class="row">
            <div class="main-items-container" ng-init="getAllAnunciosCall()" ng-controller="sliderController">
                
                <slick ng-if="results.length" responsive="breakpointsOther" dots="true" infinite="false" speed="300" slides-to-show="5" touch-move="true" slides-to-scroll="1"  init-onload="true" data="results">
                    <div class="col-xs-6 col-sm-4 col-lg-2" ng-repeat="resultado in results">
                        <div class="main-item ng-cloak" ng-class="{'highlight': resultado.idModo == 2, 'gray': resultado.idModo == 3}">
                            <div class="main-item-top">
                                <p ng-bind="resultado.titulo"></p>
                            </div>
                            <div class="main-item-content">
                                <div class="main-item-inner">
                                    <div class="highlight-ribbon"><span>destacado</span></div>
                                    <img src="Content/auth/img/dept.png" alt="">
                                    <div class="item-location">
                                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                        <span>Monterrey</span>
                                    </div>
                                    <div class="item-info">
                                        <span ng-bind="resultado.precio | currency"></span>
                                        <span class="binding"><span ng-bind="resultado.superficie"></span>m<sup>2</sup></span>
                                    </div>
                                </div>
                            </div>
                            <button class="btn-blue" ng-click="verAnuncioCall(resultado.idAnuncio);">Ver inmueble</button>
                        </div>
                    </div>
                   
                </slick> 
            </div>
        </div>
        <!-- ends home items -->

    </div>

</section>

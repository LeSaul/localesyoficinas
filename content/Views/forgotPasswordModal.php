

<form id="passwordForm" name="forgotPasswordForm" class="editForm" method="post" ng-init="reset(this)" ng-submit="submit(forgotPasswordForm)" novalidate>
    <div class="row">
        <div class="col-md-12">
            <h3 class="mb-20">Olvidaste tu contraseña?</h3>
            <p>Introduce tu nombre de usuario para enviar una nueva contraseña.</p>
        </div>
        <div class="col-md-8 mb-20">
            <div class="form-group">
                <label for="username">Nombre de usuario</label>
                <input type="text" class="form-control col-md-8" id="username" ng-model="passObj.user">
            </div>
            <div class="form-error" ng-show="errorForgotPassword">
                <span class="text-danger text-center">{{errorForgotPasswordStr}}</span>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-aqua btn-aqua-inline md" ng-disabled="forgotPasswordForm.$invalid">Enviar Contraseña</button>
        <button type="button" class="btn btn-white-black md" ng-click="cancel()">Cancelar </button>
    </div>

</form>
      
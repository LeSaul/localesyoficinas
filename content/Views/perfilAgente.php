
<!-- starts breadcrumb -->
<div class="container">
  <div class="row">
    <div class="col-xs-12">
       <ol class="breadcrumb">
        <li><a href="inicio"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
        <li><a href="#">Perfil Público de Emmanuel Justosierra <span> - Agencia Muebleria RJ</span> </a></li>
      </ol>
    </div>
  </div>
</div>
<!-- ends breadcrumb -->

<div class="container">
    <div class="row">
        <div class="col-xs-12">
          <p class="welcome-text">Emmanuel Justosierra <span> - Agencia Muebleria RJ</span> </p>
        </div>
    </div>

    <!-- starts MIS ANUNCIOS -->
    <section>
      <div class="row" vertilize-container>
        <div class="col-xs-12 col-md-10" vertilize>

          <div class="agent-data clearfix" vertilize-container>
            <div class="pull-left left-side" vertilize>
              <img src="Content/auth/img/negocio.jpg" alt="">
            </div>
            <div class="pull-left agent-info" vertilize>
              <span class="agent-name">Emmanuel Justosierra <span> - Agencia Muebleria RJ</span></span>
              <span class="agent-site">www.susitioweb.com</span>
              <span>Teléfono: <span>(818) 870 61 45</span></span>
              <span>Celular: <span>(811) 870 61 45</span></span>
            </div>
          </div>

          <div class="subtitle-txt clearfix">
            <h3>Mis Anuncios</h3>
          </div>

           <!-- starts items -->
          <div class="row">
            <div class="main-items-container">
              <slick class="responsive-slider" dots="true" infinite="false" speed="300" slides-to-show="1" touch-move="true" slides-to-scroll="1">
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">

                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item gray">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item gray">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item gray">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                  <div class="col-xs-6 col-sm-4 col-lg-2">
                      <div class="main-item highlight">
                          <div class="main-item-top">
                              <p>DEPARTAMENTO EN RENTA EN ...</p>
                          </div>
                          <div class="main-item-content">
                              <div class="main-item-inner">
                                  <div class="highlight-ribbon"><span>destacado</span></div>
                                  <img src="Content/auth/img/dept.png" alt="">
                                  <div class="item-location">
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <span>Monterrey</span>
                                  </div>
                                  <div class="item-info">
                                      <span>$13,000</span>
                                      <span>12m<sup>2</sup></span>
                                  </div>
                              </div>
                          </div>
                          <a href="detalleAnuncio" class="btn-blue">Ver inmueble</a>
                      </div>
                  </div>
                
                  
                 
              </slick> 
            </div>
          </div>
          <!-- ends items -->

        </div>
        <div class="col-xs-12 col-md-2 hidden-xs hidden-sm">
          <div class="google-advertise">
              <div class="square-advertise" vertilize></div>
              <div class="advertise-caption">
                  <a href="#">Tu publicidad en este sitio</a>
              </div>
              
          </div>
        </div>
      </div>
    </section>
    <!-- end MIS ANUNCIOS -->

</div>

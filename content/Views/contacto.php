
<!-- starts breadcrumb -->
<div class="container">
  <div class="row">
    <div class="col-xs-12">
       <ol class="breadcrumb">
        <li><a href="inicio"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
        <li><a href="#">Contacto</a></li>
      </ol>
    </div>
  </div>
</div>
<!-- ends breadcrumb -->

<article>
  <div class="container"> 
    <div class="row advertise-company" data-equalizer>
      <div class="col-xs-12 col-sm-4 left-side" >
        <img src="Content/auth/img/negocio.jpg" data-equalizer-watch>
      </div>
      <div class="col-xs-12 col-sm-8 right-side" data-equalizer-watch>
        <div class="right-inner">
          <p class="h2 text-normal">Da a conocer tu negocio, te ayudamos.</p>
          <p>Te ofrecemos espacios publicitarios a lo largo de nuestro sitio web para que anuncies tu negocio. Estamos seguros que nuestros agentes y clientes buscando locales y oficinas estarán interesados en lo que les puedes ofrecer.</p>
          <div class="clearfix">
            <button class="btn btn-aqua pull-right">Anunciar mi negocio</button>
          </div>  
        </div>
      </div>
    </div>
  </div>
</article>

<section class="contact-container">
  <div class="container">
      <div class="row">
        <div class="col-xs-12 title-side">
          <p class="h2 text-normal">Queremos saber de tí, contáctanos:</p>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-8">
          <form id="contactForm" name="contactForm" method="post" novalidate>
             <div class="form-group" ng-class="{'has-error': contactForm.contactName.$invalid && !contactForm.contactName.$pristine}">
                <label>Nombre</label>
                <input type="text" class="form-control" id="contactName"  name="contactName" ng-model="returnData.contactName" placeholder="Nombre Completo" required>

                <div class="form-error" ng-show="contactForm.contactName.$invalid && !contactForm.contactName.$pristine">
                  <span class="text-danger" ng-show="contactForm.contactName.$error.required">*Obligatorio</span>
                </div>
            </div>

            <div class="form-group" ng-class="{'has-error': contactForm.contactEmail.$invalid && !contactForm.contactEmail.$pristine}">
                <label>Correo</label>
                <input type="email" class="form-control" id="contactEmail"  name="contactEmail" ng-model="returnData.contactEmail" placeholder="micorreo@cuenta.com" ng-pattern="regexp_email" required>

                <div class="form-error" ng-show="contactForm.contactEmail.$invalid && !contactForm.contactEmail.$pristine">
                  <span class="text-danger" ng-show="contactForm.contactEmail.$error.required">*Obligatorio</span>
                  <span class="text-danger" ng-show="contactForm.contactEmail.$error.pattern">*No es un formato de email válido</span>
                </div>
            </div>

            <div class="form-group" ng-class="{'has-error': contactForm.contactMessage.$invalid && !contactForm.contactMessage.$pristine}">
                <label>Dudas</label>
                <textarea class="form-control" rows="3" id="contactMessage"  name="contactMessage" ng-model="returnData.contactMessage" placeholder="Cuentanos tus dudas" required></textarea>

                <div class="form-error" ng-show="contactForm.contactMessage.$invalid && !contactForm.contactMessage.$pristine">
                  <span class="text-danger" ng-show="contactForm.contactMessage.$error.required">*Obligatorio</span>
                </div>
            </div>

            <div class="clearfix">
              <button type="submit" class="btn btn-aqua pull-right">Enviar <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
              </button>
            </div>
          </form>

          <div>
            <p class="h2 text-normal">Si lo prefieres, mándanos un correo a:</p>
            <a class="email-info" href="mailto:info@localesyoficinas.mx">info@localesyoficinas.mx</a>
          </div>

        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="google-advertise">
              <div class="square-advertise"></div>
              <div class="advertise-caption">
                  <a href="#">Tu publicidad en este sitio</a>
              </div>
              
          </div>
        </div>
      </div>
  </div>
</section>